#include "parameters.hpp"
#include <armadillo>

#ifndef SIMULATION
#define SIMULATION

using namespace std;

class Simulation{
    public:
        Simulation();
        Simulation(
            std::string parameters_path,
            std::string result_path);
        Simulation(
            std::string parameters_path,  
            std::string result_path,
            bool friction_parallelized);
        void init(
            std::string parameters_path, 
            std::string result_path);
    public:
        void setupModelVectorsMatrices();
        void setupBlockMatrix();
        void setupBlockMatrixDerivative();
        void setupPadVector();
        void setupPadVectorDerivative();
        void setupSaveVectorsMatrices();
        void copyYamlFile();
        void setupSliderSpeedMode();

        void setupConstants();

        double phi(double y);

        void setSpeedAfterThreshold();
        void updateSliderSpeed(
            int time_step
        );

        void function_u(
            const arma::mat& current_block_matrix, 
            const arma::rowvec& current_pad_vector, 
            arma::mat& current_block_matrix_derivative
        );

        void function_x(
            const arma::mat& current_block_matrix, 
            const arma::rowvec& current_pad_vector, 
            arma::rowvec& current_pad_vector_derivative
        );
        void function_x_odof(
            const arma::rowvec& current_pad_vector,
            arma::rowvec& current_pad_vector_derivative
        );
        double calculatePadSupportDamper(
            arma::rowvec current_pad_vector
        );
        double calculatePadFriction(
            arma::mat current_block_matrix, 
            arma::rowvec current_pad_vector
        );

        void midpointMethod(
            int time_step
        );

        void oneDegreeOfFreedomMidpointMethod(
            int time_step
        );

        void writeValuesToLoggers(
            const int& time_step
        );

        void printMidPointMethod(
            arma::mat block_matrix, 
            arma::rowvec pad_vector, 
            string explanation_text = "");
        void runMidpointMethod();

        void saveToCsv(
            string filename, 
            string result_path);

        void midpointMethodParallel(
            int timestep, 
            arma::mat& block_matrix_parallel, 
            arma::rowvec& pad_vector_parallel,
            arma::mat block_matrix_derivative_parallel, 
            arma::rowvec pad_vector_derivative_parallel);
        void setupAndRunFrictionInParallel(
            int time_step,
            double slider_speed);
        void calculateFrictionInParallel();
        void saveToCsvParallel(
            const arma::colvec& armadillo_vector,
            const string& result_path,
            const double& slider_speed
        );
        void createFolder(std::string folder);
        
        double calculateStick();
        void increaseStick(double velocity);

        void calculateEigenvaluePairs();
        void saveEigenvalues();
        arma::mat createMassMatrix();
        arma::mat createStiffnessMatrix();
        void calculateDiagonalMatrices(arma::mat mass_matrix, arma::mat stiffness_matrix);
        void isSymmetric(arma::mat matrix, std::string name);
        void testEigen();
        void steadyStateSolutuion();
        void saveSteadyState(string filename, string path_result, arma::mat block_position_steady, arma::vec pad_position_steady);
        void setupLyapunov();


        // Variabless

        std::string input_parameters_path;
        
        Parameters input_parameters;
        std::string result_path;
        std::string result_folder;
        std::string yaml_path;
        arma::mat block_matrix;
        arma::mat block_matrix_derivative;

        arma::rowvec pad_vector;
        arma::rowvec pad_vector_derivative;

        int number_of_time_steps;
        int number_of_save_steps;
        int number_of_interval_save_step;

        double m_u;		// mass of block
        double k_p;			// Stationary spring between pad and blocks
        double m_c_crit;		// Critical damping coefficient
	    double c_p;			// Damper between pad and surface
	    double k_c;			// Neighboring spring

        int updateSliderSpeedDtInterval; //translate the interval for updating slider speed based on dt

        bool loggingSpecificBlocks = false;
        arma::uvec block_position_column_index = {0};
        arma::uvec block_velocity_column_index = {1};

        arma::mat block_position_to_file;
        arma::mat block_velocity_to_file;
        arma::colvec pad_position_to_file;
        arma::colvec pad_velocity_to_file;
        arma::colvec pad_friction_to_file;

        int index_pad_friction_to_file = 0;
        int index_to_file = 0;

        int friction_zero_counter = 0;

        int stick_counter = 0;
        int total_stick_counter = 0;

        arma::cx_vec eigval;
        arma::cx_mat eigvec;
        arma::vec real_eig_values;
        arma::mat real_eig_vectors; 

        arma::mat block_matrix_lyapunov;
        arma::mat block_matrix_derivative_lyapunov;
        arma::rowvec pad_vector_lyapunov;
        arma::rowvec pad_vector_derivative_lyapunov;
        double alpha;
        double delta_gamma_new;
        double delta_gamma_prev;

};

// DebugSimulation inheriting from Simulation
class DebugSimulation: public Simulation{
    public:
        DebugSimulation(std::string debug_parameters_path, std::string result_path) : Simulation(debug_parameters_path, result_path){};
        void main();

    protected:
        void printMatrix(arma::mat matrix, bool arra_format);
        void printRowVector(arma::rowvec vector);

};

#endif