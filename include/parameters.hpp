#include <yaml-cpp/yaml.h>
#include <armadillo>

#ifndef PARAMETERS
#define PARAMETERS

class Parameters
{
    public:
        // Constructers:
        Parameters();
        Parameters(std::string filePath);
        //Methods:
        YAML::Node read_parameters;
        YAML::Node parameters;
        YAML::Node debug_parameters;

        void load_file(std::string filepath);
        void assign_parameters();
        double readParameterDouble(std::string parameter_name);
        double readParameterInt(std::string parameter_name);
        std::string readParameterString(std::string parameter_name);
        bool readParameterBool(std::string parameter_name);
        arma::uvec readParameterIntList(std::string parameter_name);
        arma::uvec yamlListToArmadilloVector(YAML::Node yamlList);
        bool readDebugParameterBool(std::string debug_parameter_name);

        //Variables:
        // global constants
        double dt;
        int seed;
        int num_events;

        // local constants
        int N;
        double max_time;
        double slider_speed;
        double increment;
        double interval;

        std::string file_name;
        bool progress_indicator;

        // bksim parameters
        double m_F0;								// Maximum static fricition force = unity
        double m_alpha;							// Used to scale velocity input to the friction function
        double m_sigma;							// Used to adjust maximum kinetic friction force

        double m_mass_x;							// Mass of pad
        double m_scale_mass;						// Scaling factor of block mass
        double m_zeta;					// Damping ratio (m_c_P/m_c_crit) must be < 1
        double m_k_P0;							// Stationary spring between pad and surface
        double m_scale_P;							// Scaling factor of spring constants between block and pad
        double m_scale_C;						// Scaling factor of neighboring spring constants (axial stiffness)

        double m_t;
        double m_v0;// = m_F0*m_dt / (m_mass); // Threshold velocity
        double m_u_min;						// "zero" velocity

        arma::uvec loggedBlocks;

        double start_speed_continuous;
        double end_speed_continuous;

        int save_interval_dt;
        double threshold_speed;

        double m_eta;
        double m_delta;
        double lyapunov_delta;

        // debug of model

        bool debug_no_friction;
        bool debug_no_neighbor_springs;
        bool debug_no_stationary_springs;
        bool debug_no_damper;
        bool debug_no_min_speed;
        bool debug_no_pad;
        bool debug_negative_intial_values;
        bool debug_no_random_displacements;
        bool debug_only_negative_initial;
        bool debug_special_phi;
        bool debug_pad_as_block;
        bool debug_stop_slider;
        bool debug_stick_blocks;
        bool debug_write_blocks;
        bool debug_only_write_friction;
        bool debug_continuous_slider_speed;
        bool debug_print;
        bool debug_print_event;
        bool debug_one_degree_freedom_mode;
        bool debug_damp_all_blocks;
        bool rayleigh_damping;
        bool calculate_Lyapunov;
};

class DebugParameters: public Parameters{
    public:
        DebugParameters(std::string debug_test_parameters): Parameters(debug_test_parameters){};
        int printCurrentDirectory();
        void printLoadedFile();
        void debugTest();
};

#endif