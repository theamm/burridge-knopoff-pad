#include <yaml-cpp/yaml.h>
#include <armadillo>
#include "parameters.hpp"
#include "simulation.hpp"

#ifndef OPERATION
#define OPERATION

class Operation
{
    public:
        // Constructers:
        Operation(Simulation& sim);

        // Metods
        void calculateEigenvaluePairs();
        void saveEigenvalues();
        arma::mat createMassMatrix();
        arma::mat createStiffnessMatrix();
        arma::mat createStiffnessMatrixBK();
        arma::mat createMassMatrixBK();
        void calculateDiagonalMatrices(arma::mat mass_matrix, arma::mat stiffness_matrix);
        void isSymmetric(arma::mat matrix, std::string name);
        void testEigen();
        
        void createHeatMapMatrix();
        void createZeroEigenMatrices();
        void merge_block_pad();

        void saveHeatMapMatrices();
        void heat_map_main();

        void sortEigenvectors();

        arma::mat steadyStateSolutuion();
        void amplitudeModeZero();
        void modeZeroRemoveMean();
        void normalize(arma::mat mass_matrix);
        void createStiffnessMatrixOneBlock();

        // Parameters
        Parameters input_parameters;
        std::string result_path;
        std::string result_folder;


        arma::cx_vec eigval;
        arma::cx_mat eigvec;
        arma::vec real_eig_values;
        arma::mat real_eig_vectors; 
        arma::mat diagonal_mass_matrix;
        arma::mat diagonal_stiffness_matrix;

        arma::mat position_matrix_heat;
        arma::mat velocity_matrix_heat;
        arma::mat energy_matrix_heat;

        arma::mat mass_matrix;
        arma::mat stiffness_matrix;

        arma::mat block_position_to_file;
        arma::mat block_velocity_to_file;
        arma::colvec pad_position_to_file;
        arma::colvec pad_velocity_to_file;
        arma::colvec pad_friction_to_file;
        

        int number_of_save_steps;
        double m_u;
        double k_p;
        double k_c;	
        int degrees_freedom;

        arma::mat block_pad_position;
        arma::mat block_pad_velocity;
};

#endif