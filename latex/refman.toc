\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {section}{\numberline {1}The Burridge Knopoff model}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}Descripton}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Motivation}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Directiories}{2}{subsection.1.3}%
\contentsline {subsubsection}{\numberline {1.3.1}yaml}{2}{subsubsection.1.3.1}%
\contentsline {subsubsection}{\numberline {1.3.2}src}{2}{subsubsection.1.3.2}%
\contentsline {paragraph}{\numberline {1.3.2.1}src/simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}:}{2}{paragraph.1.3.2.1}%
\contentsline {paragraph}{\numberline {1.3.2.2}src/plots\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}:}{2}{paragraph.1.3.2.2}%
\contentsline {paragraph}{\numberline {1.3.2.3}src/archive\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}:}{2}{paragraph.1.3.2.3}%
\contentsline {subsubsection}{\numberline {1.3.3}include}{2}{subsubsection.1.3.3}%
\contentsline {subsubsection}{\numberline {1.3.4}results}{2}{subsubsection.1.3.4}%
\contentsline {subsubsection}{\numberline {1.3.5}html, latex and .\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}vscode}{2}{subsubsection.1.3.5}%
\contentsline {subsection}{\numberline {1.4}Installation}{3}{subsection.1.4}%
\contentsline {subsubsection}{\numberline {1.4.1}Simulations (c++)}{3}{subsubsection.1.4.1}%
\contentsline {subsubsection}{\numberline {1.4.2}Compilation}{3}{subsubsection.1.4.2}%
\contentsline {paragraph}{\numberline {1.4.2.1}If it wont compile\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}:}{3}{paragraph.1.4.2.1}%
\contentsline {subsubsection}{\numberline {1.4.3}Pythonplots}{3}{subsubsection.1.4.3}%
\contentsline {section}{\numberline {2}src/simulation}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Description}{4}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Class Simulation}{4}{subsubsection.2.1.1}%
\contentsline {paragraph}{\numberline {2.1.1.1}simulation.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.1.1}%
\contentsline {paragraph}{\numberline {2.1.1.2}simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}setup.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.1.2}%
\contentsline {paragraph}{\numberline {2.1.1.3}midpoint.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.1.3}%
\contentsline {paragraph}{\numberline {2.1.1.4}numerical.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.1.4}%
\contentsline {paragraph}{\numberline {2.1.1.5}simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}paralell\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}friction.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.1.5}%
\contentsline {paragraph}{\numberline {2.1.1.6}suimulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}help\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}functions.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.1.6}%
\contentsline {subsubsection}{\numberline {2.1.2}Class Parameters}{4}{subsubsection.2.1.2}%
\contentsline {paragraph}{\numberline {2.1.2.1}parameters.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.2.1}%
\contentsline {subsubsection}{\numberline {2.1.3}Class Operations}{4}{subsubsection.2.1.3}%
\contentsline {paragraph}{\numberline {2.1.3.1}operations.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.3.1}%
\contentsline {paragraph}{\numberline {2.1.3.2}operation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}eigen.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.3.2}%
\contentsline {paragraph}{\numberline {2.1.3.3}operation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}heat\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}map.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}cpp}{4}{paragraph.2.1.3.3}%
\contentsline {paragraph}{\numberline {2.1.3.4}sort\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}eigenvalues.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}py}{5}{paragraph.2.1.3.4}%
\contentsline {subsubsection}{\numberline {2.1.4}makefile}{5}{subsubsection.2.1.4}%
\contentsline {subsection}{\numberline {2.2}Installation}{5}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Simulations packages (c++)}{5}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Compilation}{5}{subsubsection.2.2.2}%
\contentsline {paragraph}{\numberline {2.2.2.1}Problems?}{5}{paragraph.2.2.2.1}%
\contentsline {section}{\numberline {3}Namespace Index}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Namespace List}{5}{subsection.3.1}%
\contentsline {section}{\numberline {4}Hierarchical Index}{6}{section.4}%
\contentsline {subsection}{\numberline {4.1}Class Hierarchy}{6}{subsection.4.1}%
\contentsline {section}{\numberline {5}Class Index}{6}{section.5}%
\contentsline {subsection}{\numberline {5.1}Class List}{6}{subsection.5.1}%
\contentsline {section}{\numberline {6}File Index}{6}{section.6}%
\contentsline {subsection}{\numberline {6.1}File List}{6}{subsection.6.1}%
\contentsline {section}{\numberline {7}Namespace Documentation}{7}{section.7}%
\contentsline {subsection}{\numberline {7.1}sort\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}eigenvalues Namespace Reference}{7}{subsection.7.1}%
\contentsline {subsubsection}{\numberline {7.1.1}Function Documentation}{7}{subsubsection.7.1.1}%
\contentsline {paragraph}{\numberline {7.1.1.1}load()}{7}{paragraph.7.1.1.1}%
\contentsline {paragraph}{\numberline {7.1.1.2}run()}{8}{paragraph.7.1.1.2}%
\contentsline {paragraph}{\numberline {7.1.1.3}save()}{8}{paragraph.7.1.1.3}%
\contentsline {paragraph}{\numberline {7.1.1.4}sort\_eigen\_values()}{9}{paragraph.7.1.1.4}%
\contentsline {section}{\numberline {8}Class Documentation}{9}{section.8}%
\contentsline {subsection}{\numberline {8.1}Debug\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}Parameters Class Reference}{9}{subsection.8.1}%
\contentsline {subsubsection}{\numberline {8.1.1}Constructor \& Destructor Documentation}{11}{subsubsection.8.1.1}%
\contentsline {paragraph}{\numberline {8.1.1.1}DebugParameters()}{12}{paragraph.8.1.1.1}%
\contentsline {subsubsection}{\numberline {8.1.2}Member Function Documentation}{12}{subsubsection.8.1.2}%
\contentsline {paragraph}{\numberline {8.1.2.1}debugTest()}{12}{paragraph.8.1.2.1}%
\contentsline {paragraph}{\numberline {8.1.2.2}printCurrentDirectory()}{12}{paragraph.8.1.2.2}%
\contentsline {paragraph}{\numberline {8.1.2.3}printLoadedFile()}{12}{paragraph.8.1.2.3}%
\contentsline {subsection}{\numberline {8.2}Debug\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}Simulation Class Reference}{13}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}Constructor \& Destructor Documentation}{15}{subsubsection.8.2.1}%
\contentsline {paragraph}{\numberline {8.2.1.1}DebugSimulation()}{15}{paragraph.8.2.1.1}%
\contentsline {subsubsection}{\numberline {8.2.2}Member Function Documentation}{15}{subsubsection.8.2.2}%
\contentsline {paragraph}{\numberline {8.2.2.1}main()}{15}{paragraph.8.2.2.1}%
\contentsline {paragraph}{\numberline {8.2.2.2}printMatrix()}{15}{paragraph.8.2.2.2}%
\contentsline {paragraph}{\numberline {8.2.2.3}printRowVector()}{16}{paragraph.8.2.2.3}%
\contentsline {subsection}{\numberline {8.3}Operation Class Reference}{16}{subsection.8.3}%
\contentsline {subsubsection}{\numberline {8.3.1}Constructor \& Destructor Documentation}{18}{subsubsection.8.3.1}%
\contentsline {paragraph}{\numberline {8.3.1.1}Operation()}{19}{paragraph.8.3.1.1}%
\contentsline {subsubsection}{\numberline {8.3.2}Member Function Documentation}{19}{subsubsection.8.3.2}%
\contentsline {paragraph}{\numberline {8.3.2.1}amplitudeModeZero()}{19}{paragraph.8.3.2.1}%
\contentsline {paragraph}{\numberline {8.3.2.2}calculateDiagonalMatrices()}{20}{paragraph.8.3.2.2}%
\contentsline {paragraph}{\numberline {8.3.2.3}calculateEigenvaluePairs()}{20}{paragraph.8.3.2.3}%
\contentsline {paragraph}{\numberline {8.3.2.4}createHeatMapMatrix()}{21}{paragraph.8.3.2.4}%
\contentsline {paragraph}{\numberline {8.3.2.5}createMassMatrix()}{21}{paragraph.8.3.2.5}%
\contentsline {paragraph}{\numberline {8.3.2.6}createMassMatrixBK()}{21}{paragraph.8.3.2.6}%
\contentsline {paragraph}{\numberline {8.3.2.7}createStiffnessMatrix()}{22}{paragraph.8.3.2.7}%
\contentsline {paragraph}{\numberline {8.3.2.8}createStiffnessMatrixBK()}{22}{paragraph.8.3.2.8}%
\contentsline {paragraph}{\numberline {8.3.2.9}createStiffnessMatrixOneBlock()}{22}{paragraph.8.3.2.9}%
\contentsline {paragraph}{\numberline {8.3.2.10}createZeroEigenMatrices()}{23}{paragraph.8.3.2.10}%
\contentsline {paragraph}{\numberline {8.3.2.11}heat\_map\_main()}{23}{paragraph.8.3.2.11}%
\contentsline {paragraph}{\numberline {8.3.2.12}isSymmetric()}{24}{paragraph.8.3.2.12}%
\contentsline {paragraph}{\numberline {8.3.2.13}merge\_block\_pad()}{24}{paragraph.8.3.2.13}%
\contentsline {paragraph}{\numberline {8.3.2.14}modeZeroRemoveMean()}{24}{paragraph.8.3.2.14}%
\contentsline {paragraph}{\numberline {8.3.2.15}normalize()}{24}{paragraph.8.3.2.15}%
\contentsline {paragraph}{\numberline {8.3.2.16}saveEigenvalues()}{25}{paragraph.8.3.2.16}%
\contentsline {paragraph}{\numberline {8.3.2.17}saveHeatMapMatrices()}{25}{paragraph.8.3.2.17}%
\contentsline {paragraph}{\numberline {8.3.2.18}sortEigenvectors()}{25}{paragraph.8.3.2.18}%
\contentsline {paragraph}{\numberline {8.3.2.19}steadyStateSolutuion()}{26}{paragraph.8.3.2.19}%
\contentsline {paragraph}{\numberline {8.3.2.20}testEigen()}{26}{paragraph.8.3.2.20}%
\contentsline {subsubsection}{\numberline {8.3.3}Member Data Documentation}{26}{subsubsection.8.3.3}%
\contentsline {paragraph}{\numberline {8.3.3.1}block\_pad\_position}{26}{paragraph.8.3.3.1}%
\contentsline {paragraph}{\numberline {8.3.3.2}block\_pad\_velocity}{26}{paragraph.8.3.3.2}%
\contentsline {paragraph}{\numberline {8.3.3.3}block\_position\_to\_file}{26}{paragraph.8.3.3.3}%
\contentsline {paragraph}{\numberline {8.3.3.4}block\_velocity\_to\_file}{26}{paragraph.8.3.3.4}%
\contentsline {paragraph}{\numberline {8.3.3.5}degrees\_freedom}{26}{paragraph.8.3.3.5}%
\contentsline {paragraph}{\numberline {8.3.3.6}diagonal\_mass\_matrix}{26}{paragraph.8.3.3.6}%
\contentsline {paragraph}{\numberline {8.3.3.7}diagonal\_stiffness\_matrix}{27}{paragraph.8.3.3.7}%
\contentsline {paragraph}{\numberline {8.3.3.8}eigval}{27}{paragraph.8.3.3.8}%
\contentsline {paragraph}{\numberline {8.3.3.9}eigvec}{27}{paragraph.8.3.3.9}%
\contentsline {paragraph}{\numberline {8.3.3.10}energy\_matrix\_heat}{27}{paragraph.8.3.3.10}%
\contentsline {paragraph}{\numberline {8.3.3.11}input\_parameters}{27}{paragraph.8.3.3.11}%
\contentsline {paragraph}{\numberline {8.3.3.12}k\_c}{27}{paragraph.8.3.3.12}%
\contentsline {paragraph}{\numberline {8.3.3.13}k\_p}{27}{paragraph.8.3.3.13}%
\contentsline {paragraph}{\numberline {8.3.3.14}m\_u}{27}{paragraph.8.3.3.14}%
\contentsline {paragraph}{\numberline {8.3.3.15}mass\_matrix}{27}{paragraph.8.3.3.15}%
\contentsline {paragraph}{\numberline {8.3.3.16}number\_of\_save\_steps}{27}{paragraph.8.3.3.16}%
\contentsline {paragraph}{\numberline {8.3.3.17}pad\_friction\_to\_file}{27}{paragraph.8.3.3.17}%
\contentsline {paragraph}{\numberline {8.3.3.18}pad\_position\_to\_file}{28}{paragraph.8.3.3.18}%
\contentsline {paragraph}{\numberline {8.3.3.19}pad\_velocity\_to\_file}{28}{paragraph.8.3.3.19}%
\contentsline {paragraph}{\numberline {8.3.3.20}position\_matrix\_heat}{28}{paragraph.8.3.3.20}%
\contentsline {paragraph}{\numberline {8.3.3.21}real\_eig\_values}{28}{paragraph.8.3.3.21}%
\contentsline {paragraph}{\numberline {8.3.3.22}real\_eig\_vectors}{28}{paragraph.8.3.3.22}%
\contentsline {paragraph}{\numberline {8.3.3.23}result\_folder}{28}{paragraph.8.3.3.23}%
\contentsline {paragraph}{\numberline {8.3.3.24}result\_path}{28}{paragraph.8.3.3.24}%
\contentsline {paragraph}{\numberline {8.3.3.25}stiffness\_matrix}{28}{paragraph.8.3.3.25}%
\contentsline {paragraph}{\numberline {8.3.3.26}velocity\_matrix\_heat}{28}{paragraph.8.3.3.26}%
\contentsline {subsection}{\numberline {8.4}Parameters Class Reference}{29}{subsection.8.4}%
\contentsline {subsubsection}{\numberline {8.4.1}Constructor \& Destructor Documentation}{31}{subsubsection.8.4.1}%
\contentsline {paragraph}{\numberline {8.4.1.1}Parameters()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{32}{paragraph.8.4.1.1}%
\contentsline {paragraph}{\numberline {8.4.1.2}Parameters()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{32}{paragraph.8.4.1.2}%
\contentsline {subsubsection}{\numberline {8.4.2}Member Function Documentation}{32}{subsubsection.8.4.2}%
\contentsline {paragraph}{\numberline {8.4.2.1}assign\_parameters()}{32}{paragraph.8.4.2.1}%
\contentsline {paragraph}{\numberline {8.4.2.2}load\_file()}{33}{paragraph.8.4.2.2}%
\contentsline {paragraph}{\numberline {8.4.2.3}readDebugParameterBool()}{33}{paragraph.8.4.2.3}%
\contentsline {paragraph}{\numberline {8.4.2.4}readParameterBool()}{34}{paragraph.8.4.2.4}%
\contentsline {paragraph}{\numberline {8.4.2.5}readParameterDouble()}{34}{paragraph.8.4.2.5}%
\contentsline {paragraph}{\numberline {8.4.2.6}readParameterInt()}{34}{paragraph.8.4.2.6}%
\contentsline {paragraph}{\numberline {8.4.2.7}readParameterIntList()}{35}{paragraph.8.4.2.7}%
\contentsline {paragraph}{\numberline {8.4.2.8}readParameterString()}{35}{paragraph.8.4.2.8}%
\contentsline {paragraph}{\numberline {8.4.2.9}yamlListToArmadilloVector()}{35}{paragraph.8.4.2.9}%
\contentsline {subsubsection}{\numberline {8.4.3}Member Data Documentation}{36}{subsubsection.8.4.3}%
\contentsline {paragraph}{\numberline {8.4.3.1}calculate\_Lyapunov}{36}{paragraph.8.4.3.1}%
\contentsline {paragraph}{\numberline {8.4.3.2}debug\_continuous\_slider\_speed}{36}{paragraph.8.4.3.2}%
\contentsline {paragraph}{\numberline {8.4.3.3}debug\_damp\_all\_blocks}{36}{paragraph.8.4.3.3}%
\contentsline {paragraph}{\numberline {8.4.3.4}debug\_negative\_intial\_values}{36}{paragraph.8.4.3.4}%
\contentsline {paragraph}{\numberline {8.4.3.5}debug\_no\_damper}{36}{paragraph.8.4.3.5}%
\contentsline {paragraph}{\numberline {8.4.3.6}debug\_no\_friction}{36}{paragraph.8.4.3.6}%
\contentsline {paragraph}{\numberline {8.4.3.7}debug\_no\_min\_speed}{36}{paragraph.8.4.3.7}%
\contentsline {paragraph}{\numberline {8.4.3.8}debug\_no\_neighbor\_springs}{36}{paragraph.8.4.3.8}%
\contentsline {paragraph}{\numberline {8.4.3.9}debug\_no\_pad}{36}{paragraph.8.4.3.9}%
\contentsline {paragraph}{\numberline {8.4.3.10}debug\_no\_random\_displacements}{36}{paragraph.8.4.3.10}%
\contentsline {paragraph}{\numberline {8.4.3.11}debug\_no\_stationary\_springs}{37}{paragraph.8.4.3.11}%
\contentsline {paragraph}{\numberline {8.4.3.12}debug\_one\_degree\_freedom\_mode}{37}{paragraph.8.4.3.12}%
\contentsline {paragraph}{\numberline {8.4.3.13}debug\_only\_negative\_initial}{37}{paragraph.8.4.3.13}%
\contentsline {paragraph}{\numberline {8.4.3.14}debug\_only\_write\_friction}{37}{paragraph.8.4.3.14}%
\contentsline {paragraph}{\numberline {8.4.3.15}debug\_pad\_as\_block}{37}{paragraph.8.4.3.15}%
\contentsline {paragraph}{\numberline {8.4.3.16}debug\_parameters}{37}{paragraph.8.4.3.16}%
\contentsline {paragraph}{\numberline {8.4.3.17}debug\_print}{37}{paragraph.8.4.3.17}%
\contentsline {paragraph}{\numberline {8.4.3.18}debug\_print\_event}{37}{paragraph.8.4.3.18}%
\contentsline {paragraph}{\numberline {8.4.3.19}debug\_special\_phi}{37}{paragraph.8.4.3.19}%
\contentsline {paragraph}{\numberline {8.4.3.20}debug\_stick\_blocks}{37}{paragraph.8.4.3.20}%
\contentsline {paragraph}{\numberline {8.4.3.21}debug\_stop\_slider}{37}{paragraph.8.4.3.21}%
\contentsline {paragraph}{\numberline {8.4.3.22}debug\_write\_blocks}{38}{paragraph.8.4.3.22}%
\contentsline {paragraph}{\numberline {8.4.3.23}dt}{38}{paragraph.8.4.3.23}%
\contentsline {paragraph}{\numberline {8.4.3.24}end\_speed\_continuous}{38}{paragraph.8.4.3.24}%
\contentsline {paragraph}{\numberline {8.4.3.25}file\_name}{38}{paragraph.8.4.3.25}%
\contentsline {paragraph}{\numberline {8.4.3.26}increment}{38}{paragraph.8.4.3.26}%
\contentsline {paragraph}{\numberline {8.4.3.27}interval}{38}{paragraph.8.4.3.27}%
\contentsline {paragraph}{\numberline {8.4.3.28}loggedBlocks}{38}{paragraph.8.4.3.28}%
\contentsline {paragraph}{\numberline {8.4.3.29}lyapunov\_delta}{38}{paragraph.8.4.3.29}%
\contentsline {paragraph}{\numberline {8.4.3.30}m\_alpha}{38}{paragraph.8.4.3.30}%
\contentsline {paragraph}{\numberline {8.4.3.31}m\_delta}{38}{paragraph.8.4.3.31}%
\contentsline {paragraph}{\numberline {8.4.3.32}m\_eta}{38}{paragraph.8.4.3.32}%
\contentsline {paragraph}{\numberline {8.4.3.33}m\_F0}{39}{paragraph.8.4.3.33}%
\contentsline {paragraph}{\numberline {8.4.3.34}m\_k\_P0}{39}{paragraph.8.4.3.34}%
\contentsline {paragraph}{\numberline {8.4.3.35}m\_mass\_x}{39}{paragraph.8.4.3.35}%
\contentsline {paragraph}{\numberline {8.4.3.36}m\_scale\_C}{39}{paragraph.8.4.3.36}%
\contentsline {paragraph}{\numberline {8.4.3.37}m\_scale\_mass}{39}{paragraph.8.4.3.37}%
\contentsline {paragraph}{\numberline {8.4.3.38}m\_scale\_P}{39}{paragraph.8.4.3.38}%
\contentsline {paragraph}{\numberline {8.4.3.39}m\_sigma}{39}{paragraph.8.4.3.39}%
\contentsline {paragraph}{\numberline {8.4.3.40}m\_t}{39}{paragraph.8.4.3.40}%
\contentsline {paragraph}{\numberline {8.4.3.41}m\_u\_min}{39}{paragraph.8.4.3.41}%
\contentsline {paragraph}{\numberline {8.4.3.42}m\_v0}{39}{paragraph.8.4.3.42}%
\contentsline {paragraph}{\numberline {8.4.3.43}m\_zeta}{39}{paragraph.8.4.3.43}%
\contentsline {paragraph}{\numberline {8.4.3.44}max\_time}{40}{paragraph.8.4.3.44}%
\contentsline {paragraph}{\numberline {8.4.3.45}N}{40}{paragraph.8.4.3.45}%
\contentsline {paragraph}{\numberline {8.4.3.46}num\_events}{40}{paragraph.8.4.3.46}%
\contentsline {paragraph}{\numberline {8.4.3.47}parameters}{40}{paragraph.8.4.3.47}%
\contentsline {paragraph}{\numberline {8.4.3.48}progress\_indicator}{40}{paragraph.8.4.3.48}%
\contentsline {paragraph}{\numberline {8.4.3.49}rayleigh\_damping}{40}{paragraph.8.4.3.49}%
\contentsline {paragraph}{\numberline {8.4.3.50}read\_parameters}{40}{paragraph.8.4.3.50}%
\contentsline {paragraph}{\numberline {8.4.3.51}save\_interval\_dt}{40}{paragraph.8.4.3.51}%
\contentsline {paragraph}{\numberline {8.4.3.52}seed}{40}{paragraph.8.4.3.52}%
\contentsline {paragraph}{\numberline {8.4.3.53}slider\_speed}{40}{paragraph.8.4.3.53}%
\contentsline {paragraph}{\numberline {8.4.3.54}start\_speed\_continuous}{40}{paragraph.8.4.3.54}%
\contentsline {paragraph}{\numberline {8.4.3.55}threshold\_speed}{41}{paragraph.8.4.3.55}%
\contentsline {subsection}{\numberline {8.5}Simulation Class Reference}{41}{subsection.8.5}%
\contentsline {subsubsection}{\numberline {8.5.1}Constructor \& Destructor Documentation}{44}{subsubsection.8.5.1}%
\contentsline {paragraph}{\numberline {8.5.1.1}Simulation()\hspace {0.1cm}{\footnotesize \ttfamily [1/3]}}{44}{paragraph.8.5.1.1}%
\contentsline {paragraph}{\numberline {8.5.1.2}Simulation()\hspace {0.1cm}{\footnotesize \ttfamily [2/3]}}{45}{paragraph.8.5.1.2}%
\contentsline {paragraph}{\numberline {8.5.1.3}Simulation()\hspace {0.1cm}{\footnotesize \ttfamily [3/3]}}{45}{paragraph.8.5.1.3}%
\contentsline {subsubsection}{\numberline {8.5.2}Member Function Documentation}{45}{subsubsection.8.5.2}%
\contentsline {paragraph}{\numberline {8.5.2.1}calculateDiagonalMatrices()}{45}{paragraph.8.5.2.1}%
\contentsline {paragraph}{\numberline {8.5.2.2}calculateEigenvaluePairs()}{45}{paragraph.8.5.2.2}%
\contentsline {paragraph}{\numberline {8.5.2.3}calculateFrictionInParallel()}{45}{paragraph.8.5.2.3}%
\contentsline {paragraph}{\numberline {8.5.2.4}calculatePadFriction()}{46}{paragraph.8.5.2.4}%
\contentsline {paragraph}{\numberline {8.5.2.5}calculatePadSupportDamper()}{46}{paragraph.8.5.2.5}%
\contentsline {paragraph}{\numberline {8.5.2.6}calculateStick()}{46}{paragraph.8.5.2.6}%
\contentsline {paragraph}{\numberline {8.5.2.7}copyYamlFile()}{47}{paragraph.8.5.2.7}%
\contentsline {paragraph}{\numberline {8.5.2.8}createFolder()}{47}{paragraph.8.5.2.8}%
\contentsline {paragraph}{\numberline {8.5.2.9}createMassMatrix()}{47}{paragraph.8.5.2.9}%
\contentsline {paragraph}{\numberline {8.5.2.10}createStiffnessMatrix()}{47}{paragraph.8.5.2.10}%
\contentsline {paragraph}{\numberline {8.5.2.11}function\_u()}{47}{paragraph.8.5.2.11}%
\contentsline {paragraph}{\numberline {8.5.2.12}function\_x()}{48}{paragraph.8.5.2.12}%
\contentsline {paragraph}{\numberline {8.5.2.13}function\_x\_odof()}{49}{paragraph.8.5.2.13}%
\contentsline {paragraph}{\numberline {8.5.2.14}increaseStick()}{49}{paragraph.8.5.2.14}%
\contentsline {paragraph}{\numberline {8.5.2.15}init()}{49}{paragraph.8.5.2.15}%
\contentsline {paragraph}{\numberline {8.5.2.16}isSymmetric()}{50}{paragraph.8.5.2.16}%
\contentsline {paragraph}{\numberline {8.5.2.17}midpointMethod()}{50}{paragraph.8.5.2.17}%
\contentsline {paragraph}{\numberline {8.5.2.18}midpointMethodParallel()}{51}{paragraph.8.5.2.18}%
\contentsline {paragraph}{\numberline {8.5.2.19}oneDegreeOfFreedomMidpointMethod()}{52}{paragraph.8.5.2.19}%
\contentsline {paragraph}{\numberline {8.5.2.20}phi()}{52}{paragraph.8.5.2.20}%
\contentsline {paragraph}{\numberline {8.5.2.21}printMidPointMethod()}{52}{paragraph.8.5.2.21}%
\contentsline {paragraph}{\numberline {8.5.2.22}runMidpointMethod()}{53}{paragraph.8.5.2.22}%
\contentsline {paragraph}{\numberline {8.5.2.23}saveEigenvalues()}{53}{paragraph.8.5.2.23}%
\contentsline {paragraph}{\numberline {8.5.2.24}saveSteadyState()}{53}{paragraph.8.5.2.24}%
\contentsline {paragraph}{\numberline {8.5.2.25}saveToCsv()}{54}{paragraph.8.5.2.25}%
\contentsline {paragraph}{\numberline {8.5.2.26}saveToCsvParallel()}{54}{paragraph.8.5.2.26}%
\contentsline {paragraph}{\numberline {8.5.2.27}setSpeedAfterThreshold()}{54}{paragraph.8.5.2.27}%
\contentsline {paragraph}{\numberline {8.5.2.28}setupAndRunFrictionInParallel()}{55}{paragraph.8.5.2.28}%
\contentsline {paragraph}{\numberline {8.5.2.29}setupBlockMatrix()}{55}{paragraph.8.5.2.29}%
\contentsline {paragraph}{\numberline {8.5.2.30}setupBlockMatrixDerivative()}{55}{paragraph.8.5.2.30}%
\contentsline {paragraph}{\numberline {8.5.2.31}setupConstants()}{56}{paragraph.8.5.2.31}%
\contentsline {paragraph}{\numberline {8.5.2.32}setupLyapunov()}{56}{paragraph.8.5.2.32}%
\contentsline {paragraph}{\numberline {8.5.2.33}setupModelVectorsMatrices()}{56}{paragraph.8.5.2.33}%
\contentsline {paragraph}{\numberline {8.5.2.34}setupPadVector()}{57}{paragraph.8.5.2.34}%
\contentsline {paragraph}{\numberline {8.5.2.35}setupPadVectorDerivative()}{58}{paragraph.8.5.2.35}%
\contentsline {paragraph}{\numberline {8.5.2.36}setupSaveVectorsMatrices()}{58}{paragraph.8.5.2.36}%
\contentsline {paragraph}{\numberline {8.5.2.37}setupSliderSpeedMode()}{58}{paragraph.8.5.2.37}%
\contentsline {paragraph}{\numberline {8.5.2.38}steadyStateSolutuion()}{58}{paragraph.8.5.2.38}%
\contentsline {paragraph}{\numberline {8.5.2.39}testEigen()}{59}{paragraph.8.5.2.39}%
\contentsline {paragraph}{\numberline {8.5.2.40}updateSliderSpeed()}{59}{paragraph.8.5.2.40}%
\contentsline {paragraph}{\numberline {8.5.2.41}writeValuesToLoggers()}{59}{paragraph.8.5.2.41}%
\contentsline {subsubsection}{\numberline {8.5.3}Member Data Documentation}{60}{subsubsection.8.5.3}%
\contentsline {paragraph}{\numberline {8.5.3.1}alpha}{60}{paragraph.8.5.3.1}%
\contentsline {paragraph}{\numberline {8.5.3.2}block\_matrix}{60}{paragraph.8.5.3.2}%
\contentsline {paragraph}{\numberline {8.5.3.3}block\_matrix\_derivative}{60}{paragraph.8.5.3.3}%
\contentsline {paragraph}{\numberline {8.5.3.4}block\_matrix\_derivative\_lyapunov}{60}{paragraph.8.5.3.4}%
\contentsline {paragraph}{\numberline {8.5.3.5}block\_matrix\_lyapunov}{60}{paragraph.8.5.3.5}%
\contentsline {paragraph}{\numberline {8.5.3.6}block\_position\_column\_index}{60}{paragraph.8.5.3.6}%
\contentsline {paragraph}{\numberline {8.5.3.7}block\_position\_to\_file}{60}{paragraph.8.5.3.7}%
\contentsline {paragraph}{\numberline {8.5.3.8}block\_velocity\_column\_index}{60}{paragraph.8.5.3.8}%
\contentsline {paragraph}{\numberline {8.5.3.9}block\_velocity\_to\_file}{60}{paragraph.8.5.3.9}%
\contentsline {paragraph}{\numberline {8.5.3.10}c\_p}{60}{paragraph.8.5.3.10}%
\contentsline {paragraph}{\numberline {8.5.3.11}delta\_gamma\_new}{61}{paragraph.8.5.3.11}%
\contentsline {paragraph}{\numberline {8.5.3.12}delta\_gamma\_prev}{61}{paragraph.8.5.3.12}%
\contentsline {paragraph}{\numberline {8.5.3.13}eigval}{61}{paragraph.8.5.3.13}%
\contentsline {paragraph}{\numberline {8.5.3.14}eigvec}{61}{paragraph.8.5.3.14}%
\contentsline {paragraph}{\numberline {8.5.3.15}friction\_zero\_counter}{61}{paragraph.8.5.3.15}%
\contentsline {paragraph}{\numberline {8.5.3.16}index\_pad\_friction\_to\_file}{61}{paragraph.8.5.3.16}%
\contentsline {paragraph}{\numberline {8.5.3.17}index\_to\_file}{61}{paragraph.8.5.3.17}%
\contentsline {paragraph}{\numberline {8.5.3.18}input\_parameters}{61}{paragraph.8.5.3.18}%
\contentsline {paragraph}{\numberline {8.5.3.19}input\_parameters\_path}{61}{paragraph.8.5.3.19}%
\contentsline {paragraph}{\numberline {8.5.3.20}k\_c}{61}{paragraph.8.5.3.20}%
\contentsline {paragraph}{\numberline {8.5.3.21}k\_p}{61}{paragraph.8.5.3.21}%
\contentsline {paragraph}{\numberline {8.5.3.22}loggingSpecificBlocks}{62}{paragraph.8.5.3.22}%
\contentsline {paragraph}{\numberline {8.5.3.23}m\_c\_crit}{62}{paragraph.8.5.3.23}%
\contentsline {paragraph}{\numberline {8.5.3.24}m\_u}{62}{paragraph.8.5.3.24}%
\contentsline {paragraph}{\numberline {8.5.3.25}number\_of\_interval\_save\_step}{62}{paragraph.8.5.3.25}%
\contentsline {paragraph}{\numberline {8.5.3.26}number\_of\_save\_steps}{62}{paragraph.8.5.3.26}%
\contentsline {paragraph}{\numberline {8.5.3.27}number\_of\_time\_steps}{62}{paragraph.8.5.3.27}%
\contentsline {paragraph}{\numberline {8.5.3.28}pad\_friction\_to\_file}{62}{paragraph.8.5.3.28}%
\contentsline {paragraph}{\numberline {8.5.3.29}pad\_position\_to\_file}{62}{paragraph.8.5.3.29}%
\contentsline {paragraph}{\numberline {8.5.3.30}pad\_vector}{62}{paragraph.8.5.3.30}%
\contentsline {paragraph}{\numberline {8.5.3.31}pad\_vector\_derivative}{62}{paragraph.8.5.3.31}%
\contentsline {paragraph}{\numberline {8.5.3.32}pad\_vector\_derivative\_lyapunov}{62}{paragraph.8.5.3.32}%
\contentsline {paragraph}{\numberline {8.5.3.33}pad\_vector\_lyapunov}{63}{paragraph.8.5.3.33}%
\contentsline {paragraph}{\numberline {8.5.3.34}pad\_velocity\_to\_file}{63}{paragraph.8.5.3.34}%
\contentsline {paragraph}{\numberline {8.5.3.35}real\_eig\_values}{63}{paragraph.8.5.3.35}%
\contentsline {paragraph}{\numberline {8.5.3.36}real\_eig\_vectors}{63}{paragraph.8.5.3.36}%
\contentsline {paragraph}{\numberline {8.5.3.37}result\_folder}{63}{paragraph.8.5.3.37}%
\contentsline {paragraph}{\numberline {8.5.3.38}result\_path}{63}{paragraph.8.5.3.38}%
\contentsline {paragraph}{\numberline {8.5.3.39}stick\_counter}{63}{paragraph.8.5.3.39}%
\contentsline {paragraph}{\numberline {8.5.3.40}total\_stick\_counter}{63}{paragraph.8.5.3.40}%
\contentsline {paragraph}{\numberline {8.5.3.41}updateSliderSpeedDtInterval}{63}{paragraph.8.5.3.41}%
\contentsline {paragraph}{\numberline {8.5.3.42}yaml\_path}{63}{paragraph.8.5.3.42}%
\contentsline {section}{\numberline {9}File Documentation}{64}{section.9}%
\contentsline {subsection}{\numberline {9.1}include/operations.hpp File Reference}{64}{subsection.9.1}%
\contentsline {subsubsection}{\numberline {9.1.1}Macro Definition Documentation}{65}{subsubsection.9.1.1}%
\contentsline {paragraph}{\numberline {9.1.1.1}OPERATION}{65}{paragraph.9.1.1.1}%
\contentsline {subsection}{\numberline {9.2}include/parameters.hpp File Reference}{65}{subsection.9.2}%
\contentsline {subsection}{\numberline {9.3}include/simulation.hpp File Reference}{66}{subsection.9.3}%
\contentsline {subsection}{\numberline {9.4}R\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}E\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}A\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}D\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}M\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}E.\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}md File Reference}{66}{subsection.9.4}%
\contentsline {subsection}{\numberline {9.5}src/simulation/debug\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}functions.cpp File Reference}{66}{subsection.9.5}%
\contentsline {subsection}{\numberline {9.6}src/simulation/debug\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}parameters.cpp File Reference}{67}{subsection.9.6}%
\contentsline {subsection}{\numberline {9.7}src/simulation/main.cpp File Reference}{68}{subsection.9.7}%
\contentsline {subsubsection}{\numberline {9.7.1}Function Documentation}{68}{subsubsection.9.7.1}%
\contentsline {paragraph}{\numberline {9.7.1.1}main()}{68}{paragraph.9.7.1.1}%
\contentsline {subsection}{\numberline {9.8}src/simulation/operation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}eigen.cpp File Reference}{69}{subsection.9.8}%
\contentsline {subsection}{\numberline {9.9}src/simulation/operation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}heat\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}map.cpp File Reference}{70}{subsection.9.9}%
\contentsline {subsection}{\numberline {9.10}src/simulation/operations.cpp File Reference}{70}{subsection.9.10}%
\contentsline {subsection}{\numberline {9.11}src/simulation/parameters.cpp File Reference}{71}{subsection.9.11}%
\contentsline {subsection}{\numberline {9.12}src/simulation/readme.md File Reference}{72}{subsection.9.12}%
\contentsline {subsection}{\numberline {9.13}src/simulation/simulation.cpp File Reference}{72}{subsection.9.13}%
\contentsline {subsection}{\numberline {9.14}src/simulation/simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}help\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}functions.cpp File Reference}{72}{subsection.9.14}%
\contentsline {subsection}{\numberline {9.15}src/simulation/simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}midpoint.cpp File Reference}{73}{subsection.9.15}%
\contentsline {subsection}{\numberline {9.16}src/simulation/simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}numerical.cpp File Reference}{73}{subsection.9.16}%
\contentsline {subsection}{\numberline {9.17}src/simulation/simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}parallel\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}friction.cpp File Reference}{74}{subsection.9.17}%
\contentsline {subsection}{\numberline {9.18}src/simulation/simulation\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}setup.cpp File Reference}{75}{subsection.9.18}%
\contentsline {subsection}{\numberline {9.19}src/simulation/sort\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\mbox {\scriptsize $\DOTSB \leftarrow \joinrel \rhook $}}{}{}eigenvalues.py File Reference}{75}{subsection.9.19}%
