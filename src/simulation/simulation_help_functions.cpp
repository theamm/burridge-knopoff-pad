#include"simulation.hpp"
#include <sys/stat.h>
#include <sys/types.h>
#include <sstream>

//      START: HELP FUNCTIONS CALCULATION       //

/**
* \f$\phi(y) =
	\begin{cases}
		\begin{aligned}
		&F_{0}[-1,1], \quad &y = 0\\
		&F_{0}\frac{1-\sigma}{1+\frac{|y|}{1-\sigma}}sign(y), \quad &y \neq 0
		\end{aligned}
	\end{cases}\f$
*/

double Simulation::phi(double y)
{
	if (y == 0){
        friction_zero_counter += 1;
        printf("[Friction zero counter: %i]", friction_zero_counter);
        return 0; // Maximum static friction force is handled elsewhere
    }
	return (y < 0 ? -1 : 1)*(1 - input_parameters.m_sigma) / (1 + abs(y) / (1 - input_parameters.m_sigma));

}

void Simulation::setSpeedAfterThreshold(){
    //if(input_parameters.slider_speed > input_parameters.threshold_speed && input_parameters.debug_continuous_slider_speed){
    //    updateSliderSpeedDtInterval = 1e8;
    // }
    updateSliderSpeedDtInterval = 1;
}

void Simulation::updateSliderSpeed(int time_step)
{
    if(time_step % updateSliderSpeedDtInterval == 0)
    {
       // printf("[Slider speed: %.6f]",input_parameters.slider_speed);
        setSpeedAfterThreshold();
        if(!input_parameters.debug_continuous_slider_speed)
        {
            if(time_step % input_parameters.save_interval_dt == 0)
            {
                saveToCsvParallel(pad_friction_to_file, this->result_path, input_parameters.slider_speed);
            }
            index_pad_friction_to_file = 0;
            //std::cout << input_parameters.slider_speed << std::endl;
        }
        input_parameters.slider_speed += input_parameters.increment;
    }
}

void Simulation::saveToCsvParallel(const arma::colvec& armadillo_vector, const string& result_path, const double& slider_speed){
    std::string str = result_path + input_parameters.file_name + "_midpoint_pad_friction" + to_string(slider_speed) + ".csv";
    armadillo_vector.save(str, arma::csv_ascii);
}

void Simulation::increaseStick(double velocity){
    if((std::round(velocity*1000)/1000) == -(std::round(input_parameters.slider_speed*1000)/1000)){
        this->stick_counter += 1;
    }
    this->total_stick_counter += 1;
}

double Simulation::calculateStick(){
    return ((double)this->stick_counter/(input_parameters.N * number_of_time_steps));
}


void Simulation::createFolder(std::string folder){   
    std::stringstream ss;
    ss << folder;
    int rc = mkdir((this->result_path+ss.str()).c_str(), 0777);
    if(rc == 0) std::cout << "Created " << ss.str() << " success\n";
} 

void Simulation::copyYamlFile(){
    YAML::Node read_parameters = YAML::LoadFile(this->yaml_path);
    std::ofstream fout(this->result_path+"parameters.yaml"); 
    fout << read_parameters;
}


void Simulation::steadyStateSolutuion(){
    // int number_of_time_steps = input_parameters.max_time / input_parameters.dt;
    // int number_of_save_steps = floor(input_parameters.max_time / (input_parameters.dt * input_parameters.save_interval_dt));
    int n = arma::size(block_position_to_file)[0];
    arma::mat steady_state;
    steady_state.set_size(n, number_of_save_steps);
    for (int i = 0; i < n; i ++){
        for (int j = 0; j < number_of_save_steps; j ++){
        steady_state(i, j) = (
                    k_c * (i == 0 ? 0 : (block_position_to_file(i, j - 1) - block_position_to_file(i, j)))
                    + k_c * (i == n - 1 ? 0 : (block_position_to_file(i, j + 1) - block_position_to_file(i, j))) 
                    +
                    - pad_friction_to_file(j)) 
                    / k_p;
        }
    }
    arma::mat block_position_steady;
    block_position_steady.set_size(n, number_of_save_steps);
    block_position_steady = block_position_to_file - steady_state;
    arma::colvec pad_position_steady;
    pad_position_steady.set_size(number_of_save_steps);
    pad_position_steady = pad_position_to_file + steady_state;
    saveSteadyState(input_parameters.file_name, this->result_path, block_position_steady, pad_position_steady);
}

void Simulation::saveSteadyState(string filename, string path_result, arma::mat block_position_steady, arma::vec pad_position_steady){
        pad_position_steady.save(path_result + filename + "_pad_position_steady.csv", arma::csv_ascii);
        block_position_steady.save(path_result + filename + "_block_positon_steady.csv", arma::csv_ascii);
}
