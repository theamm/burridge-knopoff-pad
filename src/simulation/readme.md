# src/simulation

## Description
All C++ scripts and the makefile used to simulate the model.
Read the main readme-file before this. 
The code is split up into three classes
    1. Simulation: used to simulate the BKP model.
    2. Parameters: used to input the parameters.
    3. Operation: used to compute mode shapes and modal contributions.

### Class Simulation

#### simulation.cpp
Containins initialisation methods

#### simulation_setup.cpp
Contains setup-methods

#### midpoint.cpp
Contains methods for running the midpoint method

#### numerical.cpp
Contains the numerical implementation of the Burridge-Knopoff-Pad model

#### simulation_paralell_friction.cpp
Contains code to run simulation code in parallell. Not used in the work with this thesis.

#### suimulation_help_functions.cpp
Contains functions to do smaller operations such as creating folder and calculating stick percentage.

### Class Parameters

#### parameters.cpp
Contains the code to read yaml parameters and add them to the parameters object.

### Class Operations

#### operations.cpp
Takes a simulation and parameters object and does eigen analisys operations.

#### operation_eigen.cpp
Methods to calulate and save eigenvecotors and eigenvalues.

#### operation_heat_map.cpp
Contains methods to create heat map matrices from the eigenvectors and displacemment and velocity matrices.

#### sort_eigenvalues.py
Python program to sort the eigenvalues and corresponing eigenvecotrs in correct order.

### makefile
Used to compile the program.

## Installation 

### Simulations packages (c++)
- yaml
- eigen
- openmp
- larmadillo

### Compilation 
Navigate to the simulation folder

`cd src/simulation`

Compile 

`make`

To run

`./main`

To run with the creation of eigenvalues, eigenvectors and displacememnt per mode shape:

`./main -e`

Delete object-files:

`make clean`

#### Problems?
As stated in the thesis, the compilation is done using the makefile under src/simulation.
If the program dosent compile, first make sure to have download the neceserry dependencies. If there are errors, try to add more flags behind CXXFLAGS according to the error message. Another option is to install and use cmake to compile.
