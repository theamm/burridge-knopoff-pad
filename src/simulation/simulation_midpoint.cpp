#include"simulation.hpp"

/**
 * \f$\begin{aligned}
	y^{*}  &= y^{n} + \frac{\Delta t}{2} f(y^{n}),\\
	y^{n+1} &= y^{n} + \Delta t f(y^{*})
	\end{aligned}
    \f$
 * \f$\vec{y}=
	\begin{bmatrix}
	y \\ \dot{y}
	\end{bmatrix}
	\quad
	\dot{\vec{y}} = f(\vec{y}) =
	\begin{bmatrix}
	\dot{y} \\ \ddot{y}
	\end{bmatrix}
    \f$
 */

void Simulation::midpointMethod(int time_step)
{
    arma::mat block_matrix_half_step = block_matrix;
    arma::rowvec pad_vector_half_step = pad_vector;
    
    function_u(block_matrix, pad_vector, block_matrix_derivative);
    function_x(block_matrix, pad_vector, pad_vector_derivative);

    block_matrix_half_step = block_matrix + 0.5 * input_parameters.dt * block_matrix_derivative;
    pad_vector_half_step = pad_vector + 0.5 * input_parameters.dt * pad_vector_derivative;
    
    function_u(block_matrix_half_step, pad_vector_half_step, block_matrix_derivative);
    function_x(block_matrix_half_step, pad_vector_half_step, pad_vector_derivative);
    
    block_matrix = block_matrix + input_parameters.dt * block_matrix_derivative;
    pad_vector = pad_vector + input_parameters.dt * pad_vector_derivative;

    for(int i = 0; i < block_matrix.n_rows; i++){
        increaseStick((double)block_matrix(i, 1));
    }

    if(time_step % input_parameters.save_interval_dt == 0)
    {
        writeValuesToLoggers(time_step);
    }

    if(input_parameters.calculate_Lyapunov){
        arma::mat block_matrix_half_step_lyapunov = block_matrix_lyapunov;
        arma::rowvec pad_vector_half_step_lyapunov = pad_vector_lyapunov;
        
        function_u(block_matrix_lyapunov, pad_vector_lyapunov, block_matrix_derivative_lyapunov);
        function_x(block_matrix_lyapunov, pad_vector_lyapunov, pad_vector_derivative_lyapunov);

        block_matrix_half_step_lyapunov = block_matrix_lyapunov + 0.5 * input_parameters.dt * block_matrix_derivative_lyapunov;
        pad_vector_half_step_lyapunov = pad_vector_lyapunov + 0.5 * input_parameters.dt * pad_vector_derivative_lyapunov;
        
        function_u(block_matrix_half_step_lyapunov, pad_vector_half_step_lyapunov, block_matrix_derivative_lyapunov);
        function_x(block_matrix_half_step_lyapunov, pad_vector_half_step_lyapunov, pad_vector_derivative_lyapunov);
        
        block_matrix_lyapunov = block_matrix_lyapunov + input_parameters.dt * block_matrix_derivative_lyapunov;
        pad_vector_lyapunov = pad_vector_lyapunov + input_parameters.dt * pad_vector_derivative_lyapunov;

        delta_gamma_new = sum(block_matrix_lyapunov.col(0)) + sum(block_matrix_lyapunov.col(1)) + pad_vector_lyapunov(0) + pad_vector_lyapunov(1);
        alpha += log(abs(delta_gamma_new)/abs(delta_gamma_prev));
    }
}

void Simulation::oneDegreeOfFreedomMidpointMethod(
    int time_step
){
    arma::rowvec pad_vector_half_step = pad_vector;

    function_x_odof(pad_vector, pad_vector_derivative);

    pad_vector_half_step = pad_vector + 0.5 * input_parameters.dt * pad_vector_derivative;

    function_x_odof(pad_vector_half_step, pad_vector_derivative);

    pad_vector = pad_vector + input_parameters.dt * pad_vector_derivative;

    if(time_step % input_parameters.save_interval_dt == 0)
    {
        writeValuesToLoggers(time_step);
    }

}

void Simulation::writeValuesToLoggers(const int& time_step){
    if(!input_parameters.debug_only_write_friction)
    {
        if(loggingSpecificBlocks)
        {
            block_position_to_file.col(index_to_file) = block_matrix(input_parameters.loggedBlocks, block_position_column_index);
            block_velocity_to_file.col(index_to_file) = block_matrix(input_parameters.loggedBlocks, block_velocity_column_index);
        }else
        {
            block_position_to_file.col(index_to_file) = block_matrix.col(0);
            block_velocity_to_file.col(index_to_file) = block_matrix.col(1);
        }
        pad_position_to_file(index_to_file) = pad_vector(0);
        pad_velocity_to_file(index_to_file) = pad_vector(1);
    }
    pad_friction_to_file(index_pad_friction_to_file) = calculatePadFriction(block_matrix, pad_vector);
    index_to_file +=1;
    index_pad_friction_to_file += 1;
}

void Simulation::printMidPointMethod(arma::mat block_matrix, arma::rowvec pad_vector, string explanation_text)
{
    block_matrix.print("Block matrix" + explanation_text + ": ");
    pad_vector.print("Pad vector" + explanation_text + ": ");
}

void Simulation::runMidpointMethod()
{
    double percentageTransformer = 100.0/(double)number_of_time_steps;
    cout << "\n\n" << endl;
    for(int i = 0; i < number_of_time_steps; i++)
    {
        if(i % 1000 == 0)
        {
            printf("\r[Percentage complete: %.0f%%]",(double)i*percentageTransformer);
        }
        
        if(!input_parameters.debug_one_degree_freedom_mode)
        {
            midpointMethod(i);
        }else
        {
            oneDegreeOfFreedomMidpointMethod(i);
        }
        updateSliderSpeed(i+1);
    }
    cout << "\n\n" << endl;
    saveToCsv(input_parameters.file_name, this->result_path);
    
    if(input_parameters.calculate_Lyapunov){
        double lyapunov = alpha / ((double)number_of_time_steps);
        std::cout << "Lyapunov: " << lyapunov <<std::endl;
    }
}

void Simulation::saveToCsv(string filename, string path_result)
{
    if(!input_parameters.debug_only_write_friction)
    {
        block_position_to_file.save(path_result + filename + "_midpoint_block_position.csv", arma::csv_ascii);
        block_velocity_to_file.save(path_result + filename + "_midpoint_block_velocity.csv", arma::csv_ascii);
        pad_position_to_file.save(path_result + filename + "_midpoint_pad_position.csv", arma::csv_ascii);
        pad_velocity_to_file.save(path_result + filename + "_midpoint_pad_velocity.csv", arma::csv_ascii);
    }
    if(input_parameters.debug_continuous_slider_speed)
    {
        pad_friction_to_file.save(path_result + filename + "_midpoint_pad_friction.csv", arma::csv_ascii);
        std::ofstream outfile (path_result + filename + "_midpoint_total_stick_fraction.txt");
        outfile << calculateStick() << std::endl;
        outfile.close();
    }
}