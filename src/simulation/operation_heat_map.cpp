#include"operations.hpp"
#include <iostream>
#include <limits>

/**
 * Main method to run all necesarry methods to create heat map matrices.
 */
void Operation::heat_map_main(){
    merge_block_pad();
    createZeroEigenMatrices();
    createHeatMapMatrix();
    saveHeatMapMatrices();
    modeZeroRemoveMean();
}

/**
 * Merging the block and pad position and velcoity matrices. 
 */
void Operation::merge_block_pad(){
    if(!input_parameters.debug_no_pad){
        block_pad_position = arma::join_cols(block_position_to_file, pad_position_to_file.t()); 
        block_pad_velocity = arma::join_cols(block_velocity_to_file, pad_velocity_to_file.t());   
    }
    else{
        block_pad_position = block_position_to_file;
        block_pad_velocity = block_velocity_to_file;
    }
}

/**
 * Creating matrices that gives the modal contribution to displacement, velocity and energy over time.
 * 
 */
void Operation::createHeatMapMatrix(){
    std::cout << "creating heat map matrices, this can take some time" << std::endl;
    int n = this->degrees_freedom+1;
    int timesteps = number_of_save_steps;

    for (int i = 0; i < n ; i ++){

        arma::vec eigen_vector = real_eig_vectors.col(i);
        arma::vec position_amplitude_vector = arma::zeros<arma::colvec>(number_of_save_steps);
        arma::vec velcity_amplitude_vector = arma::zeros<arma::colvec>(number_of_save_steps);
        arma::vec energy_amplitude_vecotor = arma::zeros<arma::colvec>(number_of_save_steps);
        for (int j = 0; j < timesteps; j ++){
            double position_amplitude = arma::dot(eigen_vector, block_pad_position.col(j));
            double velocity_amplitude = arma::dot(eigen_vector, block_pad_velocity.col(j));
            double e = position_amplitude;
            double edot =  velocity_amplitude;
            
            double energy_amplitude = 0.5*double(diagonal_mass_matrix(i,i))*pow(edot, 2) + 0.5*double(diagonal_stiffness_matrix(i,i)) * pow(e,2);

            position_amplitude_vector(j) = position_amplitude;
            velcity_amplitude_vector(j) = velocity_amplitude;
            energy_amplitude_vecotor(j) = energy_amplitude;
        };
        position_matrix_heat.row(i) = position_amplitude_vector.t();
        velocity_matrix_heat.row(i) = velcity_amplitude_vector.t();
        energy_matrix_heat.row(i) = energy_amplitude_vecotor.t();
    };
};

/**
 * Initilised matrices to only zeroes.
 * ToDO: Make this more efficient by using a more "best practice"-method to initilise matrices.
 */
void Operation::createZeroEigenMatrices(){
    position_matrix_heat = arma::zeros<arma::mat>(this->degrees_freedom+1, number_of_save_steps);
    velocity_matrix_heat = arma::zeros<arma::mat>(this->degrees_freedom+1, number_of_save_steps);
    energy_matrix_heat = arma::zeros<arma::mat>(this->degrees_freedom+1, number_of_save_steps);
}

/**
 * Saves the heat map matrices to .csv-files.
 */
void Operation::saveHeatMapMatrices(){
    std::string path =  this->result_path + this->input_parameters.file_name;
    position_matrix_heat.save(path + "_position_matrix_heat" + ".csv", arma::csv_ascii);
    velocity_matrix_heat.save(path + "_velocity_matrix_heat" + ".csv", arma::csv_ascii);
    energy_matrix_heat.save(path + "_energy_matrix_heat" + ".csv", arma::csv_ascii);
}

/**
 * Mothod not working as supposed to.
 * Indented to create state solution of heat-matrix. 
 * ToDO: Make the function work.
 */
arma::mat Operation::steadyStateSolutuion(){
    int n = arma::size(block_position_to_file)[0];
    arma::mat steady_state(n, number_of_save_steps);

    for (int i = 0; i < n; i ++){
        for (int j = 0; j < number_of_save_steps; j ++){
        steady_state(i, j) =
                    pad_friction_to_file(j)/k_p;
                    /*(k_c * (j == 0 ? 0 : (block_position_to_file(i, j - 1) - block_position_to_file(i, j)))
                    + k_c * (j == number_of_save_steps - 1 ? 0 : (block_position_to_file(i, j + 1) - block_position_to_file(i, j))) 
                    - pad_friction_to_file(j)) 
                    / k_p;
                    */
        }
    }
    arma::mat block_position_steady(n, number_of_save_steps);
    for (int i = 0; i < n; i ++){
        block_position_steady.row(i) = block_position_to_file.row(i) - steady_state.row(i);
    }
    return block_position_steady;
}

/**
 * Method used to create the steady state solution. 
 */
void Operation::amplitudeModeZero(){
    arma::mat block_position_steady = steadyStateSolutuion();
    int n = arma::size(block_position_to_file)[0];
    
    arma::mat block_pad_position_steady(n+1, number_of_save_steps);
    block_pad_position_steady = arma::join_cols(block_position_steady, pad_position_to_file.t());

    arma::colvec mode_zero(number_of_save_steps);

    for (int i = 0; i < number_of_save_steps ; i ++){
        arma::colvec eigenvector = real_eig_vectors.col(n);

        mode_zero(i) = arma::dot(eigenvector, block_pad_position_steady.col(i));
    }
    position_matrix_heat.row(n) = mode_zero.t();
    position_matrix_heat.save(this->result_path + this->input_parameters.file_name + "_position_matrix_heat.csv", arma::csv_ascii);
}

/**
 * Mothod to remove the mean value of all modal cotributions.
 * Done so that the heat map would not be "taken over" by certain modes.
 * It also make sence from a physical perspective.
 */
void Operation::modeZeroRemoveMean(){
    int n = arma::size(position_matrix_heat)[0];
    for (int i = 0; i < n; i++){
        double mean = arma::mean(position_matrix_heat.row(i));
        double mean_v = arma::mean(velocity_matrix_heat.row(i));
        double mean_e = arma::mean(energy_matrix_heat.row(i));

        for (int j = 0; j< number_of_save_steps; j++){
            position_matrix_heat.row(i).col(j) = position_matrix_heat.row(i).col(j) - mean;
            velocity_matrix_heat.row(i).col(j) = velocity_matrix_heat.row(i).col(j) - mean_v;
            energy_matrix_heat.row(i).col(j) = energy_matrix_heat.row(i).col(j) - mean_e;
        }
    }
    position_matrix_heat.save(this->result_path + this->input_parameters.file_name + "_position_matrix_heat_mean_removed.csv", arma::csv_ascii);
    velocity_matrix_heat.save(this->result_path + this->input_parameters.file_name + "_velocity_matrix_heat_mean_removed.csv", arma::csv_ascii);
    energy_matrix_heat.save(this->result_path + this->input_parameters.file_name + "_energy_matrix_heat_mean_removed.csv", arma::csv_ascii);
}
