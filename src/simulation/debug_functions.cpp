#include"simulation.hpp"
#include<omp.h>
#include "parameters.hpp"
#include <iostream>

///      DEBUGGING FUNCTIONS      ///

/**
 * This method prints matrix.
 * @param matrix matrix
 * @param array_format input truw if array format on the output is wanted.
 */
void DebugSimulation::printMatrix(arma::mat matrix, bool array_format = false)
{
    cout << endl;
    if(!array_format)
    {
        matrix.print("Matrix: ");
    }else
    {
        int count = 0;
        arma::mat::iterator it_end = matrix.end();
        for(arma::mat::iterator it = matrix.begin(); it != it_end; ++it)
        {
            cout << it[0] << endl;
            count += 1;
        }
        cout << count << endl;
    }
    cout << endl;
}

/**
 * This method prints vector.
 * @param vector vector
 */

void DebugSimulation::printRowVector(arma::rowvec vector){
    cout << endl;
    vector.print("Row vector: ");
    cout << endl;
}

/**
 * Main method runs the program. Uncomment to run different methods. 
 */
void DebugSimulation::main()
{
    // init();
    // setupModelVectorsMatrices();
    /* printMatrix(block_matrix);
    printRowVector(pad_vector); */
    // function_u(block_matrix, pad_vector);
    // function_x(block_matrix, pad_vector);
    /* printMatrix(block_matrix_derivative);
    printRowVector(pad_vector_derivative); */
    runMidpointMethod();
}