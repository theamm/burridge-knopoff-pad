#include"operations.hpp"
#include <iostream>
#include <limits>
#include <string>
#include <algorithm>
#include <armadillo>

/**
* Calculates eigenvalues and eigenvectors and saves them to .csv.
*/
void Operation::calculateEigenvaluePairs(){
    if(!input_parameters.debug_no_pad){
        arma::mat mass_matrix = createMassMatrix();
        arma::mat stiffness_matrix = createStiffnessMatrix();
    }
    else{
        arma::mat mass_matrix = createMassMatrixBK();  //mass_matrix er global så gir ikke mening med returverdi...
        arma::mat stiffness_matrix = createStiffnessMatrixBK();
    }
    std::string path =  result_path + input_parameters.file_name;

    mass_matrix.save(path + "_mass_matrix.csv", arma::csv_ascii);
    stiffness_matrix.save(path + "_stiffness_matrix.csv", arma::csv_ascii);
    
    bool eig = arma::eig_pair(eigval, eigvec, stiffness_matrix, mass_matrix);

    real_eig_values = real(eigval);
    real_eig_vectors = real(eigvec);
    calculateDiagonalMatrices(mass_matrix, stiffness_matrix);
    normalize(mass_matrix);
    saveEigenvalues();
}


/**
* Saves global eigenvectors and eigenvalues as .csv-files.
*/
void Operation::saveEigenvalues(){
    std::string path =  this->result_path + this->input_parameters.file_name;
    
    real_eig_values.save(path + "_eigenvalues" + ".csv", arma::csv_ascii);
    real_eig_vectors.save(path + "_eigenvectors" + ".csv", arma::csv_ascii);
}

/**
* Creates a non-normalised mass matrix for the BKP equation.
*/
arma::mat Operation::createMassMatrix(){
    arma::mat mass_matrix_local(input_parameters.N+1, input_parameters.N+1, arma::fill::eye); 
    mass_matrix = mass_matrix_local;
    mass_matrix = mass_matrix * m_u;
    mass_matrix(input_parameters.N, input_parameters.N) = input_parameters.m_mass_x;
    return mass_matrix;
}

/**
* Creates a non-normalised mass matrix for the BK equation.
*/
arma::mat Operation::createMassMatrixBK(){
    arma::mat mass_matrix_local(input_parameters.N, input_parameters.N, arma::fill::eye); 
    mass_matrix = mass_matrix_local;
    mass_matrix = mass_matrix * m_u;
    return mass_matrix;
}

/**
* Creates a non-normalised stifness matrix for the BKP equation.
*/
arma::mat Operation::createStiffnessMatrix(){
    arma::mat stiffness_matrix_local(input_parameters.N+1, input_parameters.N+1, arma::fill::zeros);
    stiffness_matrix = stiffness_matrix_local;
    
    if(input_parameters.N == 1)
    {
        createStiffnessMatrixOneBlock();
    }
    else
    {
        for (int i = 1; i < input_parameters.N-1; i++){
            for (int j = 0; j < input_parameters.N+1; j++){
                if (i == j){
                    stiffness_matrix(i,j) = 2*k_c+k_p;
                }
                if (j == i+1 || j == i-1){
                    stiffness_matrix(i, j) = -k_c;
                }
        }
        }
        for (int i = 0; i < input_parameters.N; i++){
            stiffness_matrix(input_parameters.N, i) = -k_p;
            stiffness_matrix(i, input_parameters.N) =  -k_p;
        }
        stiffness_matrix(input_parameters.N, input_parameters.N) = input_parameters.N*k_p+input_parameters.m_k_P0;
        stiffness_matrix(0, 0) = stiffness_matrix(input_parameters.N-1, input_parameters.N-1) = k_c+k_p;
        stiffness_matrix(0, 1) = stiffness_matrix(input_parameters.N-1, input_parameters.N-2) = -k_c;
    }
    return stiffness_matrix;
}

void Operation::createStiffnessMatrixOneBlock(){
    stiffness_matrix(input_parameters.N, input_parameters.N) = k_p + input_parameters.m_k_P0;
    stiffness_matrix(0, 0) = k_p;
    stiffness_matrix(1, 0) = -k_p;
    stiffness_matrix(0, 1) = -k_p;
}

/**
* Creates a non-normalised stifness matrix for the BK equation.
*/
arma::mat Operation::createStiffnessMatrixBK(){
    arma::mat stiffness_matrix_local(input_parameters.N, input_parameters.N, arma::fill::zeros);
    
    for (int i = 1; i < input_parameters.N-1; i++){
        for (int j = 0; j < input_parameters.N; j++){
            if (i == j){
                stiffness_matrix_local(i,j) = 2*k_c+k_p;
            }
            if (j == i+1 || j == i-1){
                stiffness_matrix_local(i, j) = -k_c;
            }
        }
    }
    stiffness_matrix_local(0,0) = k_c+k_p;
    stiffness_matrix_local(0,1) = -k_c;
    stiffness_matrix_local(input_parameters.N-1,input_parameters.N-1) = k_c+k_p;
    stiffness_matrix_local(input_parameters.N-1,input_parameters.N-2) = -k_c;
    stiffness_matrix = stiffness_matrix_local;
    return stiffness_matrix_local;
}


/**
 * Checks is matrix is symetric. Used for debugging purpuses.
 * @param matrix input matrix
 * @param name matrix name
 */
void Operation::isSymmetric(arma::mat matrix, std::string name){
    if (!(matrix.is_symmetric())){
        std::cout << name << " is not symetric. Something is wrong." << std::endl;
    }
    else{
        std::cout << name << " is symetric. Good job." << std::endl;
    }
}

/**
* Calculates the mass and stiffness diagonal matrices. 
*/
void Operation::calculateDiagonalMatrices(arma::mat mass_matrix, arma::mat stiffness_matrix){
    arma::mat diagonal_mass_matrix = real_eig_vectors.t()*mass_matrix*real_eig_vectors;
    arma::mat diagonal_stiffness_matrix = real_eig_vectors.t()*stiffness_matrix*real_eig_vectors;
    arma::uvec q1 = find(diagonal_mass_matrix < std::pow(10, -10));
    arma::uvec q2 = find(diagonal_stiffness_matrix < std::pow(10, -5));
    diagonal_mass_matrix(q1).zeros();
    diagonal_stiffness_matrix(q2).zeros();

    if (!(diagonal_mass_matrix.is_diagmat())){
        std::cout <<  "Diagonal mass matrix is supposed to be diagonal but is not." << std::endl;
    }
    if (!diagonal_stiffness_matrix.is_diagmat()){
        std::cout <<  "Diagonal stiffness matrix is supposed to be diagonal but is not." << std::endl;
    }

    this->diagonal_mass_matrix = diagonal_mass_matrix;
    this->diagonal_stiffness_matrix = diagonal_stiffness_matrix;
}

/**
 * Method to mass normalize eigenvectors.  
 * @param mass_matrix mass_matrix
 */
void Operation::normalize(arma::mat mass_matrix){
    arma::mat eig_vectors_new = real_eig_vectors;

    for (int i = 0; i < this->degrees_freedom+1; i++){
        arma::vec eig_vector = real_eig_vectors.col(i);
        arma::vec diagonalised_element_vec = (eig_vector.t()*mass_matrix)*eig_vector;
        double diagonalised_element = diagonalised_element_vec(0);
        for(int i = 0; i < this->degrees_freedom+1; i++){
            eig_vector(i) = eig_vector(i)/sqrt(diagonalised_element);
        }
        eig_vectors_new.col(i) = eig_vector;
    }
    real_eig_vectors = eig_vectors_new;
    arma::mat result = eig_vectors_new.t()*mass_matrix*eig_vectors_new;
}

    