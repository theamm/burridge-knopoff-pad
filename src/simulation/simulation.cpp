#include"simulation.hpp"
#include "parameters.hpp"

Simulation::Simulation(std::string parameters_path, std::string result_path)
{
    //main(std::string parameters_path, std::string result_path);
}

/**
 * Method to initialize. 
 * Cleaner to have this code in constructor, but then doxygen wont make a proper flow diagram.
 * This is a non-solved doxygen issue. 
 */
void Simulation::init(std::string parameters_path, std::string result_path){
    this->input_parameters = Parameters(parameters_path);
    this->yaml_path = parameters_path;
    this->result_path = result_path;

    this->result_folder = input_parameters.file_name;
    createFolder(this->result_folder);
    this->result_path=this->result_path + this->result_folder + "/";
    createFolder("Figures");
    createFolder("code");
    copyYamlFile();
    arma::arma_rng::set_seed(input_parameters.seed);
    this->setupModelVectorsMatrices();
    this->setupConstants();
}

Simulation::Simulation(std::string parameters_path, std::string result_path, bool friction_parallelized):input_parameters_path(parameters_path){
    if(!friction_parallelized){
        cout << "Called with friction_parallized parameter, but it is set to false." << endl;
        cout << "Please run without this parameter if the friction parallization version is not wanted." << endl;
    }
    else
    {
        Simulation(parameters_path, result_path);
        calculateFrictionInParallel();
    }
}

void Simulation::setupConstants(){
    m_u = input_parameters.m_scale_mass*input_parameters.m_mass_x/input_parameters.N;				
	k_p = input_parameters.m_scale_P*input_parameters.m_k_P0/input_parameters.N;					
	m_c_crit = 2*sqrt((input_parameters.m_k_P0+input_parameters.N*k_p)*input_parameters.m_mass_x);	
	c_p = input_parameters.m_zeta*m_c_crit;
    k_c = input_parameters.m_scale_C*input_parameters.N*input_parameters.m_k_P0;
    this->setupSliderSpeedMode(); 
}
