
#include"simulation.hpp"

//      START: F FUNCTIONS NUMERICAL SCHEME       //

/**
 *\f$
    \vec{u_{j}}=
    \begin{bmatrix}
    u_{j} \\ \dot{u_{j}}
    \end{bmatrix}
    \quad
    \dot{\vec{u_{j}}} = f(\vec{u_{j}}) =
    \begin{bmatrix}
    \dot{u_{j}} \\ \frac{1}{m_{u}}[k_c(u_{j-1} - 2u_j + u_{j+1})-k_p(u_j - x) - m_u \phi(v + \dot{u_j})]
    \end{bmatrix}
    \f$
*/

void Simulation::function_u(
    const arma::mat& current_block_matrix,
    const arma::rowvec& current_pad_vector,
    arma::mat& current_block_matrix_derivative)
{
    arma::colvec spring_forces = arma::zeros<arma::colvec>(input_parameters.N);

    for (int i = 0; i < input_parameters.N; i++)
    {
        if(!input_parameters.debug_no_neighbor_springs)
        {
            spring_forces(i)
                = k_c*(i == 0 ? 0 : (current_block_matrix(i - 1, 0) - current_block_matrix(i, 0)))
                + k_c*(i == input_parameters.N - 1 ? 0 : (current_block_matrix(i + 1, 0) - current_block_matrix(i, 0)));
            
            if(input_parameters.rayleigh_damping){
                spring_forces(i) += (input_parameters.m_delta*(
                    k_c*(i == 0 ? 0 : (current_block_matrix(i - 1, 1) - current_block_matrix(i, 1)))
                    + k_c*(i == input_parameters.N - 1 ? 0 : (current_block_matrix(i + 1, 1) - current_block_matrix(i, 1)))
                    ));  
            }
        }
        if(!input_parameters.debug_no_stationary_springs)
        {
            spring_forces(i) -= k_p*(current_block_matrix(i, 0) - current_pad_vector(0));
            
            if (input_parameters.rayleigh_damping){
                spring_forces(i) -= input_parameters.m_delta*k_p*(current_block_matrix(i,1) - current_pad_vector(1));
            }
        }
        if(!input_parameters.debug_no_friction)
        {
            spring_forces(i) -= m_u*phi(2*input_parameters.m_alpha*input_parameters.slider_speed + current_block_matrix(i, 1));
        }
        
        if (input_parameters.rayleigh_damping){
            spring_forces(i) 
            -= input_parameters.m_eta*m_u*current_block_matrix(i,1); 
        }
	}
    current_block_matrix_derivative.col(0) = current_block_matrix.col(1);
    current_block_matrix_derivative.col(1) = spring_forces / m_u;
}

/**
    \f$\vec{x}=
	\begin{bmatrix}
	x \\ \dot{x}
	\end{bmatrix}
	\quad
	\dot{\vec{x}} = f(\vec{x}) =
	\begin{bmatrix}
	\dot{x} \\ \frac{1}{m_{x}}[-c_p\dot{x} + k_p \sum_j(u_j -x) - k_{p0}x]
	\end{bmatrix}
    \f$
 */

void Simulation::function_x(
    const arma::mat& current_block_matrix,
    const arma::rowvec& current_pad_vector,
    arma::rowvec& current_pad_vector_derivative)
{
    double pad_second_derivative = 0;
    if(!input_parameters.debug_no_stationary_springs && !input_parameters.debug_no_pad){
        pad_second_derivative = calculatePadFriction(current_block_matrix, current_pad_vector);
    }
    pad_second_derivative += -input_parameters.m_k_P0*current_pad_vector(0);
    
    if(input_parameters.rayleigh_damping){
        pad_second_derivative += 
        input_parameters.m_delta*(
        - input_parameters.m_k_P0*current_pad_vector(1)
        + k_p*arma::sum(current_block_matrix.col(1) - current_pad_vector(1))
        )
        - input_parameters.m_eta*input_parameters.m_mass_x*current_pad_vector(1); 
    }

    if(!input_parameters.debug_no_damper){
        pad_second_derivative += calculatePadSupportDamper(current_pad_vector);
    }
    
    current_pad_vector_derivative(0) = current_pad_vector(1);
    current_pad_vector_derivative(1) = pad_second_derivative / input_parameters.m_mass_x;
}


// ONE DEGREE-OF-FREEDOM //
void Simulation::function_x_odof(
    const arma::rowvec& current_pad_vector,
    arma::rowvec& current_pad_vector_derivative
){
    double pad_second_derivative = 0;

    if(!input_parameters.debug_no_damper)
    {
        pad_second_derivative = calculatePadSupportDamper(current_pad_vector);
    }

    if(!input_parameters.debug_no_stationary_springs && !input_parameters.debug_no_pad)
    {
        pad_second_derivative += -phi(current_pad_vector(1) + input_parameters.slider_speed);
    }


    pad_second_derivative += -input_parameters.m_k_P0*current_pad_vector(0);

    current_pad_vector_derivative(0) = current_pad_vector(1);
    current_pad_vector_derivative(1) = pad_second_derivative / input_parameters.m_mass_x;
}

double Simulation::calculatePadSupportDamper(arma::rowvec current_pad_vector)
{
    return -c_p*current_pad_vector(1);
}

double Simulation::calculatePadFriction(arma::mat current_block_matrix, arma::rowvec current_pad_vector)
{
    return k_p*arma::sum(current_block_matrix.col(0) - current_pad_vector(0));
}