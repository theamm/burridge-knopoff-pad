#include <iostream>
//#include "../../include/parameters.hpp"
#include "../../include/simulation.hpp"
#include "../../include/operations.hpp"

/**
* Main method. 
* @param argc standard input parameters.
* @param argv list of inputs.
*/
int main(int argc, char *argv[])
{
    std::string result_path = "../../results/";
    std::string yaml_path = "../../yaml/parameters.yaml";
    
    if (argc > 1) {
        if (strcmp(argv[1], "-h")==0) {
            std::cout << "This is the help text. \n \n" <<
            "argv[1]: input -e if wanted to run the eigenvalue stuff." <<
            "if you wish to change the which yaml file to input you need to edit the main.cpp file manually and recompile." << std::endl;
            return 0;
        }
    }
    DebugSimulation simulation(yaml_path, result_path); 
    simulation.init(yaml_path, result_path);
    simulation.main();
    
    if(argc > 1){
        if(strcmp(argv[1], "-e")==0){
            Operation operation(simulation);
        } 
    }
    return 0;
}
