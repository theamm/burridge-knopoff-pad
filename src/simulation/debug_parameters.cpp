
#include"simulation.hpp"
#include<omp.h>
#include "parameters.hpp"
#include <iostream>

///      DEBUGGING PAMRAMETERS      ///

/**
 * Prints parameters
 */
void DebugParameters::printLoadedFile(){
    std::cout << read_parameters << std::endl;
}

/** 
 * Used for debugging purposes
 */
void DebugParameters::debugTest(){
    //printCurrentDirectory();
    printLoadedFile();
    loggedBlocks.print();
}