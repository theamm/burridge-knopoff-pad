//#include"parameters.hpp"
#include "operations.hpp"

/**
 * Constructor that saves the necesarry parameters to object.
 * Calles the main methods to create eigenvalues and heat map matrices. 
 * 
 */
Operation::Operation(Simulation& simulation){
    k_c = simulation.k_c;
    k_p = simulation.k_p;
    m_u = simulation.m_u;
    number_of_save_steps = simulation.number_of_save_steps;
    block_position_to_file = simulation.block_position_to_file;
    block_velocity_to_file = simulation.block_velocity_to_file;
    pad_position_to_file = simulation.pad_position_to_file;
    pad_velocity_to_file = simulation.pad_velocity_to_file;
    pad_friction_to_file = simulation.pad_friction_to_file;

    std::string parameters_path = "../../yaml/parameters.yaml";
    this->input_parameters = Parameters(parameters_path);

    std::string result_path = "../../results/";

    this->result_path = result_path;
    this->result_folder = input_parameters.file_name;
    this->result_path=this->result_path + this->result_folder + "/";
    this->degrees_freedom = !input_parameters.debug_no_pad ? input_parameters.N : input_parameters.N-1;
    calculateEigenvaluePairs();  
    sortEigenvectors();
    heat_map_main();
};

/**
 * Sorts the eigenvalues and the corresponing eigenvectors.
 * This is done using a python script due to lack of C++ experience.
 * ToDo: Sorts the eigenvalues and the corresponing eigenvectors in C++ and omit the python-script..
 */
void Operation::sortEigenvectors(){
    std::string filename = "sort_eigenvalues.py " + input_parameters.file_name;
    std::string command = "python3 ";
    command += filename;
    system(command.c_str());  
    real_eig_values.load(result_path + result_folder + "_eigenvalues.csv", arma::csv_ascii);
    real_eig_vectors.load(result_path + result_folder + "_eigenvectors.csv", arma::csv_ascii);
}