import yaml
import numpy as np
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

### Script used to sort eigenvalues and corresponing eigenvectors.    

def run():
    stream = open("../../yaml/parameters.yaml", "r")
    parameters = yaml.load(stream, Loader=Loader)
    run_name = parameters["Parameters"]["file_name"]
    eigen_values, eigen_vectors, eigen_values_path, eigen_vector_path = load(run_name)  
    eigen_values, eigen_vectors = sort_eigen_values(eigen_values, eigen_vectors)
    save(eigen_values, eigen_vectors, eigen_values_path, eigen_vector_path)

def sort_eigen_values(eigen_values, eigen_vectors):
    idx = eigen_values.argsort()[::-1]   
    eigen_values = eigen_values[idx]
    eigen_vectors = eigen_vectors[:,idx]
    return eigen_values, eigen_vectors 

def load(run_name):
    results_path = "../../results/"
    eigen_values_path = results_path + run_name + "/" + run_name + "_eigenvalues.csv"
    eigen_vector_path = results_path + run_name + "/" + run_name + "_eigenvectors.csv"
    eigen_values = np.loadtxt(eigen_values_path, delimiter=",")
    eigen_vectors = np.loadtxt(eigen_vector_path, delimiter=",")
    return eigen_values, eigen_vectors, eigen_values_path, eigen_vector_path

def save(eigen_values, eigen_vecotrs, eigen_values_path, eigen_vector_path):
    np.savetxt(eigen_values_path, eigen_values, delimiter=",")
    np.savetxt(eigen_vector_path, eigen_vecotrs, delimiter=",")

if __name__ == "__main__":
    run()