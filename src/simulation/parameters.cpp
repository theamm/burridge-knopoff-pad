//#include <iostream>
#include "parameters.hpp"


Parameters::Parameters(){
}

Parameters::Parameters(std::string filepath){
    load_file(filepath);
    assign_parameters();
}

void Parameters::load_file(std::string yaml_filepath)
{
    YAML::Node read_parameters = YAML::LoadFile(yaml_filepath);
    this->read_parameters = read_parameters;
}

void Parameters::assign_parameters()
{
    this->parameters = read_parameters["Parameters"];
    this->debug_parameters = read_parameters["Debug"];

	this->dt = readParameterDouble("dt");
	this->seed = readParameterInt("seed");
	this->num_events = readParameterInt("num_events");
	
	this->N = readParameterInt("N");
	this->max_time = readParameterDouble("max_time");
	this->slider_speed = readParameterDouble("slider_speed");
	this->increment = readParameterDouble("increment");
	this->interval = readParameterDouble("interval");

    this->file_name = readParameterString("file_name");

	this->progress_indicator = readParameterBool("progress_indicator");

    this->m_F0 = readParameterDouble("m_F0");
    this->m_alpha = readParameterDouble("m_alpha");
	this->m_sigma = readParameterDouble("m_sigma");
	this->m_mass_x = readParameterDouble("m_mass_x");
	this->m_scale_mass = readParameterDouble("m_scale_mass");
	this->m_zeta = readParameterDouble("m_zeta");
	this->m_k_P0 = readParameterDouble("m_k_P0");
	this->m_scale_P = readParameterDouble("m_scale_P");
    this->m_scale_C = readParameterDouble("m_scale_C");

    this->m_t = readParameterDouble("m_t");
	this->m_v0 = readParameterDouble("m_v0");
	this->m_u_min = readParameterDouble("m_u_min");

    this->loggedBlocks = readParameterIntList("blocks");	

    this->start_speed_continuous = readParameterDouble("start_speed_continuous");
    this->end_speed_continuous = readParameterDouble("end_speed_continuous");

    this->save_interval_dt = readParameterInt("save_interval_dt");
    this->threshold_speed = readParameterDouble("threshold_speed");

    this->m_eta = readParameterDouble("m_eta");
    this->m_delta = readParameterDouble("m_delta");
    this->lyapunov_delta = readParameterDouble("lyapunov_delta");

    this->debug_no_friction = readDebugParameterBool("debug_no_friction");
    this->debug_no_neighbor_springs = readDebugParameterBool("debug_no_neighbor_springs");
    this->debug_no_stationary_springs = readDebugParameterBool("debug_no_stationary_springs");
    this->debug_no_damper = readDebugParameterBool("debug_no_damper");
    this->debug_no_pad = readDebugParameterBool("debug_no_pad");
    this->debug_negative_intial_values = readDebugParameterBool("debug_negative_initial_values");
    this->debug_only_negative_initial = readDebugParameterBool("debug_only_negative_initial");
    this->debug_only_write_friction = readDebugParameterBool("debug_only_write_friction");
    this->debug_continuous_slider_speed = readDebugParameterBool("debug_continuous_slider_speed");
    this->debug_one_degree_freedom_mode = readDebugParameterBool("debug_one_degree_freedom_mode");
    this->rayleigh_damping = readDebugParameterBool("rayleigh_damping");
    this->calculate_Lyapunov = readDebugParameterBool("calculate_Lyapunov");
}

double Parameters::readParameterDouble(std::string parameter_name){
    return parameters[parameter_name].as<double>();
}

double Parameters::readParameterInt(std::string parameter_name){
    return parameters[parameter_name].as<int>();
}

std::string Parameters::readParameterString(std::string parameter_name){
    return parameters[parameter_name].as<std::string>();
}

bool Parameters::readParameterBool(std::string parameter_name){
    return parameters[parameter_name].as<bool>();
}  

arma::uvec Parameters::readParameterIntList(std::string parameter_name){
    return(yamlListToArmadilloVector(parameters[parameter_name]));
}

arma::uvec Parameters::yamlListToArmadilloVector(YAML::Node yamlList){
    arma::uvec armaVector(yamlList.size());
    for(size_t i = 0; i < yamlList.size(); i++){
        armaVector[i] = yamlList[i].as<int>();
    }
    return armaVector;
}

// Used for debugging purposes in simulation //

bool Parameters::readDebugParameterBool(std::string debug_parameter_name){
    return debug_parameters[debug_parameter_name].as<bool>();
}
