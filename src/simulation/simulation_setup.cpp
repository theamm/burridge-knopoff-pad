#include"simulation.hpp"
//#include<omp.h>
//#include "parameters.hpp"
//#include <iostream>

//      START: SET UP VECTORS AND MATRICES       //
void Simulation::setupModelVectorsMatrices()
{
    setupBlockMatrix();
    setupBlockMatrixDerivative();
    setupPadVector();
    setupPadVectorDerivative();
    setupSaveVectorsMatrices();

    if(input_parameters.calculate_Lyapunov){
        setupLyapunov();
    }
}

// Initalizes a random N*2 arma matrix.
void Simulation::setupBlockMatrix()
{
    block_matrix.set_size(input_parameters.N, 2);
    if(input_parameters.debug_negative_intial_values){
        block_matrix = 2.0*block_matrix.randu() - 1.0;
    }
    else if (input_parameters.debug_only_negative_initial){
        block_matrix = block_matrix.randu() * (-1.0);
    }
    else{
        block_matrix.randu();
    }
}

void Simulation::setupBlockMatrixDerivative(){
    block_matrix_derivative.set_size(input_parameters.N, 2);
    block_matrix_derivative.fill(1.0);
}

void Simulation::setupPadVector(){
    pad_vector = arma::zeros<arma::rowvec>(2);
}

void Simulation::setupPadVectorDerivative(){
    pad_vector_derivative = arma::zeros<arma::rowvec>(2);
}

void Simulation::setupSaveVectorsMatrices()
{
    // SAVE PURPOSES //
    number_of_time_steps = input_parameters.max_time / input_parameters.dt;
    number_of_save_steps = floor(input_parameters.max_time / (input_parameters.dt * input_parameters.save_interval_dt));
    number_of_interval_save_step = floor(input_parameters.interval / (input_parameters.dt * input_parameters.save_interval_dt));
    if(input_parameters.loggedBlocks.size() < input_parameters.N && input_parameters.loggedBlocks.size() > 0){
        loggingSpecificBlocks = true;
    }
    if(loggingSpecificBlocks){
        block_position_to_file.set_size(input_parameters.loggedBlocks.size(), number_of_save_steps);
        block_velocity_to_file.set_size(input_parameters.loggedBlocks.size(), number_of_save_steps);
    } else{
        block_position_to_file.set_size(input_parameters.N, number_of_save_steps);
        block_velocity_to_file.set_size(input_parameters.N, number_of_save_steps);
    }
    printf("Number of time steps: %i\n",number_of_save_steps);
    printf("Logging specific blocks: %d\n", loggingSpecificBlocks);
  //  block_position_to_file.print();
    pad_position_to_file.set_size(number_of_save_steps);
    pad_velocity_to_file.set_size(number_of_save_steps);
    if(input_parameters.debug_continuous_slider_speed){
        pad_friction_to_file.set_size(number_of_save_steps);
    }else{
        pad_friction_to_file.set_size(number_of_interval_save_step);
    }
}


void Simulation::setupSliderSpeedMode()
{
    if(input_parameters.debug_continuous_slider_speed)
    {
        if(input_parameters.interval == 0)
        {
            updateSliderSpeedDtInterval = 1;
            input_parameters.slider_speed = input_parameters.start_speed_continuous;
            input_parameters.increment = (input_parameters.end_speed_continuous - input_parameters.start_speed_continuous) / number_of_time_steps;
        }
        else
        {
            updateSliderSpeedDtInterval = (int)(input_parameters.interval/input_parameters.dt); // used for updating slider speed
        }
    }else
    {
        updateSliderSpeedDtInterval = (int)(input_parameters.interval/input_parameters.dt); // used for updating slider speed
    }
   // printf("Slider speed increment: %.10f\n",input_parameters.increment);
}

void Simulation::setupLyapunov(){
    block_matrix_lyapunov = block_matrix + block_matrix * input_parameters.lyapunov_delta;
    block_matrix_derivative_lyapunov = block_matrix_derivative + block_matrix_derivative * input_parameters.lyapunov_delta;
    pad_vector_lyapunov = pad_vector + pad_vector * input_parameters.lyapunov_delta;
    pad_vector_derivative_lyapunov = pad_vector_derivative + pad_vector_derivative * input_parameters.lyapunov_delta;

    delta_gamma_prev = sum(block_matrix_lyapunov.col(0)) + sum(block_matrix_lyapunov.col(1)) + pad_vector_lyapunov(0) + pad_vector_lyapunov(1);
    std::cout << "Delta_gamma (t=0)" << delta_gamma_prev << std::endl;

    arma::mat test_matrix(4, 4, arma::fill::eye);  
    std::cout << "test matrix" << test_matrix << std::endl;
    test_matrix = test_matrix * 2;
    std::cout << "test matrix2" << test_matrix << std::endl;
}
