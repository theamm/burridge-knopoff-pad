#include"simulation.hpp"
#include <omp.h>
//#include "parameters.hpp"
//#include <iostream>

// START: Parallel friction calculation //

void Simulation::midpointMethodParallel(
    int timestep,
    arma::mat& block_matrix_parallel,
    arma::rowvec& pad_vector_parallel,
    arma::mat block_matrix_derivative_parallel,
    arma::rowvec pad_vector_derivative_parallel
)
{
    arma::mat block_matrix_half_step = block_matrix_parallel;
    arma::rowvec pad_vector_half_step = pad_vector_parallel;

    function_u(block_matrix_parallel, pad_vector_parallel, block_matrix_derivative_parallel);
    function_x(block_matrix_parallel, pad_vector_parallel, pad_vector_derivative_parallel);

    block_matrix_half_step = block_matrix_parallel + 0.5 * input_parameters.dt * block_matrix_derivative_parallel;
    pad_vector_half_step = pad_vector_parallel + 0.5 * input_parameters.dt * pad_vector_derivative_parallel;

    function_u(block_matrix_half_step, pad_vector_half_step, block_matrix_derivative_parallel);
    function_x(block_matrix_half_step, pad_vector_half_step, pad_vector_derivative_parallel);

    block_matrix_parallel = block_matrix_parallel + input_parameters.dt * block_matrix_derivative_parallel;
    pad_vector_parallel = pad_vector_parallel + input_parameters.dt * pad_vector_derivative_parallel;
}

void Simulation::setupAndRunFrictionInParallel(
    int time_step,
    double slider_speed)
{
    arma::mat block_matrix_parallel;
    arma::mat block_matrix_derivative_parallel;

    arma::rowvec pad_vector_parallel;
    arma::rowvec pad_vector_derivative_parallel;
    block_matrix_parallel.set_size(input_parameters.N, 2);
    if(input_parameters.debug_negative_intial_values){
        block_matrix_parallel = 2.0*block_matrix_parallel.randu() - 1.0;
    }

    else if (input_parameters.debug_only_negative_initial)
    {
        block_matrix_parallel = block_matrix_parallel.randu() * (-1.0);
    }
    
    else
    {
        block_matrix_parallel.randu();
    }
    block_matrix_derivative_parallel.set_size(input_parameters.N, 2);
    block_matrix_derivative_parallel.fill(1.0);

    pad_vector_parallel = arma::zeros<arma::rowvec>(2);
    pad_vector_derivative_parallel = arma::zeros<arma::rowvec>(2);

    arma::colvec pad_friction_parallel_to_file;
    pad_friction_parallel_to_file.set_size(time_step);

    for(int i = 0; i < time_step; i++)
    {
        midpointMethodParallel(time_step, block_matrix_parallel, pad_vector_parallel, block_matrix_derivative_parallel, pad_vector_derivative_parallel);
        pad_friction_parallel_to_file(i) = calculatePadFriction(block_matrix_parallel, pad_vector_parallel);
    }
    saveToCsvParallel(pad_friction_parallel_to_file, this->result_path, slider_speed);
}

void Simulation::calculateFrictionInParallel()
{
    int number_of_slider_speed_intervals = floor(input_parameters.max_time / input_parameters.interval);
    double slider_speed_parallel = input_parameters.slider_speed;

    double time_steps_per_slider_speed = input_parameters.interval / input_parameters.dt;

    omp_set_num_threads(4);
    #pragma omp parallel for
    for(int i = 1; i < number_of_slider_speed_intervals + 1; i++)
    {
        slider_speed_parallel = input_parameters.slider_speed * i;
        setupAndRunFrictionInParallel(time_steps_per_slider_speed, slider_speed_parallel);
    }
}

// END: Parallel friction calculation //