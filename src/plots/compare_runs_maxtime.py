from myimports_basic import * 
import math
sns.set_style("white")
sns.set_palette("Paired")
from tqdm import tqdm
import time
import statistics 


class ComparePlots():
    def __init__(self, run_names):
        self.run_names = run_names
        self.load()
        self.calculate_mean()
        self.calculate_deviation()
        self.plot()

    def load(self):
        self.data_list = []
        self.labels = []
        self.maxtimes = []
        for run_name in self.run_names:
            parameters = loader.load_yaml_parameters(run_name)
            self.data_list.append(loader.load_simulation_output(parameters, run_name, "pad_friction"))
            self.labels.append(loader.get_parameter(parameters, "seed"))
            self.maxtimes.append(loader.get_parameter(parameters, "maxtime"))
        self.maxtimes.append("mean")

    def calculate_mean(self):
        self.mean_list = []
        n_timesteps = len(self.data_list[0])
        n_runs = len(self.data_list)
        for i in tqdm(range(n_timesteps)):
            mean = 0
            for run in self.data_list:
                if not(len(run) == n_timesteps):
                    if not(len(run) % n_timesteps == 0):
                        raise Exception("The runtimes needs to be devisible.")
                    else:
                        devider = len(run)/n_timesteps
                        if(devider<1):
                            raise Exception("Please input the run with the smalles maxtime first.")
                        else:
                            run = del lst[::devider]
                else:
                    mean += run[i]
            mean = mean / n_runs
            self.mean_list.append(mean)
        print(len(self.mean_list))

    def calculate_deviation(self):
        self.sd_list = []
        n_timesteps = len(self.data_list[0]) 
        for run in tqdm(self.data_list):
            sd = np.subtract(self.mean_list, run)**2
            self.sd_list.append(sd)
        self.sd_list.append(self.mean_list)


    def plot(self):
        n_timesteps = len(self.data_list[0]) 
        for i in len(self.sd_list):
            plot(n_timesteps, self.sd_list[i], label=self.maxtimes[i])
        plt.legend(title="Maxtime:")
        plt.show()
            

def run():
    if(len(sys.argv) >= 3):
        run_name = sys.argv[1:]
        plotter = ComparePlots(run_name)
    else:
        raise Exception("You need to input at least two run_names.")

if __name__ == "__main__":
    run()