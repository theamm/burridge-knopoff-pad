from myimports_basic import * 
from myimports_sp import * 

# Plots the stick percentage of different runs. 
# Input: two or more run names

class PlotStickPersentage:
    def __init__(self, run_list, numeric_method = "_midpoint_"):
        self.plot_stick(run_list)

    def set_up(self, run_name):
        parameters = loader.load_yaml_parameters(run_name)
        if not(loader.get_parameter(parameters, "start_speed_continuous") == loader.get_parameter(parameters, "end_speed_continuous")):
            Exception("Making this plot only makes sence with constant slider velocity.")
        self.slider_speed = loader.get_parameter(parameters, "start_speed_continuous")
        self.stick_persentage = loader.load_simulation_output(parameters, run_name, "total_stick_fraction", ".txt")

    # Used if simulation does not produce stick persentage by its own and it there are only outputed a few blocks.
    def calculate_stick_persentage_single(self, block):
        counter = 0
        for i in range(len(block)):
            if (round(block[i], 2) == -self.slider_speed):
                counter +=1
        fraction = counter / self.n_data_point * 100
        return fraction
    
    # Used if simulation does not produce stick persentage by its own and it there are only outputed a few blocks.
    def calculate_stick_persentage_all(self):
        stick_persentage = [];
        for block in self.data:
            stick_persentage.append(self.calculate_stick_persentage_single(block))
        return stick_persentage

    # Used if simulation does not produce stick persentage by its own and it there are only outputed a few blocks.
    def plot_runs(self, run_list):
        barWidth = 0.25
        barSpace = 0.30
        y_pos = []
        slider_speeds = []
        figure = plt.figure()
        for i in range(len(run_list)):
            self.set_up(run_list[i]) 
            objects = self.calculate_stick_persentage_all()
            if(i == 0):
                y_pos = [(barSpace*r) for r in range(len(objects))]
            else:
                y_pos = [1+x for x in y_pos]
            plt.bar(y_pos, objects, align='center', alpha=0.4, width=barWidth); 
            slider_speeds.append(self.slider_speed)
        plt.xlabel('runs')
        plt.ylabel('$\%$')
        plt.xticks([r+ barSpace for r in range(len(slider_speeds))], slider_speeds)
        plt.title('Persentage of stick for each block at different velocities.')
        loader.save_figure(run_list[len(run_list)-1],"stick_persentage", figure)
        plt.show()

    def plot_stick(self, run_list):
        barWidth = 0.15
        barSpace = 0.30
        sticks = []
        slider_speeds = []
        figure = plt.figure()
        counter = 0
        for i in range(len(run_list)):
            self.set_up(run_list[i])
            y_pos = barSpace*i
            if ((counter % 3) == 0):
                slider_speeds.append(self.slider_speed)
                print(counter)
            sticks.append(self.stick_persentage*100)
            plt.bar(y_pos, self.stick_persentage*100, align='center', width=0.2, color="#a6cee3", edgecolor="#a6cee3") #, color=colors[i]
            counter += 1
        plt.xlabel('$v$')
        plt.ylabel('$\\%$')
        plt.yticks(fontsize=9) 
        plt.xticks(fontsize=9) 
        plt.xticks([3*r*(barWidth + barWidth) for r in range(len(slider_speeds))], slider_speeds)
        plt.title('')
        loader.save_figure(run_list[len(run_list)-1],"stick_persentage", figure)
        plt.show()

    def scale_axsis(self, slider_speeds):
        new_list = []
        for i in range(len(slider_speeds)):
            if(slider_speeds[i] <= 0.1 and slider_speeds[i] > 0.01):
                number = slider_speeds[i] / 10 + 0.01
            elif(slider_speeds[i] <= 1 and slider_speeds[i] > 0.1):
                number = slider_speeds[i] /100 + 0.02
            elif(slider_speeds[i] >= 1):
                number = slider_speeds[i] /1000 + 0.03
            else:
                number = slider_speeds[i]
            new_list.append(number)
            print(new_list) 
        return new_list

    def plot_poly(self, run_list):
        sticks = []
        slider_speeds = []
        figure = plt.figure()
        for i in range(len(run_list)):
            self.set_up(run_list[i])
            sticks.append(self.stick_persentage*100)
            slider_speeds.append(self.slider_speed)
        new_slider_speeds = self.scale_axsis(slider_speeds)
        plt.xticks([new_slider_speeds[len(new_slider_speeds)-1]/len(new_slider_speeds)*r for r in range(len(slider_speeds))], slider_speeds)
        plt.plot(new_slider_speeds, sticks)
        sns.despine(trim=True)
        plt.xlabel('velcoity')
        plt.ylabel('\%')
        loader.save_figure(run_list[len(run_list)-1],"stick_persentage_graph", figure)
        plt.show()


def run():
    if(len(sys.argv) >= 2):
        run_name = sys.argv[1:]
        plot_stick = PlotStickPersentage(run_name)
    else:
        plot_stick = PlotStickPersentage()

if __name__ == "__main__":
    run()
