from myimports_basic import *
store_data = []
save_intervals = []
sns.set_palette("Paired")
#mpl.rcParams["svg.fonttype"] = "none"
#mpl.rcParams["font.size"] = 6
#plt.rcParams['axes.labelsize'] = 3

# Script to plot the block and pad position against a continous slider velocity.
# Input
# argv[1] = run_name
# To compare, pass in more run_name's with the same parameters but different save dt.

class PlotFriction:
    def __init__(self, start_speed, end_speed, figure_name="friction", trim=False):
        self.trim = trim
        self.new_start_speed = start_speed
        self.new_end_speed = end_speed
        self.fig_name = figure_name

    def velocity_x_values(self, parameters, n_data_points):
        if not(loader.get_debug_parameter(parameters, "debug_continuous_slider_speed")):
            raise Exception ("Slider speed need to be contiious for this script to run")
        else:
            self.speeds = loader.get_start_end_speed(parameters)
            self.max_time = loader.get_parameter(parameters, "max_time")
            self.step_size = loader.get_parameter(parameters, "dt")
            self.save_interval_dt = loader.get_parameter(parameters, "save_interval_dt")
            self.speed_difference = self.speeds["end_speed"] - self.speeds["start_speed"]
            self.n_steps = self.max_time / self.step_size
            self.step_speed_increment = self.speed_difference / self.n_steps
            self.label = str(self.speeds["start_speed"])
            linspace = np.linspace(self.speeds["start_speed"], self.speeds["end_speed"], num=n_data_points)
            return linspace, self.n_steps
            

    def direction_label(self, parameters):
        speeds = loader.get_start_end_speed(parameters)
        if speeds["start_speed"] > speeds["end_speed"]:
            speed_direction = "decreasing"
        else:
            speed_direction = "increasing"
        return speed_direction


    def save_interval_label(self, paramters):
        save_interval = str(loader.get_parameter(paramters, "save_interval_dt"))
        return save_interval


    def plot_run_hardcode(self, run_name):
        parameters = loader.load_yaml_parameters(run_name)
        data = loader.load_simulation_output(parameters, run_name, "pad_friction")
        x_values, n_steps = self.velocity_x_values(parameters, len(data))
        if(self.trim):
            data = loader.continuous_trim(data, len(data), self.speeds["start_speed"], self.speeds["end_speed"], self.new_start_speed, self.new_end_speed, False)
            x_values = np.linspace(self.new_start_speed, self.new_end_speed, num=len(data))
            plt.xlim(self.new_start_speed, self.new_end_speed)
        x_labels = np.linspace(self.new_start_speed, self.new_end_speed, 10)
        x_labels = [round(num, 3) for num in x_labels]
        xticks = np.linspace(len(x_values)-1,0, 10, dtype=np.int)
        self.label = str(loader.get_parameter(parameters, "m_zeta")) + " " + str(loader.get_parameter(parameters, "m_eta")) +" " + str(loader.get_parameter(parameters, "m_delta"))

        plt.plot(x_values, data, alpha= 1, color = "#fb9a99", label=self.label)
        #plt.xticks(xticks, x_labels[::-1])
        plt.xlabel("$v$")
        plt.ylabel("Friction")
        plt.tight_layout()

    def plot_run(self, run_name):
        parameters = loader.load_yaml_parameters(run_name)
        data = loader.load_simulation_output(parameters, run_name, "pad_friction")
        #data = loader.load_simulation_output(parameters, run_name, "pad_position")
        store_data.append(data)
        x_values, n_steps = self.velocity_x_values(parameters, len(data))
        #self.label = self.direction_label(parameters)
        #interval = self.save_interval_label(parameters)
        #save_intervals.append(interval)
        self.label = str(loader.get_parameter(parameters, "N")) #N #save_interval_dt #seed #max_time
        self.label = str(loader.get_parameter(parameters, "start_speed_continuous"))
        self.label = str(loader.get_parameter(parameters, "m_zeta")) + " " + str(loader.get_parameter(parameters, "r_eta")) +" " + str(loader.get_parameter(parameters, "r_delta"))
        plt.plot(x_values, data, alpha=0.6,  label = self.label) 
        plt.xlabel("$v$")
        plt.ylabel("Friction")


    def plot_runs(self, run_list):
        figure = plt.figure()
        self.counter = 0
        plt.xlim(2.15,-0.05)
        for run_name in run_list:
            #self.plot_run_hardcode(run_name)
            self.plot_run(run_name)
            #self.counter += 1
            #loader.copy_scripts(run_name, "plot_friction_by_continuous_velocity.py")
        plt.legend(title = "$\\zeta$(damping ratio), $\\eta$(mass), $\\delta$(stiffness):") #Save interval: # "Seed" Blocks:
        #plt.legend(title = "Start speed:")
        sns.despine()
        #plt.title("Light blue: Only kc propotional damping. \n Dark blue: Only kp propotional damping. \n delta =0.001")   
        loader.save_figure(run_name,self.fig_name, figure)
        #plt.show()

    def plot_runs_ax(self, run_list): 
        x = 4
        y = 4
        figure, axs = plt.subplots(x,y)
        counter_x = 0
        counter_y = 0
        counter = 0
        colors =sns.color_palette("Paired",20)
        for run_name in run_list:
            color = colors[counter]
            parameters = loader.load_yaml_parameters(run_name)
            data = loader.load_simulation_output(parameters, run_name, "pad_friction")
            x_values, n_steps = self.velocity_x_values(parameters, len(data))
            #self.label = str(loader.get_parameter(parameters, "max_time"))
            self.label = str(loader.get_parameter(parameters, "N"))
            #self.label = str(loader.get_parameter(parameters, "start_speed_continuous"))
            #self.label = str(loader.get_parameter(parameters, "seed"))
            #self.label = str(loader.get_parameter(parameters, "m_zeta")) + " " + str(loader.get_parameter(parameters, "m_eta")) +" " + str(loader.get_parameter(parameters, "m_delta"))
            axs[counter_y, counter_x].plot(x_values, data, alpha=0.8, color = color, label=self.label) 
            #axs[counter_y, counter_x].legend(title = "$\\zeta$, $\\eta$, $\\delta$:")
            axs[counter_y, counter_x].legend(title="N:")
            axs[counter_y, counter_x].set_xlim([2.1,-0.1])
            if((counter_x+1) % y == 0):
                counter_x = 0
                counter_y += 1
            else:
                counter_x += 1
            counter +=1
        #plt.suptitle("Seed:" + str(loader.get_parameter(parameters, "seed")))
       # plt.suptitle("Same parameter settings, different runs.")
        sns.despine(trim=True)
        loader.save_figure(run_name,self.fig_name, figure)
        #plt.show()
        loader.copy_scripts(run_name, "plot_friction_by_continuous_velocity.py")




    def check_data_temp(self):
        diff_data = []
        counter = 0
        for point in store_data[0]:
            diff_data.append(point - store_data[1][10*counter])
            counter += 1
        plt.plot(diff_data)
        plt.show()


def run():
    start_speed = 0.15
    end_speed = 0.05
    trim=False
    figure_name = "friction_all"
    plotter = PlotFriction(start_speed, end_speed, figure_name=figure_name, trim=trim)
    if (len(sys.argv) > 2):
        run_name = sys.argv[1:]
        #plotter.plot_runs(run_name)
        plotter.plot_runs_ax(run_name)
    elif len(sys.argv) == 2:
        plotter.plot_runs([sys.argv[1]])
    else:
        print(
            'No input given, please provide one or more paths within the "results" folder'
        )

if __name__ == "__main__":
    run()

# TODO: take in pad_friction, pad_position etc. as arguments, so the user can choose. 
# TODO: pass in -h.
# Pass in a parameter specifying what is being compared.