import os
import sys
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
import yaml
from help_functions import LoadFile
loader = LoadFile()
import seaborn as sns

mpl.rcParams["savefig.directory"] = "../../Figures"
mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
#mpl.rc('text', usetex=True)

sns.set_style("dark")