from myimports_basic import *
store_data = []
save_intervals = []
sns.set_palette("PuBu_r")
mpl.rcParams["svg.fonttype"] = "none"
mpl.rcParams["font.size"] = 12
plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titlesize'] = 15
mpl.rcParams["savefig.directory"] = "../../Figures"
mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
mpl.rc('text', usetex=True)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)

# Class used to plot friction versus slider velocity.
# input:
# argv[1]: run_name
# argv[2]: start_velcoity
# argv[3]: end_velcoity
# argv[4]: run_name


class PlotFrictionSingle:
    def __init__(self, start_speed, end_speed, figure_name="friction", trim=False):
        self.trim = trim
        self.new_start_speed = float(start_speed)
        self.new_end_speed = float(end_speed)
        self.fig_name = figure_name

    def velocity_x_values(self, parameters, n_data_points):
        if not(loader.get_debug_parameter(parameters, "debug_continuous_slider_speed")):
            raise Exception ("Slider speed need to be contiious for this script to run")
        else:
            self.speeds = loader.get_start_end_speed(parameters)
            self.max_time = loader.get_parameter(parameters, "max_time")
            self.step_size = loader.get_parameter(parameters, "dt")
            self.save_interval_dt = loader.get_parameter(parameters, "save_interval_dt")
            self.speed_difference = self.speeds["end_speed"] - self.speeds["start_speed"]
            self.n_steps = self.max_time / self.step_size
            self.step_speed_increment = self.speed_difference / self.n_steps
            self.label = str(self.speeds["start_speed"])
            #end_speed = speeds["end_speed"] - step_speed_increment * (
            #    save_interval_dt - 1
            #)
            # linspace = np.linspace(speeds["start_speed"], end_speed, num=n_data_points)
            linspace = np.linspace(self.speeds["start_speed"], self.speeds["end_speed"], num=n_data_points)
            return linspace, self.n_steps
            

    def direction_label(self, parameters):
        speeds = loader.get_start_end_speed(parameters)
        if speeds["start_speed"] > speeds["end_speed"]:
            speed_direction = "decreasing"
        else:
            speed_direction = "increasing"
        return speed_direction


    def save_interval_label(self, paramters):
        save_interval = str(loader.get_parameter(paramters, "save_interval_dt"))
        return save_interval


    def plot_run(self, run_name):
        parameters = loader.load_yaml_parameters(run_name)
        data = loader.load_simulation_output(parameters, run_name, "pad_friction")
        x_values, n_steps = self.velocity_x_values(parameters, len(data))
        if(self.trim):
            data = loader.continuous_trim(data, len(data), self.speeds["start_speed"], self.speeds["end_speed"], self.new_start_speed, self.new_end_speed, False)
            x_values = np.linspace(self.new_start_speed, self.new_end_speed, num=len(data))
            plt.xlim(self.new_start_speed, self.new_end_speed)
        x_labels = np.linspace(self.new_start_speed, self.new_end_speed, 10)
        x_labels = [round(num, 3) for num in x_labels]
        xticks = np.linspace(len(x_values)-1,0, 10, dtype=np.int)
        self.label = str(loader.get_parameter(parameters, "seed"))
        self.label = str(loader.get_parameter(parameters, "m_zeta")) + " " + str(loader.get_parameter(parameters, "m_eta")) +" " + str(loader.get_parameter(parameters, "m_delta"))
        plt.plot(x_values, data, alpha= 1, label=self.label, color = "#1f78b4")
        plt.xlabel("$v$")
        plt.ylabel("$F$")
        plt.tight_layout()


    def plot_runs(self, run_name):
        figure = plt.figure()
        self.counter = 0
        plt.xlim(2.1,-0.1)
        
        self.plot_run(run_name)
        loader.copy_scripts(run_name, "plot_friction_by_continuous_velocity.py")
        #plt.legend(title = "Seed:")
        #plt.legend(title = "$\\zeta$(damping ratio), $\\eta$(mass), $\\delta$(stiffness):") #Save interval: # "Seed" Blocks:
        sns.despine()
        loader.save_figure(run_name,self.fig_name, figure)
        plt.show()


def run():
    trim=True
    figure_name = "friction"
    if (len(sys.argv) == 5):
        run_name = sys.argv[1]
        start_speed = sys.argv[2]
        end_speed = sys.argv[3]
        figure_name_end = sys.argv[4]
        plotter = PlotFrictionSingle(start_speed, end_speed, figure_name=figure_name+figure_name_end, trim=trim)
        plotter.plot_runs(run_name)
    else:
        raise Exception("Input run_name, start_speed, end_speed and end of figure name.")

if __name__ == "__main__":
    run()