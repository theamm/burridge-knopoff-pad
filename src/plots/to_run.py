
python3 plot_friction_by_continuous_velocity.py stiffness_damping_exp_1 stiffness_damping_delta=0.001 stiffness_damping_delta=0.005 stiffness_damping_delta=0.01 stiffness_damping_delta=0.05 stiffness_damping_delta=0.1 stiffness_damping_delta=0.5 stiffness_damping_delta=0.7 stiffness_damping_delta=1 

python3 stick_percentage.py constant_v=0.001_blocks_100 constant_v=0.002_blocks_100 constant_v=0.003_blocks_100 constant_v=0.004_blocks_100 constant_v=0.005_blocks_100 constant_v=0.006_blocks_100 constant_v=0.007_blocks_100 constant_v=0.008_blocks_100 constant_v=0.009_blocks_100 constant_v=0.01_blocks_100  constant_v=0.02_blocks_100 constant_v=0.03_blocks_100 constant_v=0.04_blocks_100 constant_v=0.05_blocks_100 constant_v=0.06_blocks_100 constant_v=0.07_blocks_100 constant_v=0.08_blocks_100 constant_v=0.09_blocks_100 constant_v=0.1_blocks_100 constant_v=0.2_blocks_100 constant_v=0.3_blocks_100 constant_v=0.4_blocks_100 constant_v=0.5_blocks_100 constant_v=0.6_blocks_100 constant_v=0.7_blocks_100 constant_v=0.8_blocks_100 constant_v=0.9_blocks_100 constant_v=1_blocks_100 constant_v=2_blocks_100 

python3 stick_percentage.py c=0.001 c=0.002 c=0.003 c=0.004 c=0.005 c=0.006 c=0.007 c=0.008 c=0.009 c=0.01 c=0.02 c=0.03 c=0.04 c=0.05 c=0.06 c=0.07 c=0.08 c=0.09 c=0.1 c=0.2 c=0.3 c=0.4 c=0.5 c=0.6 c=0.7 c=0.8 c=0.9 c=1 c=2 c=3

# for general behaviour:
python3 quick_plotting.py hundered_blocks_seed_101_save_100_block_50

# for large velocities: v= 1.5-1.6:
python3 quick_plotting.py hundered_blocks_seed_101_save_100_block_50

#for small velocities: v= 0.3-0.2:
python3 quick_plotting.py hundered_blocks_seed_101_save_100_block_50

# mass 100 blocks:
python3 plot_friction_by_continuous_velocity.py large_run_heatmaps mass_eta=0.001_100blocks mass_eta=0.01_100blocks mass_eta=0.1_100blocks mass_eta=0.2_100blocks mass_eta=0.3_100blocks mass_eta=0.5_100blocks mass_eta=0.7_100blocks mass_eta=1_100blocks

# Stiffness 100 blocks
python3 plot_friction_by_continuous_velocity.py large_run_heatmaps stifness_delta=0.001_heat stiffness_damping_delta=0.005_100blocks stiffness_damping_delta=0.01_100blocks stiffness_damping_delta=0.05_100blocks stiffness_damping_delta=0.1_100blocks stiffness_damping_delta=0.5_100blocks stiffness_damping_delta=0.7 stiffness_damping_delta=1


#stiffness 100 seed 100
python3 plot_friction_by_continuous_velocity.py stiffness_delta=0_seed_100 stifness_damping_delta=0.001_heat_seed_100 stifness_damping_delta=0.005_heat_seed_100 stifness_damping_delta=0.01_heat_seed_100 stifness_damping_delta=0.05_heat_seed_100 stifness_damping_delta=0.1_heat_seed_100 stifness_damping_delta=0.5_heat_seed_100 stifness_damping_delta=0.7_heat_seed_100 stifness_damping_delta=1_heat_seed_100

#stiffness 100 seed 103
python3 plot_friction_by_continuous_velocity.py stiffness_delta=0_seed_103 stiffness_delta=0.001_seed_103 stiffness_delta=0.005_seed_103 stiffness_delta=0.01_seed_103 stiffness_delta=0.05_seed_103 stiffness_delta=0.1_seed_103 stiffness_delta=0.5_seed_103 stiffness_delta=0.7_seed_103 stiffness_delta=1_seed_103

# delta = 0 
python3 plot_friction_by_continuous_velocity.py


# stifness 5 blocs 
python3 plot_friction_by_continuous_velocity.py large_run_heatmapsstiffness_damping_delta=0.001 stiffness_damping_delta=0.005 stiffness_damping_delta=0.01 stiffness_damping_delta=0.05 stiffness_damping_delta=0.1 stiffness_damping_delta=0.5 stiffness_damping_delta=0.7_5blocks stiffness_damping_delta=1_5blocks

# mass seed 101
python3 plot_friction_by_continuous_velocity.py stiffness_delta=0_seed_100 mass_eta=0.001_seed_100 mass_eta=0.01_seed_100 mass_eta=0.1_seed_100 mass_eta=0.2_seed_100 mass_eta=0.3_seed_100

## maxtime 4-0
#seed 100
python3 plot_friction_by_continuous_velocity.py seed_100_maxtime_10000 seed_100_maxtime_20000 seed_100_maxtime_40000 seed_100_maxtime_80000 seed_100_maxtime_160000 seed_100_maxtime_320000

python3 plot_friction_by_continuous_velocity.py seed_101_maxtime_10000 seed_101_maxtime_20000 seed_101_maxtime_40000 seed_101_maxtime_80000 seed_101_maxtime_160000 seed_101_maxtime_320000

python3 plot_friction_by_continuous_velocity.py seed_102_maxtime_20000 seed_102_maxtime_40000 seed_102_maxtime_80000 seed_102_maxtime_160000

python3 plot_friction_by_continuous_velocity.py seed_103_maxtime_20000 seed_103_maxtime_40000 seed_103_maxtime_80000 seed_103_maxtime_160000

python3 plot_friction_by_continuous_velocity.py seed_104_maxtime_20000 seed_104_maxtime_40000 seed_104_maxtime_80000 seed_104_maxtime_160000


## maxtime 2-0
#seed 100
python3 plot_friction_by_continuous_velocity.py seed_100_maxtime_10000_2_0 seed_100_maxtime_20000_2_0 seed_100_maxtime_40000_2_0 seed_100_maxtime_80000_2_0 seed_100_maxtime_160000_2_0 seed_100_maxtime_320000_2_0

python3 plot_friction_by_continuous_velocity.py seed_101_maxtime_10000_2_0 seed_101_maxtime_20000_2_0 seed_101_maxtime_40000_2_0 seed_101_maxtime_80000_2_0 seed_101_maxtime_160000_2_0 seed_101_maxtime_320000_2_0

#the same run a few times

python3 plot_friction_by_continuous_velocity.py seed_101_maxtime_80000_2_0 general_behaviour_seed_4 large_run_heatmaps large_run_heatmaps_mass_norm large_run_heatmap_mass_norm_2 hundered_blocks_general_3_blocks

# Seed: 17
python3 plot_friction_by_continuous_velocity.py general_behaviour_seed_1 seed_10 seed_20 seed_30 seed_40 seed_50 general_behaviour_seed_3 seed_70 seed_80 general_behaviour_seed_5 general_behaviour general_behaviour_seed_4 seed_130 seed_140 seed_150 seed_160

python3 plot_friction_single.py seed_160 0 2 "_seed_160_friction" 

general_behaviour_seed_2 


#start =4 vs start = 2 
python3 plot_friction_by_continuous_velocity.py seed_100_maxtime_80000 seed_100_maxtime_80000_2_0

python3 plot_friction_by_continuous_velocity.py seed_100_maxtime_160000 seed_100_maxtime_80000_2_0
python3 plot_friction_by_continuous_velocity.py seed_101_maxtime_160000 seed_101_maxtime_80000_2_0
python3 plot_friction_by_continuous_velocity.py seed_101_maxtime_160000 seed_100_maxtime_80000_2_0


## n blcoks
#seed 101
python3 plot_friction_by_continuous_velocity.py n_1_seed_101 n_5_seed_101 n_10_seed_101 fifteen_blocks_seed_101_save_10_ts80000 twenty_blocks_seed_101_save_10_ts80000 thirty_blocks_seed_101_save_100 fourty_blocks_seed_101_save_10_ts80000 n_50_seed_101 n_60_seed_101 n_70_seed_101 n_80_seed_101 n_90_seed_101 seed_101_maxtime_80000_2_0 n_110_seed_101 n_120_seed_101 n_150_seed_101

#seed 100
fifteen_blocks_seed_101_save_10_ts80000 twenty_blocks_seed_101_save_10_ts80000 thirty_blocks_seed_101_save_100 fourty_blocks_seed_101_save_10_ts80000 n_50_seed_101 n_60_seed_101 n_70_seed_101 n_80_seed_101 n_90_seed_101 seed_101_maxtime_80000_2_0 n_110_seed_101 n_120_seed_101 n_150_seed_101

python3 plot_friction_by_continuous_velocity.py n_1_seed_100 n_5_seed_100 n_10_seed_100 n_15_seed_100 twenty_blocks_seed_100_save_10_ts80000 thirty_blocks_seed_100_save_10_ts80000 fourty_blocks_seed_100_save_10_ts80000 n_50_seed_100 n_60_seed_100 n_70_seed_100 n_80_seed_100 n_90_seed_100 general_behaviour n_110_seed_100 n_120_seed_100 n_150_seed_100

#stiffness cp=0
python plot_friction_by_continuous_velocity.py stiffness_delta=0_zeta=0 stiffness_delta=0.001_zeta=0  stiffness_delta=0.005_zeta=0  stiffness_delta=0.01_zeta=0  stiffness_delta=0.05_zeta=0  stiffness_delta=0.1_zeta=0  stiffness_delta=0.5_zeta=0  stiffness_delta=0.7_zeta=0  stiffness_delta=1_zeta=0

# n blocks bk
python3 plot_friction_by_continuous_velocity.py 10_02_bk_1_block 10_02_bk_5_block 10_02_bk_10_block 10_02_bk_15_block 10_02_bk_20_block 10_02_bk_30_block 10_02_bk_40_block 10_02_bk_50_block 10_02_bk_60_block 10_02_bk_70_block 10_02_bk_80_block 10_02_bk_90_block 10_02_bk_100_block 10_02_bk_110_block 10_02_bk_130_block 10_02_bk_150_block
