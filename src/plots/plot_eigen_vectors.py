from myimports_basic import *
import math
from single_line import SingleLine, SinglePlot
sns.set_style("white")

class PlotEigenVectors:
    def __init__(self, run_name, k):
        self.run_name = run_name
        self.k = int(k)
        self.figs_per_page = 1
        eigen_vectors, eigen_values = loader.load_eigen_vectors(run_name)
        self.title_low = True
        for i in range(2):
            if(i == 0):
                self.eigen_values = eigen_values[-self.k:]
                self.eigen_vectors = eigen_vectors[:,-self.k:]*4
            if(i == 8):
                self.eigen_values = eigen_values[1:4]
                self.eigen_vectors = eigen_vectors[:,1:4]
                self.k = 3
                self.title_low = True
        self.get_scaling_number()
        self.loop_vectors()

            
    def loop_vectors(self):
        counter = 0;
        for j in range(self.k):
            if ((j+1) % self.figs_per_page == 1 or self.figs_per_page == 1):
                counter += 1
                self.fig, self.axs = plt.subplots(self.figs_per_page, gridspec_kw={'hspace': 1})
                self.title = "Mode: " + str(self.k-j-1)if self.title_low else "Mode: " + str(100-j) 
                #self.title = "Low eigenmodes - page " + str(counter) + "/" + str(math.ceil(self.k/self.figs_per_page)) if self.title_low else "High eigenmode."
                self.fig.suptitle(self.title)
            vector = self.eigen_vectors[:,j]
            self.i = j
            self.spring_plot = self.make_single_line_spring_plot(vector)
            self.init_specified_subplot(self.spring_plot)
            if ((j+1) % self.figs_per_page == 0 or (j) == self.k-1):
                plt.axis('off')
                title = "eigen"+str(counter) if self.title_low else "eigen_high"+str(counter)
                sns.despine(trim=True)
                loader.save_figure(self.run_name, title, self.fig)
                #plt.show()


    def get_scaling_number(self):
        last_vector = self.eigen_vectors[:,0]
        last_vector.sort()
        max_vector_value = last_vector[-1]
        self.scaler = math.ceil(max_vector_value)
        self.scaler = 1


    def make_single_line_spring_plot(self, vector):
        ax_number = 3
        line_number = 0
        common_indexes = [0]
        show_pad=True
        height_of_plot = 2
        spring_lim = {'min': -height_of_plot/2, 'max': height_of_plot}

        n_spring_values = len(vector)
        pad_position = spring_positions = np.array(np.arange(0, n_spring_values)*self.scaler)
        spring_values = vector
        line_dependencies = {'ax_numbers': [ax_number], 'line_numbers': [spring_positions]}
        spring_indexes = [spring_positions[0], spring_positions[-1]]

        spring_line = SingleLine(x_values = spring_values, \
            y_values = np.array([0.0]*n_spring_values), \
            starting_position = {"x":spring_positions, "y": 0.0}, \
            x_indexes=spring_indexes, \
            y_indexes=spring_indexes, \
            ax_number = ax_number, \
            line_number = line_number, \
            line_dependencies=line_dependencies,
            marker="s",
            marker_size=5.0)

        line_shifter = 1
        line_list = [spring_line]

        if(len(pad_position) > 0 and len(common_indexes) > 0 and show_pad):
            pad_line = SingleLine(x_values = np.array([pad_position[common_indexes[-1]]]), \
                y_values = np.array([height_of_plot/2]*2), \
                starting_position = {"x":np.array(spring_positions[[0,-1]]), "y": 0.0}, \
                x_indexes = np.array([0, 1]), \
                y_indexes = np.array([0, 1]), \
                ax_number = ax_number, \
                line_number = line_number + 1, \
                line_dependencies= {'ax_numbers': [ax_number], 'line_numbers': [np.array([line_number + 2]*2)]}, \
                line_width= 10.0,
                color = "#a6cee3")
            pad_values = SingleLine(x_values = np.array(pad_position), \
                y_values = np.array([height_of_plot/2]),
                x_indexes = [common_indexes[-1]]*2,
                y_indexes = [0]*2,
                ax_number = ax_number,
                line_number = line_number + 2,
                marker = '+'
                )
            line_shifter += 0
            line_list += [pad_line, pad_values]

        for i, (spring_value, spring_position) in enumerate(zip(spring_values, spring_positions)):
            x_values = np.array([spring_position + spring_value, spring_position])
            y_values = np.array([0, height_of_plot/2])
            common_indexes = np.array([0,1])
            single_line = SingleLine(\
                x_values, \
                y_values, \
                x_indexes = common_indexes, \
                y_indexes = common_indexes, \
                ax_number = ax_number, \
                line_number = i + line_shifter,
                color = "#1f78b4")
            line_list.append(single_line)

        spring_plot = SinglePlot(ax_number, \
            single_line_list = line_list, \
            x_lim={'min': -3.0 + spring_positions[0], \
            'max': 3.0 + spring_positions[-1]}, \
            y_lim=spring_lim, \
            plot_type = 'spring',
            x_label = "x label",
            y_label = "y_label")
        return spring_plot

    def init_specified_subplot(self,single_plot):
        for single_line in single_plot.single_line_list:
            self.init_spring_plot(single_line)

    def init_spring_plot(self,single_line):
        #self.axs[self.i % self.figs_per_page].
        plt.plot(\
            single_line.x_values[single_line.x_i[0]:(single_line.x_i[1] + 1)] + single_line.starting_position["x"],\
            single_line.y_values[single_line.y_i[0]:(single_line.y_i[1] + 1)] + single_line.starting_position["y"],\
            marker = single_line.marker,
            markersize = single_line.marker_size,
            linewidth = single_line.line_width,
            color = single_line.color
        )
        #self.axs[self.i % self.figs_per_page].set_title
        plt.title(str(round(self.eigen_values[self.i], 2)))

def run():
    if(len(sys.argv) == 3):
        plot = PlotEigenVectors(sys.argv[1], sys.argv[2])
    else:
        Exception("You need to input two arguments: the run_name and number of modes you want to plot.")

if __name__ == "__main__":
    run()

