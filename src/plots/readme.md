#  src/plots

Scripts to plot the simulations. 
All scripts, except for help_functions.py takes in one or several run names as arguments.

## Scripts

### stick_persentage.py
    Plots the percentage in which the block velcoity is equal to the slider velocity. It only workes with simulations of constant slider velocity.     
    By calling the method calculate_stick_persentage_single(), the script will calculate stick persentage based on the output in block_position.csv.
    The stick percentage is also calculated in Simulations.cpp, which saves the total stick percentage to a .txt file in the run_name folder and is opened by PlotStickPersentage. 
    This is more exact than calling calculate_stick_persentage_single() as many runs does not save every time step or every block.

### standard_deviation.py
    Script that caluclates how a run diffese from the mean of all the runs are inputed as argument. 

### to_run.py
    A draft script used to plan what to run.
    When plotting figures that contain several simulaitons, it can get messy in the terminal/command window, hence writing the command in as a draft can be handy.
    It is included (instead of deleted) so that the reader can see how the scripts are called by the the master student. 

### quick_plotting.py
    Scripts that plots the pad position, block position, pad velocity, block velocity against time and a phase plot of the block velocity against block position.

### plot_friction_by_continuous_velocity.py
    Script that plots the friction as a function of velocity for many runs.
    Requires continuous increasing or decreasing slider velcoity.

### plot_friction_single.py
    Script that plots the friction as a function of velocity for a single run.
    Requires continuous increasing or decreasing slider velcoity.

### single_line.py
    Script with help methods for plot_eigen_vecotrs.py.

### plot_eigen_vecotrs.py
    Plots the eigenvectors/mode shapes where each value in the mode shape is plotted as a verical line directed according to its value.

### plot_eigen_vecots_bars.py
    Plots the eigenvectors as bar plots where each value in the mode shapes are plotted as verticle bars which hight is decided by the value.

### help_functions.py
    Includes help functions to load and save scripts and figures.
    It also includes functions to trim the velcoity range of the csv.files.

### heat_map.py
    Creates heat maps from pad_position, pad_velocity, block_position and block_veclocity.

### heat_map_only_plot.py
    Creates heat maps from already existing heat map matrices, which can be creates by the c++ class Operations. 

### quick_plotting.py
    Used to plot position and velocity of blocks and pad with respect to time.

### plot_postion_single.py
    To plot pad position versus slider velocity.

### plot_postion_continious_velcoity.py
    To compare many pad postion plots with respect to slider velocity. 


