try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
from help_functions import LoadFile
sns.set(font_scale = 0.7)
sns.set_style("dark")
mpl.rcParams["font.size"] = 12
plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titlesize'] = 12
mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
mpl.rc('text', usetex=True)

import math
from tqdm import tqdm
import time
import statistics 