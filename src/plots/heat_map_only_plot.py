from myimports_basic import *
import math
sns.set_style("white")
plt.rcParams['axes.labelsize'] = 15
plt.rcParams['axes.titlesize'] = 15
mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
mpl.rc('text', usetex=True)

# Creates heat maps from already existing heat map matrices, which can be creates by the c++ class Operations. 
# Input:
# argv[1]: run_name
# See run()-methos (bottom) for setting some boolean values. 

class HeatMapPlot:
    def __init__(self, run_name, start_velo = 0, end_velo = 2, trim=False, remove_0_and_5=False, remove_high_modes=False, figure_name="heatmap",plot_each_mode=False):
        self.start_velo = start_velo
        self.end_velo = end_velo
        self.figure_name = figure_name
        self.run_name = run_name
        self.load_files()
        self.remove_high_modes = remove_high_modes
        if(trim):
            self.trim_velocity_interval(start_velo, end_velo)
        self.remove_0_and_5 = remove_0_and_5
        if(remove_0_and_5):
            self.remove_mode_5_and_0()
        if(remove_high_modes):
            self.remove_high_modes_m()
        if(plot_each_mode):
            self.plot_each_mode()
        self.run_create_plot()
    
    def load_files(self):
        self.parameters = loader.load_yaml_parameters(self.run_name)
        self.position_heat, self.velocity_heat, self.energy_heat = loader.load_heat_map_matrices(self.run_name)
        print("done load")

    def run_create_plot(self):
        if(loader.get_parameter(self.parameters, "N") == 100):
            self.create_heat_map(self.position_heat)
            self.create_heat_map(self.velocity_heat, "velocity_heat_map")
            self.create_heat_map(self.energy_heat, "energy_heat_map")
        else:
            self.create_heat_map_few_blocks(self.position_heat)
            self.create_heat_map_few_blocks(self.velocity_heat, "velocity_heat_map")
            self.create_heat_map_few_blocks(self.energy_heat, "energy_heat_map")

    def create_heat_map_few_blocks(self, matrix, fig_name=""):
        figure_name=self.figure_name+fig_name
        self.x_values = np.linspace(self.start_velo, self.end_velo, 10)
        self.x_values = [round(num, 3) for num in self.x_values]
        matrix = np.flip(matrix, axis=0)
        fig = plt.figure()
        x = matrix.shape[1]
        xticks = np.linspace(0, x-1, 10, dtype=np.int)
        # self.y_values = np.linspace(0, 100, 20)
        # matrix = np.flip(matrix,1)
        self.x_values = np.flip(self.x_values)
        ax = sns.heatmap(matrix, xticklabels=self.x_values)
        plt.xlabel("$v$")
        plt.ylabel('Mode')
        plt.xticks(xticks, self.x_values, fontsize= 11, rotation=0)
        ax.invert_yaxis()
        loader.save_figure(self.run_name, figure_name, fig)
        plt.show()


    def create_heat_map(self, matrix, fig_name=""):
        figure_name=self.figure_name+fig_name
        self.x_values = np.linspace(self.start_velo, self.end_velo, 10)
        self.x_values = [round(num, 3) for num in self.x_values]
        cmap = sns.diverging_palette(230, 20, as_cmap=True)
        matrix = np.flip(matrix, axis=0)
        fig = plt.figure()
        i = matrix.shape[1]
        xticks = np.linspace(0, i-1, 10, dtype=np.int)
        self.y_values = np.linspace(0, 100, 20)
        if(self.remove_high_modes):
            self.y_values = np.linspace(0, 20, 21)
        if(self.remove_0_and_5 and self.remove_high_modes):
            self.y_values = np.linspace(1, 20, 20)
            self.y_values = np.delete(self.y_values, 4)
        self.y_values = [int(n) for n in self.y_values]
        # Uncomment if increasing slider velocity : 
            # matrix = np.flip(matrix,1)
            # self.x_values = np.flip(self.x_values)
        ax = sns.heatmap(matrix, xticklabels=self.x_values, yticklabels = self.y_values, cmap="PuBuGn") 
        plt.xlabel("$v$")
        plt.ylabel('Mode')
        plt.xticks(xticks, self.x_values, fontsize= 11, rotation=0)
        if(self.remove_high_modes):
            yticks = np.linspace(0, 20, 21, dtype=np.int)
            yticks = [x+0.4 for x in yticks]
        else:
            yticks = np.linspace(0, 101, 20, dtype=np.int)
        
        if(self.remove_0_and_5):
            self.y_values = np.linspace(1, 20, 20)
            self.y_values = np.delete(self.y_values, 4)
            yticks = np.linspace(0, 19, 19)
            yticks = [x+0.1 for x in yticks]
            if(self.bk):
                self.y_values = np.linspace(1, 20, 20)
                yticks = np.linspace(0, 20, 20)
                yticks = [x+0.1 for x in yticks]
            self.y_values = [int(n) for n in self.y_values]
            plt.yticks(yticks, self.y_values, fontsize= 10, rotation=360)
        else:
            self.y_values = [int(n) for n in self.y_values]
            plt.yticks(yticks, self.y_values, fontsize= 11, rotation=360, va="center")
        ax.invert_yaxis()
        loader.save_figure(self.run_name, figure_name, fig)
        plt.show()



    def trim_velocity_interval(self, start_velo, end_velo):
        new_start_speed = start_velo
        new_end_speed =end_velo
        speeds = loader.get_start_end_speed(self.parameters)
        start_speed = speeds["start_speed"]
        end_speed = speeds["end_speed"]
        n_steps = self.position_heat.shape[1]
        self.position_heat = loader.continuous_trim(self.position_heat, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, True)
        self.velocity_heat = loader.continuous_trim(self.velocity_heat, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, True)
        self.energy_heat = loader.continuous_trim(self.energy_heat, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, True)
        print("done trim")

    def remove_high_modes_m(self):
        bk = loader.get_debug_parameter(self.parameters, "debug_no_pad")
        if(not bk):
            self.position_heat = self.position_heat[80:101,:]
            self.energy_heat = self.energy_heat[80:101,:]
            self.velocity_heat = self.velocity_heat[80:101,:]
        else:
            self.position_heat = self.position_heat[79:101,:]
            self.energy_heat = self.energy_heat[79:101,:]
            self.velocity_heat = self.velocity_heat[79:101,:]
        

    def remove_mode_5_and_0(self):
        self.bk = loader.get_debug_parameter(self.parameters, "debug_no_pad")
        if(not self.bk):
            self.position_heat = np.delete(self.position_heat, 100, axis = 0)
            self.position_heat = np.delete(self.position_heat, 95, axis = 0)
            
            self.velocity_heat = np.delete(self.velocity_heat, 100, axis = 0)
            self.velocity_heat = np.delete(self.velocity_heat, 95, axis = 0)
            
            self.energy_heat = np.delete(self.energy_heat, 100, axis = 0)
            self.energy_heat = np.delete(self.energy_heat, 95, axis = 0)
        else:
            self.position_heat = np.delete(self.position_heat, 99, axis = 0)
            self.velocity_heat = np.delete(self.velocity_heat, 99, axis = 0)
            self.energy_heat = np.delete(self.energy_heat, 99, axis = 0)
    
    def plot_each_mode(self):
        n_modes = self.position_heat.shape[0]
        for i in range(n_modes):
            fig = plt.figure()
            mode_displacemement = self.position_heat[i,:]
            sns.lineplot(data = mode_displacemement, color = "#74c476")
            plt.ylabel('e')
            plt.xlabel('t')
            sns.despine(trim=True)
            loader.save_figure(self.run_name, "mode:" + str(i), fig)
        #plt.show()

    def displacement(self):
        n_blocks = self.block_position.shape[0]
        for i in range(n_blocks):
            fig = plt.figure()
            block_displacemement = self.block_position[i,:]
            title = "Displacement block " + str(i)
            sns.lineplot(data = block_displacemement)
            #fig.suptitle(title)
            plt.ylabel('displacement')
            plt.xlabel('time')
            sns.despine(trim=True)
            loader.save_figure(self.run_name, "diplacement_mode_"+str(i), fig)
        plt.show()



def run():
    start = 1.115
    end  = 1.11
    trim=False
    remove_0_and_5=False
    remove_high_modes=True
    plot_each_mode = True
    figname = "heat_map"
    if(len(sys.argv) == 5):
        plot = HeatMapPlot(sys.argv[1], float(sys.argv[2]), float(sys.argv[3]), trim=trim, remove_0_and_5=remove_0_and_5, remove_high_modes=remove_high_modes, figure_name="heat_map_" + str(sys.argv[4]), plot_each_mode=plot_each_mode)
    else:
        raise Exception("You need to input three arguments: the run_name, start velocity, end velocity and figure name.")

if __name__ == "__main__":
    run()