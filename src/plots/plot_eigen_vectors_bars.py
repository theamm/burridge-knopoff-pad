from myimports_basic import *
import numpy.linalg as linalg
sns.set_palette("Paired")
sns.set_style("white")
plt.rcParams['axes.labelsize'] = 15
plt.rcParams['axes.titlesize'] = 15
mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
mpl.rc('text', usetex=True)

# Class to plot the eigenvectors as bar plots where each value in the mode shapes are plotted as verticle bars which hight is decided by the value.

class PlotEigenVectors:
    def __init__(self, run_name, k):
        self.run_name = run_name
        self.k = int(k)
        self.eigen_vectors, self.eigen_values = loader.load_eigen_vectors(run_name)
        #self.sort_eigen_values()
        self.plot_low_high_vectors(self.eigen_vectors, self.eigen_values)

    def sort_eigen_values(self):
        idx = self.eigen_values.argsort()[::-1]   
        self.eigen_values = self.eigen_values[idx]
        self.eigen_vectors = self.eigen_vectors[:,idx]
    
    def plot_low_high_vectors(self, eigen_vectors, eigen_values):
        self.title_low = True
        for i in range(1):
            if(i == 0):
                print(self.eigen_values)
                self.eigen_values = eigen_values[-self.k:]
                print(self.eigen_values)
                self.eigen_vectors = eigen_vectors[:,-self.k:]
            if(i == 1):
                print("notheing")
            self.loop_vectors()
    
    def loop_vectors(self):
        counter = 0;
        for j in range(self.k):
            i = self.k - j - 1
            eigen_value = self.eigen_values[i]
            self.fig, self.axs = plt.subplots(1, gridspec_kw={'hspace': 1})
            self.title = "Mode: " + str(counter) + "  Eigenvalue: " + str(round(eigen_value, 2)) if self.title_low else "High eigenvalue: " + str(eigen_value)
            self.fig.suptitle(self.title)
            vector = self.eigen_vectors[:,i]
            x = np.array(np.arange(0, len(vector)))
            self.axs.bar(x, vector)
            sns.despine(trim=True)
            title = "eigen_bars_"+str(counter) if self.title_low else "eigen_high_bars_"+str(counter)
            loader.save_figure(self.run_name,title, self.fig)
            counter += 1

def run():
    if(len(sys.argv) == 3):
        plot = PlotEigenVectors(sys.argv[1], sys.argv[2])
    else:
        Exception("You need to input two arguments: the run_name and number of modes you want to plot.")

if __name__ == "__main__":
    run()
