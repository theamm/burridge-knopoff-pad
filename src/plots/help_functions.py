import os
import sys
from matplotlib import pyplot as plt
import numpy as np
import yaml
import shutil
from shutil import copyfile
import scipy.misc

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class LoadFile:
    def __init__(self):
        self.loadedYaml = False
        self.results_path = (
            os.path.dirname(os.path.realpath(__file__)) + "/../../results/"
        )
        self.numeric_method = "_midpoint_"

    def set_results_path(self, results_path):
        self.results_path = results_path

    def set_numeric_method(self, numeric_method):
        self.numeric_method = numeric_method

    def load_file(self, filename):
        return np.loadtxt(filename, delimiter=",")

    def load_simulation_output(
        self, yaml_parameters, run_name, output_name,  filetype = ".csv", use_shared_names=True,
    ):
        numeric_method = ""
        if use_shared_names:
            shared_names = self.get_parameter(yaml_parameters, "file_name")
            numeric_method = self.numeric_method
        else:
            shared_names = ""
        path_to_file = (
            self.results_path
            + run_name
            + "/"
            + shared_names
            + numeric_method
            + output_name
            + filetype
        )
        return self.load_file(path_to_file)
    
    def get_simulation_path(self, run_name=""):
        return self.results_path + run_name

    def check_if_yaml_has_been_loaded(self):
        if self.loadedYaml:
            print("Some yaml has been loaded before with this instance-loader.")
        else:
            print("There has not been a yaml loaded before on this loader.")

    def load_yaml_parameters(self, run_name):
        self.loadedYaml = True
        path = str(self.results_path) + str(run_name) + "/parameters.yaml"
        stream = open(path, "r")
        return yaml.load(stream, Loader=Loader)

    def loadYaml(self, run_name, file_name):
        stream = open(self.results_path + run_name + file_name, "r")
        return yaml.load(stream, Loader=Loader)

    def get_parameter(self, run_parameters, parameter_name):
        return run_parameters["Parameters"][parameter_name]

    def get_debug_parameter(self, run_parameters, debug_parameter_name):
        return run_parameters["Debug"][debug_parameter_name]

    def get_start_end_speed(self, parameters):
        start_speed = self.get_parameter(parameters, "start_speed_continuous")
        end_speed = self.get_parameter(parameters, "end_speed_continuous")
        return {"start_speed": start_speed, "end_speed": end_speed}

    def check_if_file_exist_in_run_folder(self, run_name, file_name=""):
        if os.path.isfile(self.results_path + run_name + file_name):
            return True
        else:
            return False

    def save_csv(self, run_name, file_name, array_or_matrix):
        file_path = self.results_path + run_name + file_name
        np.savetxt(file_path, array_or_matrix, delimiter=",")

    def save_figure(self, run_name, file_name, fig):
        filepath = self.results_path + run_name + "/Figures/" + run_name + "_" + file_name + ".png"
        print(filepath)
        fig.set_size_inches(12, 8)
        plt.savefig(filepath, dpi=500, bbox_inches= 'tight', pad_inches=0.1)
        #Change to dip=1000 if using image in thesis.

    def load_eigen_vectors(self,run_name):
        vector_path = self.results_path + run_name + "/" + run_name + "_eigenvectors.csv"
        value_path = self.results_path + run_name + "/" + run_name + "_eigenvalues.csv"
        vectors = np.loadtxt(vector_path, delimiter=",")
        values = np.loadtxt(value_path)
        return vectors, values
    
    def sort_eigen_values(self, eigen_values, eigen_vectors):
        idx = eigen_values.argsort()[::-1]   
        eigen_values = eigen_values[idx]
        eigen_vectors = eigen_vectors[:,idx]
        return eigen_values, eigen_vectors

    def remove_first_mode(self, eigenvalues, eigenvectors):
        n = len(eigenvalues)
        eigenvalues = eigenvalues[0:n-1]
        eigenvectors = eigenvectors[:,0:n-1]
        return eigenvalues, eigenvectors

    def continuous_trim(self, data, timesteps, start_speed, end_speed, new_start_speed, new_end_speed, blocks):
        start_speed_index = int(((new_start_speed - start_speed)/(end_speed - start_speed) * timesteps))
        end_speed_index = int(((new_end_speed - start_speed)/(end_speed - start_speed) * timesteps))
        if (blocks):
            data = data[:,start_speed_index:end_speed_index] 
        else:
            data = data[start_speed_index:end_speed_index] 
        return data

    def load_diagonal_mass_stiffness(self,run_name):
        vector_path = self.results_path + run_name + "/" + run_name + "_diagonal_mass_matrix.csv"
        value_path = self.results_path + run_name + "/" + run_name + "_diagonal_stiffness_matrix.csv"
        mass_matrix = np.loadtxt(vector_path, delimiter=",")
        stiffness_matrix = np.loadtxt(value_path, delimiter=",")
        return mass_matrix, stiffness_matrix

    def load_heat_map_matrices(self,run_name):
        path = self.results_path + run_name + "/" + run_name
        velocity_heat_path = path + "_velocity_matrix_heat.csv"
        #velocity_heat_path = path + "_velocity_matrix_heat_mean_removed.csv"
        #position_heat_path = path + "_position_matrix_heat_mean_removed.csv"
        position_heat_path = path + "_position_matrix_heat.csv"
        #energy_heat_path = path + "_energy_matrix_heat_mean_removed.csv"
        energy_heat_path = path + "_energy_matrix_heat.csv"
        velocity_heat = np.loadtxt(velocity_heat_path, delimiter=",")
        position_heat = np.loadtxt(position_heat_path, delimiter=",")
        energy_heat = np.loadtxt(energy_heat_path, delimiter=",")
        return position_heat, velocity_heat, energy_heat


    def load_steady(self, run_name):
        block_path = self.results_path + run_name + "/" + run_name + "_block_positon_steady.csv"
        pad_path = self.results_path + run_name + "/" + run_name + "_pad_position_steady.csv"
        block_matrix_steady = np.loadtxt(block_path, delimiter=",")
        pad_matrix_steady = np.loadtxt(pad_path, delimiter=",")
        return block_matrix_steady, pad_matrix_steady

    def copy_scripts(self, run_name, script_name):
        dst = self.results_path + run_name + "/code"     
        src = os.path.dirname(os.path.realpath(__file__)) + "/" + script_name   
        src_help = os.path.dirname(os.path.realpath(__file__)) + "/help_functions.py"
        shutil.copy2(src_help,dst)
        shutil.copy2(src,dst)
