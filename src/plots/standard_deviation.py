from myimports import*
from myimports_sp import * 

# Script that caluclates how a run differs from the mean of all the runs are inputed as argument. 
# Input: Two or more run names.

class StandardDeviation():
    def __init__(self, run_names):
        self.run_names = run_names
        self.load()
        self.calculate_mean()
        self.calculate_deviation()
        self.create_bar_plot()
        self.create_regular_plot()    

    def load(self):
        self.data_list = []
        self.labels = []
        for run_name in self.run_names:
            parameters = loader.load_yaml_parameters(run_name)
            self.data_list.append(loader.load_simulation_output(parameters, run_name, "pad_friction"))
            self.labels.append(loader.get_parameter(parameters, "seed"))

    def calculate_mean(self):
        self.mean_list = []
        n_timesteps = len(self.data_list[0])
        n_runs = len(self.data_list)
        for i in tqdm(range(n_timesteps)):
            mean = 0
            for run in self.data_list:
                if not(len(run) == n_timesteps):
                    Exception("RUNS NEED TO BE OF EQUAL LENGTH!")
                mean += run[i]
            mean = mean / n_runs
            self.mean_list.append(mean)
        print(len(self.mean_list))

    def calculate_deviation(self):
        self.sd_list = []
        n_timesteps = len(self.data_list[0]) 
        self.sd_normal_plot = np.empty((0, n_timesteps))
        print(self.sd_normal_plot.shape)
        for run in tqdm(self.data_list):
            sd = np.subtract(self.mean_list, run)**2
            self.sd_normal_plot = np.append(self.sd_normal_plot, np.array([sd]), axis=0)
            sd = (sum(sd) / n_timesteps)**(1/2)
            self.sd_list.append(sd)
        print(self.sd_normal_plot.shape)

    def create_regular_plot(self):
        print(self.sd_normal_plot[1,0:200])
        for i in range(self.sd_normal_plot.shape[0]):
            y = self.sd_normal_plot[i,:]
            x = np.array(np.arange(0, self.sd_normal_plot.shape[1]))
            plt.plot(x, y) 
        sns.despine(trim=True)
        plt.show()


    def create_bar_plot(self):
        title = "Standard deviation"
        x = np.array(np.arange(0, len(self.sd_list)))
        height = self.sd_list
        plt.bar(x, height)
        sns.despine(trim=True)
        plt.show()
            

def run():
    if(len(sys.argv) >= 3):
        run_name = sys.argv[1:]
        plotter = StandardDeviation(run_name)
    else:
        Exception("You need to input at least two run_names.")

if __name__ == "__main__":
    run()