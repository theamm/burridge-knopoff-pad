from myimports_basic import *
from tqdm import tqdm
import time
sns.set_style("white")
sns.set_palette("PuBuGn")

# Creates heat maps from pad_position, pad_velocity, block_position and block_veclocity.
# It is better to generate the heat map matrices using c++ Operation class and plot with heat_map_only_plot.py. 

class HeatMap:
    def __init__(self, run_name, remove_first_mode, edit_first_mode):
        self.run_name = run_name
        self.edit_first_mode = edit_first_mode
        self.load_files()
        self.eigen_values, self.eigen_vectors = loader.sort_eigen_values(self.eigen_values,self.eigen_vectors)        
        self.merge_block_pad()
        if(remove_first_mode):
            self.eigen_values, self.eigen_vectors = loader.remove_first_mode(self.eigen_values, self.eigen_vectors)
      #  self.remove_first_time_steps()
        self.create_all_matrices()
        #self.plot_mode(5)
        loader.copy_scripts(run_name, "heat_map.py")
    
    def load_files(self):
        self.parameters = loader.load_yaml_parameters(self.run_name)
        self.eigen_vectors, self.eigen_values = loader.load_eigen_vectors(self.run_name)
        self.block_position = loader.load_simulation_output(self.parameters, self.run_name, "block_position")
        self.pad_position = loader.load_simulation_output(self.parameters, self.run_name, "pad_position")
        self.pad_velocity = loader.load_simulation_output(self.parameters, self.run_name, "pad_velocity")
        self.block_velocity = loader.load_simulation_output(self.parameters, self.run_name, "block_velocity")
        self.mass_matrix, self.stiffness_matrix = loader.load_diagonal_mass_stiffness(self.run_name)

    def load_steady_matrices(self):
        self.steady_block, self.steady_pad = loader.load_steady(self.run_name)
        self.steady_block = np.append(self.steady_block, [self.steady_pad], axis = 0)        


    def create_zero_matrices(self, n_eigen_values, n_time_steps):
        position_matrix = np.zeros((n_eigen_values, n_time_steps))
        velocity_matrix = np.zeros((n_eigen_values, n_time_steps))
        energy = np.zeros((n_eigen_values, n_time_steps))
        return position_matrix, velocity_matrix, energy
    
    def create_amplitude_arrays(self):
        position_amplitude_vector = np.array([[]]) 
        velcity_amplitude_vector = np.array([[]]) 
        energy_amplitude_vecotor = np.array([[]]) 
        return position_amplitude_vector, velcity_amplitude_vector, energy_amplitude_vecotor
    
    # Energy
    # T = 0.5 * A.t()*M*A * (aj dot v)^2
    # V = 0.5 * A.t()*K*A * (A dot d)^2
    def create_all_matrices(self):
        n_eigen_values = len(self.eigen_values)
        n_time_steps = len(self.block_position[0,:])
        position_matrix, velocity_matrix, energy = self.create_zero_matrices(n_eigen_values, n_time_steps) 
        print(self.eigen_vectors)
        for i in tqdm(range(n_eigen_values)): 
            a_eigen_vector = self.eigen_vectors[:,i]
            position_amplitude_vector, velcity_amplitude_vector, energy_amplitude_vecotor = self.create_amplitude_arrays()
            for j in tqdm(range(n_time_steps)):
                position_amplitude = np.array(a_eigen_vector) @ self.block_position[:,j]

                velcity_amplitude = np.array(a_eigen_vector) @ self.block_velocity[:,j]
                    
                e = position_amplitude
                edot =  velcity_amplitude
                energy_amplitude = 0.5 * self.mass_matrix[i,i]*edot**2 + 0.5*self.stiffness_matrix[i,i] * e**2

                position_amplitude_vector = np.append(position_amplitude_vector, position_amplitude)
                velcity_amplitude_vector = np.append(velcity_amplitude_vector, velcity_amplitude)
                energy_amplitude_vecotor = np.append(energy_amplitude_vecotor, energy_amplitude)
            
            position_matrix[i,:] = position_amplitude_vector
            velocity_matrix[i,:] = velcity_amplitude_vector
            energy[i, :] = energy_amplitude_vecotor
        print(energy)
        self.create_heat_map(position_matrix, "_position_eigen")
        self.create_heat_map(velocity_matrix, "_velocity_eigen")
        self.create_heat_map(energy, "_energy_eigen")

    def sort_eigen_values(self):
        idx = self.eigen_values.argsort()[::-1]   
        self.eigen_values = self.eigen_values[idx]
        self.eigen_vectors = self.eigen_vectors[:,idx]

    def merge_block_pad(self):
        self.block_position = np.append(self.block_position, [self.pad_position], axis=0)
        self.block_velocity = np.append(self.block_velocity, [self.pad_velocity], axis=0)

    def remove_modes(self):
        length = len(self.eigen_values)
        # Remove first and last
        self.eigen_vectors = self.eigen_vectors[1:length-1,1:length-1]
        self.eigen_values = self.eigen_values[1:length-1]
        self.block_position = self.block_position[1:length-1,:]
        self.block_velocity = self.block_velocity[1:length-1,:]
        self.mass_matrix = self.mass_matrix[1:length-1,1:length-1]
        self.stiffness_matrix = self.stiffness_matrix[1:length-1,1:length-1]
        self.eigen_values = np.delete(self.eigen_values, 94)
        self.eigen_vectors = np.delete(self.eigen_vectors, 94, axis=1)
        self.eigen_vectors = np.delete(self.eigen_vectors, 94, axis=0)
        self.mass_matrix = np.delete(self.mass_matrix, 94, axis=0)
        self.mass_matrix = np.delete(self.mass_matrix, 94, axis=1)
        self.stiffness_matrix = np.delete(self.stiffness_matrix, 94, axis=0)
        self.stiffness_matrix = np.delete(self.stiffness_matrix, 94, axis=1)
        self.block_position = np.delete(self.block_position, 94, axis=0)
        self.block_velocity = np.delete(self.block_velocity, 94, axis=0)
    
    def create_heat_map(self, matrix, figname):
        linspace = np.linspace(self.speeds["start_speed"], self.speeds["end_speed"], num=n_data_points)

        cmap = sns.diverging_palette(230, 20, as_cmap=True)
        matrix = np.flip(matrix, axis=0)
        fig = plt.figure()
        ax = sns.heatmap(matrix,  xticklabels=False, cmap="PuBuGn")
        ax.invert_yaxis()
        plt.xlabel("t")
        plt.ylabel('Mode')
        loader.save_figure(self.run_name, figname, fig)
        plt.show()

    def remove_first_time_steps(self):
        n_remove = int(len(self.block_position[0,:])*0.1)
        print(n_remove)
        self.block_position = self.block_position[:,n_remove:]
        self.block_velocity = self.block_velocity[:,n_remove:]
        print(len(self.pad_position))


    def plot_mode(self, modenumber):
        index_modenumber = len(self.eigen_values) - modenumber - 1
        eigenvector = self.eigen_vectors[:,index_modenumber]
        n_time_steps = len(self.block_position[0,:])
        amplitude_array = np.array([[]]) 
        for i in range(n_time_steps):
            amplitude = eigenvector @ np.array(self.block_position[:,i])
            amplitude_array = np.append(amplitude_array, amplitude)
        sns.lineplot(data = amplitude_array, color = "#74c476")
        sns.despine(trim=True)
        plt.show()

    def trim_matrices_velocity_interval(self):
        new_start_speed = 1.7
        new_end_speed = 1.5
        speeds = loader.get_start_end_speed(self.parameters)
        start_speed = speeds["start_speed"]
        end_speed = speeds["end_speed"]
        n_steps = len(self.pad_position)
        self.pad_position = loader.continuous_trim(self.pad_position, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, False)
        self.pad_velocity = loader.continuous_trim(self.pad_velocity, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, False)
        self.block_velocity = loader.continuous_trim(self.block_velocity, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, True)
        self.block_position = loader.continuous_trim(self.block_position, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, True)
        self.steady_block = loader.continuous_trim(self.steady_block, n_steps+1, start_speed, end_speed, new_start_speed, new_end_speed, True)



def run():
    remove_first_mode = False
    # If set to true, the first mode will be multiplied with a different block-position matrix, making the first mode kind of normalised.
    edit_first_mode = False 

    if(len(sys.argv) == 2):
        plot = HeatMap(sys.argv[1], remove_first_mode, edit_first_mode)
    else:
        Exception("You need to input one argument: the run_name")

if __name__ == "__main__":
    run()