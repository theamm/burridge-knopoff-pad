from myimports_basic import *
from myimports_sp import *
import itertools
sns.set_palette("PuBu_r")
from myimports_sp import *
mpl.rcParams["svg.fonttype"] = "none"
mpl.rcParams["font.size"] = 16
plt.rcParams['axes.labelsize'] = 16
plt.rcParams['axes.titlesize'] = 15
style=itertools.cycle(["--","-.",":",".","h","H"])

# Class used to plot position and velocity of blocks and pad with respect to time.

class PlotResults:
    def __init__(self, run_name = "", numeric_method = "_midpoint_", one_block_boolean=False):
        self.run_name = run_name
        self.one_block_boolean = one_block_boolean
        self.load_files(run_name, numeric_method)
        self.set_up_plot()
        loader.copy_scripts(run_name, "quick_plotting.py")
        self.blocks = [0, 50, 99]

    def load_files(self, run_name, numeric_method):
        parameters = loader.load_yaml_parameters(run_name)
        self.run_parameters = parameters
        self.pad_friction = loader.load_simulation_output(parameters, run_name, "pad_friction")
        self.pad_velocity = loader.load_simulation_output(parameters, run_name, "pad_velocity")
        self.pad_position = loader.load_simulation_output(parameters, run_name, "pad_position")
        self.block_velocity = loader.load_simulation_output(parameters, run_name, "block_velocity")
        self.block_position = loader.load_simulation_output(parameters, run_name, "block_position")
        #self.trim_matrices_velocity_interval()

    def trim_matrices_velocity_interval(self):
        new_start_speed = 0.3
        new_end_speed = 0.2
        speeds = loader.get_start_end_speed(self.run_parameters)
        start_speed = speeds["start_speed"]
        end_speed = speeds["end_speed"]
        n_steps = len(self.pad_position)
        self.pad_friction = loader.continuous_trim(self.pad_friction, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, False) 
        self.pad_position = loader.continuous_trim(self.pad_position, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, False)
        self.pad_velocity = loader.continuous_trim(self.pad_velocity, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, False)
        self.block_velocity = loader.continuous_trim(self.block_velocity, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, False)
        self.block_position = loader.continuous_trim(self.block_position, n_steps, start_speed, end_speed, new_start_speed, new_end_speed, False)


    def set_up_plot(self):
        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]
        if(len(self.block_position.shape) == 1):
            self.one_block_boolean = True
            self.x = np.linspace(start = start, stop = stop, num = len(self.block_position))
        else:
            self.x = np.linspace(start = start, stop = stop, num = len(self.block_position[0]))

    def example_function(self):
        sigma = 1.0
        mu = 0
        return( (1.0/np.sqrt(2*np.pi*np.square(sigma))) * np.exp(-np.square(self.x - mu)/(2*np.square(sigma))) )

    def example_plot(self):
        self.y = self.example_function()
        fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, sharex=True)

        if(not self.one_block_boolean):
            for i in range(0, len(self.block_position)):
                ax1.plot(self.x, self.block_position[i], label = "Block: " + str(i))
                ax2.plot(self.x, self.block_velocity[i], label = "Velocity: " + str(i))
        else:
            ax1.plot(self.x, self.block_position, label = "Block: " + "1")
            ax2.plot(self.x, self.block_velocity, label = "Velocity: " + "1")
        ax3.plot(self.x, self.pad_position)
        ax4.plot(self.x, self.pad_velocity)

        plt.xlabel("$t$")
        loader.save_figure(self.run_name,"all plots", fig)
        plt.show()

    def plot_blocks_position(self):
        figure = plt.figure()
        if(not self.one_block_boolean):
            for i in range(0, len(self.block_position)):
                label = str(i)
                #if(i == 1):
                #    plt.plot(self.x, self.block_position[i])
                plt.plot(self.x,self.block_position[i],  alpha=0.4)
        else:
            plt.plot(self.x, self.block_position)
        plt.ylabel("$u$")
        plt.xlabel("$t$")
        sns.despine()
        loader.save_figure(self.run_name,"block_position", figure)
        plt.show()

    def plot_blocks_velocity(self):
        figure = plt.figure()
        if(not self.one_block_boolean):
                for i in range(0, len(self.block_position)):
                    plt.plot(self.x, self.block_velocity[i], alpha=0.4, label = self.blocks[i])
        else:
            plt.plot(self.x, self.block_velocity, alpha=1, label = "Block: " + "1")
        plt.title("Position plot of blocks", fontdict={'fontname': 'Times New Roman', 'fontsize': 21}, y=1.03)
        plt.ylabel("$\\dot{u}$")
        plt.xlabel("$t$")
        plt.legend(title = "Block:")
        sns.despine()
        loader.save_figure(self.run_name,"block_velocity", figure)
        plt.show()

    def plot_blocks_position_2(self):
        figure = plt.figure()
        plt.plot(self.x, self.block_position, label = "Block: " + "1")
        #plt.title("Position plot of blocks", fontdict={'fontname': 'Times New Roman', 'fontsize': 21}, y=1.03)
        plt.ylabel("$x$")
        plt.xlabel("$t$")
       # plt.legend()
        sns.despine()
        loader.save_figure(self.run_name,"block_position_2", figure)
        plt.show()

    def plot_pad_position(self):
        figure = plt.figure()
        plt.plot(self.x, self.pad_position, label = "Pad position")

        plt.ylabel("$x$")
        plt.xlabel("$t$")
        sns.despine()
        loader.save_figure(self.run_name,"pad_position_time", figure)
        plt.show()
    
    def plot_pad_velocity(self):
        figure = plt.figure()
        print("x shape ", len(self.x))
        print(len(self.pad_velocity))
        plt.plot(self.x, self.pad_velocity, label = "Pad position")
        plt.ylabel("$\\dot{x}$")
        plt.xlabel("$t$")
        sns.despine()
        loader.save_figure(self.run_name,"pad_velocity_time", figure)
        plt.show()

    def plot_blocks_position_vers_velocity(self):
        figure = plt.figure()
        if(not self.one_block_boolean):
            for i in range(0, len(self.block_position)):
                plt.plot(self.block_velocity[i], self.block_position[i], label = "Block: " + str(i), linestyle=next(style))
                # plt.plot(self.block_velocity[i][-14000:], self.block_position[i][-14000:], label = "Block: " + str(i))
        else:
            plt.plot(self.block_velocity, self.block_position, label = "Block: " + "50")
        plt.ylabel("$u$")
        plt.xlabel("$\\dot{u}$")
        sns.despine()
        plt.legend()
        loader.save_figure(self.run_name,"_block_position_velocity", figure)
        plt.show()

    def plot_pad_position_vers_velocity(self):
        figure = plt.figure()
        plt.plot(self.pad_velocity, self.pad_position)
        plt.ylabel("$x$")
        plt.xlabel("$\\dot{x}$")
        sns.despine()
        loader.save_figure(self.run_name,"_pad_position_velocity", figure)
        plt.show()


    def plot_friction_vers_pad_time(self):
        figure = plt.figure()
        print("x shape ", len(self.x))
        plt.plot(self.x, self.pad_friction)   
        plt.ylabel("$F$")
        plt.xlabel("$t$")
        sns.despine()
        loader.save_figure(self.run_name,"friction_vs_time", figure)
        plt.show()

def run():
    # Do you want to plot one or several blocks:
    one_block_boolean = False
    if(len(sys.argv) > 2):
        example = PlotResults(sys.argv[1], one_block_boolean)
    elif(len(sys.argv) == 2):
        example = PlotResults(sys.argv[1], one_block_boolean)
    else:
        example = PlotResults(one_block_boolean)
    #example.example_plot()
    example.plot_blocks_position()
    example.plot_blocks_velocity()
    example.plot_pad_position()
    example.plot_pad_velocity()
    example.plot_blocks_position_vers_velocity()
    example.plot_pad_position_vers_velocity()
    example.plot_friction_vers_pad_time()


if __name__ == "__main__":
    run()