from frictionImports import *

python_path = os.path.dirname(os.path.realpath(__file__))

def getFileWithPaths(path):
    paths_to_amplitude_files = np.loadtxt(python_path + path, delimiter = "\n", dtype = bytes).astype(str)
    print(paths_to_amplitude_files)
    return paths_to_amplitude_files

def loadAmplitudeFiles(paths):
    if("-notMultiple" in sys.argv):
        print(python_path + "/../../../results/" + str(paths))
        amplitude_files = [np.loadtxt(python_path + "/../../../results/" + str(paths), delimiter = ",")]
    else:
        amplitude_files = np.array([np.loadtxt(python_path + "/../../../results/" + path, delimiter = ",") for path in paths])
    print(amplitude_files)

    if('--direction' in sys.argv):
        colors = ['#a6611a','#dfc27d','#f5f5f5','#80cdc1','#018571']
        legends = ['dec: pad', 'dec: no pad', 'inc: pad', 'inc: no pad', 'inc: inf. weight']
    elif('--inf' in sys.argv):
        colors = ['#a6611a','#dfc27d','#80cdc1','#018571']
        legends = ['inc: no pad', 'inc: inf. weight', 'inc: pad', 'dec: pad', 'dec: no pad']
    else:
        colors = ['black', 'green', 'orange', 'pink']
        legends = ['pad', 'no pad', 'no neighbor', 'half mass']

    for i in range(0,len(amplitude_files)):
        print(len(amplitude_files[i]))
        if(len(amplitude_files[i]) > 3):
            plt.plot(amplitude_files[i][0], -amplitude_files[i][3], color = colors[i])
        plt.errorbar(amplitude_files[i][0], amplitude_files[i][1], yerr=amplitude_files[i][2], ecolor='red', color = colors[i], label = legends[i])
    if("-log" in sys.argv):
        plt.xscale('log')
    plt.ylabel("\$A\$")
    plt.xlabel("\$\\nu\$")
    plt.legend()
    plt.show()

def run():
    # path_1 = "/FrictionAmplitudeResultPaths/test_fusing_amplitude_files.csv"
    # path_2 = "/FrictionAmplitudeResultPaths/test_long_amplitude_run.csv"
    #path_3 = "/FrictionAmplitudeResultPaths/test_bk_bkp_amplitude_files_3.csv"
    #path_4 = "/FrictionAmplitudeResultPaths/test_bk_bkp_amplitude_files_2.csv"
    path_5 = "/FrictionAmplitudeResultPaths/test_bk_bkp_amplitude_files_2.csv"
    # paths_1 = getFileWithPaths(path_1)
    # paths_2 = getFileWithPaths(path_2)
    #paths_3 = getFileWithPaths(path_3)
    #paths_4 = getFileWithPaths(path_4)
    paths_5 = getFileWithPaths(path_5)
    # loadAmplitudeFiles(paths_1)
    # loadAmplitudeFiles(paths_2)
    #loadAmplitudeFiles(paths_3)
    #loadAmplitudeFiles(paths_4)
    loadAmplitudeFiles(paths_5)

if __name__ == "__main__":
    run()
