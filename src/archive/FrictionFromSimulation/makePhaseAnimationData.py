from frictionImports import *

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/File')
from loadFromResults import LoadFile, SimplePlots, VectorArrayHelper, MathHelper

def splitSingleIncrementRun(
    loader,
    vector_array_helper,
    file_name,
    last_x_percent
):
    data = loader.loadFile(file_name)
    print("data shape loaded: ", data.shape)
    interval_size = calculateIntervalSize(loader)
    print("interval size: ", interval_size)
    last_x_percent_index = vector_array_helper.lastXPercentIndex(last_x_percent, interval_size)
    #check if it is loaded data is matrix or vector
    if(len(data.shape) > 1):
        splitted_line = vector_array_helper.splitNumpy2DMatrixTo3DMatrix(data, interval_size)
        splitted_line = np.array(splitted_line)
        last_x_values = splitted_line[:,:,last_x_percent_index:]
        print(last_x_values.shape)
    else:
        #to do: code for pad as well
        splitted_line = vector_array_helper.splitNumpyVectorToMatrix(data, interval_size)
        splitted_line = np.array(splitted_line)
        last_x_values = splitted_line[:, last_x_percent_index:]
        print(last_x_values.shape)

    return last_x_values

def calculateVelocities(
    loader
):
    max_time = loader.getParameter("max_time")
    slider_speed = loader.getParameter("slider_speed")
    increment = loader.getParameter("increment")
    interval = loader.getParameter("interval")

    number_of_intervals = int(np.ceil(max_time/interval))

    velocities = [round(slider_speed + increment * i,4) for i in range(0, number_of_intervals)]
    print("Velocities in run: ", velocities)

    return {'velocities': velocities}


def savePhaseData(
    loader,
    position_selection,
    velocity_selection,
    file_name
):
    if(len(position_selection.shape) > 1):
        save_matrix =[]
        for i in range(0, len(position_selection)):
            save_matrix.append(position_selection[i])
            save_matrix.append(velocity_selection[i])
        save_matrix = np.array(save_matrix)
    else:
        save_matrix = np.array([position_selection, velocity_selection])
        print(save_matrix.shape)
    loader.save_csv(file_name, save_matrix)

def makeBlockPhaseData(
    loader,
    vector_array_helper,
    last_x_percent = 5,
    save_folder = "/results/friction_phase/"
):
    position_data = splitSingleIncrementRun(
        loader = loader,
        vector_array_helper=vector_array_helper,
        file_name = "block_position",
        last_x_percent=last_x_percent)
    velocity_data = splitSingleIncrementRun(
        loader = loader,
        vector_array_helper=vector_array_helper,
        file_name = "block_velocity",
        last_x_percent=last_x_percent)
    print(position_data.shape, velocity_data.shape)

    for i, (position, velocity) in enumerate(zip(position_data, velocity_data)):
        if(not loader.check_if_dir_exist_in_run_folder(save_folder)):
            os.makedirs(loader.results_path + loader.run_name + save_folder)

        savePhaseData(
            loader,
            position,
            velocity,
            file_name = save_folder + "block_phase_" + str(i) + ".csv"
            )

    velocities = calculateVelocities(loader)
    loader.saveYaml(velocities, "/results/friction_phase/increment_run.yaml")

def makePadPhaseData(
    loader,
    vector_array_helper,
    last_x_percent = 5,
    save_folder = "/results/friction_phase/"
):
    position_data = splitSingleIncrementRun(
        loader = loader,
        vector_array_helper=vector_array_helper,
        file_name = "pad_position",
        last_x_percent=last_x_percent)
    velocity_data = splitSingleIncrementRun(
        loader = loader,
        vector_array_helper=vector_array_helper,
        file_name = "pad_velocity",
        last_x_percent=last_x_percent)
    print(position_data.shape, velocity_data.shape)

    for i, (position, velocity) in enumerate(zip(position_data, velocity_data)):
        if(not loader.check_if_dir_exist_in_run_folder(save_folder)):
                os.makedirs(loader.results_path + loader.run_name + save_folder)

        savePhaseData(
            loader,
            position,
            velocity,
            file_name = save_folder + "pad_phase_" + str(i) + ".csv"
        )


def calculateIntervalSize(
    loader
):
    interval = loader.getParameter("interval")
    dt = loader.getParameter("dt")
    save_interval = loader.getParameter("save_interval_dt")

    data_points_per_velocity = int(interval/(dt*save_interval))

    return data_points_per_velocity


def main():
    last_x_percent = 5
    if(len(sys.argv) > 1):
        run_name = sys.argv[1]

        if("-p" in sys.argv):
            p_index = sys.argv.index('-p')
            try:
                last_x_percent = int(sys.argv[p_index + 1])
            except IndexError as e:
                print(e)
    else:
        quit()
    loader = LoadFile(run_name=run_name)
    vector_array_helper = VectorArrayHelper()
    makeBlockPhaseData(loader, vector_array_helper, last_x_percent=last_x_percent)
    makePadPhaseData(loader, vector_array_helper, last_x_percent=last_x_percent)


if __name__ == "__main__":
    main()
