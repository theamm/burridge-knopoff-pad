from frictionImports import sys, os, plt, np
from frictionImports import *

python_path = os.path.dirname(os.path.realpath(__file__))

colors_diverging_short = ['#d7191c','#fdae61','#ffffbf','#abd9e9','#2c7bb6']
colors_diverging_long = ['#a50026','#d73027','#f46d43','#fdae61','#fee090','#ffffbf','#e0f3f8','#abd9e9','#74add1','#4575b4','#313695']
colors_qualitative_short = ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e']
colors_qualitative_long = ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#e6ab02','#a6761d']
linestyles = ['-', '--', '-.', ':']


def plotCIAmplitudes():
    general_path = python_path + "/../../../results/CI_amplitude_plots/"
    paths = ["average_bk_increase_16_runs.csv", "average_bkp_increase_16_runs.csv", "average_bk_decrease_16_runs.csv", "average_bkp_decrease_16_runs.csv"] 
    labels = ["BK: increase", "BK-Pad: increase", "BK: decrease", "BK-Pad: decrease"]
    amplitude_files = np.array([np.loadtxt(general_path + path, delimiter = ",") for path in paths])

    colors = colors_qualitative_long

    for i, amplitude_file in enumerate(amplitude_files):
        plt.plot(amplitude_file[0], amplitude_file[1], color=colors[i], label=labels[i], linestyle=linestyles[i])
        plt.fill_between(amplitude_file[0], amplitude_file[1] + amplitude_file[2], amplitude_file[1] - amplitude_file[2], color = colors[i], alpha=.5)
    
    plt.legend(title="Model: run type")

    if("-log" in sys.argv):
        plt.xscale('log')
    plt.show()


def frictionLaw(
    y,
    sigma=0.01,
    F_0=1
):
    phi = F_0 * ((1 - sigma)/(1 + np.abs(y)/(1 - sigma)) * np.sign(y))
    return phi



def plotAveragedAmplitudes():
    general_path = python_path + "/../../../results/week_20/average_model_plots/"
    paths = ["bk_increasing/results/average_16_runs.csv", "bkp_increasing/results/average_16_runs.csv", "bk_decreasing/results/average_16_runs.csv", "bkp_decreasing/results/average_16_runs.csv"] 
    labels = ["BK: inc.", "BK-Pad: inc.", "BK: dec.", "BK-Pad: dec."]
    amplitude_files = np.array([np.loadtxt(general_path + path, delimiter = ",") for path in paths])

    colors = colors_qualitative_long
    ecolors = colors_diverging_long
    start_index = 0

    

    for i, amplitude_file in enumerate(amplitude_files):
        if(len(amplitude_file) > 3):
            # plots average of firction value
            plt.plot(amplitude_file[0], abs(amplitude_file[3]), color=colors[i], linestyle=linestyles[i % 4])

        plt.errorbar(amplitude_file[0], amplitude_file[1], yerr=amplitude_file[2], ecolor=ecolors[-i], label=labels[i], color=colors[i], linestyle=linestyles[i % 4])
    
    phi = frictionLaw(amplitude_files[0][0])
    plt.plot(amplitude_files[0][0], phi*100, label="\$\phi(\\nu)*N\$", color=colors[-1])
    
    plt.legend(title="Model: type")
    plt.ylabel("\$A\$")
    plt.xlabel("\$\\nu\$")
    plt.suptitle("Averaged BK/BKP runs")

    if("-log" in sys.argv):
        plt.xscale('log')
    plt.show()


def run():
    if("--average" in sys.argv):
        plotAveragedAmplitudes()
    else:
        plotCIAmplitudes()


if __name__ == "__main__":
    run()
