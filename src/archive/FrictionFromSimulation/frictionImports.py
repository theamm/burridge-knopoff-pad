import sys
import os

from matplotlib import pyplot as plt
import numpy as np

plt.rcParams["svg.fonttype"] = "none"
plt.rcParams["font.size"] = 16
plt.rcParams["savefig.directory"] = "../../Figures/Master"
plt.rcParams["savefig.format"] = "svg"
