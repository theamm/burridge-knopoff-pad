import os
import sys

from matplotlib import pyplot as plt

import numpy as np
import numpy.fft as fft

import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class LoadFile:
    def __init__(
            self,
            run_name="",
            numeric_method="_midpoint_",
    ):
        self.run_name = run_name
        self.numeric_method = numeric_method
        self.setResultsPath()
        self.loadYamlParameters(run_name)

    def setResultsPath(
            self
    ):
        self.results_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../../results/"

    def loadFile(
        self,
        file_description
    ):
        results_folder = "/"

        shared_names = self.getParameter("file_name")

        return np.loadtxt(self.results_path + self.run_name + results_folder + shared_names + self.numeric_method + file_description + ".csv", delimiter=",")

    def loadLastHalfParallelRun(
            self,
            file_description,
            file_names
        ):
        vectorArrayHelper = VectorArrayHelper()

        file_matrix = self.loadFile(file_description + file_names[0])
        half_point_index = vectorArrayHelper.startSecondHalfIndex(file_matrix)
        file_matrix = file_matrix[half_point_index:]

        for i in range(1, len(file_names)):
            loaded_file = self.loadFile(file_description + file_names[i])
            half_point_index = vectorArrayHelper.startSecondHalfIndex(loaded_file)
            loaded_file = loaded_file[half_point_index:]
            file_matrix = np.vstack((file_matrix, loaded_file))

        print(len(file_matrix))

        return(file_matrix)

    def saveArray(
        self,
        array_matrix,
        file_description
    ):
        results_folder = "/"

        shared_names = self.getParameter("file_name")

        np.savetxt(self.results_path + self.run_name + results_folder + shared_names + self.numeric_method + file_description + ".csv", array_matrix, delimiter=",")

    def save_csv(
        self,
        file_name,
        array_or_matrix
    ):
        file_path = self.results_path + self.run_name + file_name
        print("Saving to path: ", file_path)
        np.savetxt(file_path, array_or_matrix, delimiter=',')

    def loadYamlParameters(
            self,
            run_name
    ):
        stream = open(self.results_path + run_name + "/parameters.yaml", 'r')
        self.run_parameters = yaml.load(stream, Loader = Loader)


    def loadYaml(
        self,
        run_name,
        file_name
    ):
        stream = open(self.results_path + run_name + file_name, 'r')
        return yaml.load(stream, Loader = Loader)

    def loadYamlStandAlone(
        self,
        run_name,
        file_name
    ):
        stream = open(run_name + file_name, 'r')
        return yaml.load(stream, Loader = Loader)

    def save_yaml_parameters(
        self,
        run_name
    ):
        print(yaml.dump(self.run_parameters, default_flow_style = False))
        stream = open(self.results_path + self.run_name + "/doc/yaml/parameters.yaml", "w")
        yaml.dump(self.run_parameters, stream, default_flow_style = False)

    def saveYaml(
        self,
        yaml_element,
        file_name
    ):
        print(yaml.dump(yaml_element, default_flow_style = False))
        stream = open(self.results_path + self.run_name + file_name, "w")
        yaml.dump(yaml_element, stream, default_flow_style = False)


    def getParameter(
            self,
            parameter_name
    ):
        return self.run_parameters["Parameters"][parameter_name]

    def getDebugParameter(
            self,
            debug_parameter_name
    ):
        return self.run_parameters["Debug"][debug_parameter_name]

    def check_if_file_exist_in_run_folder(
        self,
        run_name,
        file_name = ""
    ):
        if(os.path.isfile(self.results_path + run_name + file_name)):
            return True
        else:
            return False

    def check_if_dir_exist_in_run_folder(
        self,
        dir_name
    ):
        if(os.path.isdir(self.results_path + self.run_name + dir_name)):
            return True
        else:
            return False


class SimplePlots:

    def __init__(
        self,
        loader
    ):
        self.loader = loader

    def timeSimpleLinspaceFromParameters(
        self,
        singleVector,
        run_parameters
    ):
        start = 0
        stop = self.loader.getParameter("max_time")
        return np.linspace(start = start, stop = stop, num = len(singleVector))

    def sliderSpeedSimpleLinspaceFromParameters(
        self,
        intervals,
        run_parameters
    ):
        start =  self.loader.getParameter("slider_speed")
        interval_increment = self.loader.getParameter("increment")
        stop = start + interval_increment * (intervals - 1)  # not +1 now
        print("slider intervals", start, stop, intervals)
        return np.linspace(start = start, stop = stop, num = intervals)

    ## TODO: Finish when needed, needs to return two elements
    def intervalLinspaceFromParameters(
        self,
        singleVector,
        run_parameters,
        plot_start_interval = None,
        plot_end_interval = None
    ):
        if((plot_start_interval or plot_end_interval) is None):
            start = 0
            max_time = run_parameters["Parameters"]["max_time"]
        else:
            start = plot_start_interval
            max_time = plot_end_interval

        start_block_index = start/run_parameters["Parameters"]["dt"]
        max_time_block_index = max_time/run_parameters["Parameters"]["dt"] #(-1 removed)
        amount_data_points = max_time_block_index - start_block_index

        x = np.linspace(start = start, stop = max_time, num = amount_data_points)
        block_indexes = np.linspace(start = start_block_index, stop = max_time_block_index, num = max_time_block_index - start_block_index, dtype=int)

        return {'x_values': x, 'block_indexes': block_indexes}

    def intervalLinspaceParallelHalfRun(
        self,
        single_vector,
        run_parameters
    ):
         amount_data_points = len(single_vector)
         end_value = run_parameters["Parameters"]["interval"]
         start_value = end_value / 2

         print(end_value, start_value)

         x_values = np.linspace(start = start_value, stop = end_value, num = amount_data_points)

         return x_values

    def plotOneWithOneAxis(
        self,
        vector_values,
        axis = "y"
    ):
        if(axis == "y"):
            plt.plot(vector_values, label = "plotOneWithOneAxis")
        elif(axis == "x"):
            plt.plot(vector_values, np.linspace(0, len(vector_values), len(vector_values)), label = "plotOneWithOneAxis")
        else:
            raise Exception("invalid axis name given, please select \"x\" or \"y\" or leave empty.")

    def plotOneWithTwoAxes(
        self,
        x_values,
        y_values,
        label = "plotOneWithTwoAxes"
    ):
        plt.plot(x_values, y_values, label = label)

    def plot_fft_frequencies(
        self,
        fft_results
    ):
        self.plotOneWithTwoAxes(fft_results['Frequency'], fft_results['Spectrum'])
        plt.show()

class VectorArrayHelper:
    def splitNumpyVectorToMatrix(
            self,
            vector,
            interval_size
        ):
        print(len(vector), interval_size)
        max_number_of_intervals = np.floor(len(vector)/interval_size)
        if(max_number_of_intervals <= 0):
            raise Exception("the interval is to big or negative: " + str(interval_size))

        return np.split(vector, max_number_of_intervals)

    def splitNumpy2DMatrixTo3DMatrix(
            self,
            matrix,
            interval_size
         ):
        #np.spit returns list of subarrays
        max_number_of_intervals = np.floor(matrix.shape[1]/interval_size)

        if(max_number_of_intervals <= 0):
            raise Exception("the interval is to big or negative: " + str(interval_size))

        return np.split(matrix, max_number_of_intervals, axis=1)

    def startSecondHalfIndex(
        self,
        vector
        ):
        return(int(np.ceil(len(vector)/2.0)))

    def lastXPercentIndex(
        self,
        last_x_percent,
        vector_length
    ):
        #zero-indexation in python, keep in mind
        index_amount = int(vector_length*(last_x_percent/100))
        last_x_percent_index = (vector_length) - index_amount

        return last_x_percent_index

class MathHelper:
    def __init__(
        self,
        loader
    ):
        self.simplePlots = SimplePlots(loader)

    def calculate_fft(
        self,
        data
    ):
        half_point = int(0.5*len(data))
        data = data[half_point:]
        spectrum = fft.fft(data)

        frequency = fft.fftfreq(len(spectrum))

        threshold = 0.5 * max(abs(spectrum))
        mask = abs(spectrum) > threshold
        peaks = frequency[mask]
        print(peaks)
        return_values = {'Spectrum' : spectrum, 'Frequency' : frequency}
        self.simplePlots.plot_fft_frequencies(return_values)

        return return_values
