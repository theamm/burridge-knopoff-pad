from frictionImports import *
from glob import glob

if("-confidence" in sys.argv):
    print("import confidence code")
    try:
        import scipy.stats
    except ImportError as e:
        print("Error when trying to import scipy.stats: ", e)

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/File')
from loadFromResults import LoadFile
python_path = os.path.dirname(os.path.realpath(__file__))

colors_diverging_short = ['#d7191c','#fdae61','#ffffbf','#abd9e9','#2c7bb6']
colors_diverging_long = ['#a50026','#d73027','#f46d43','#fdae61','#fee090','#ffffbf','#e0f3f8','#abd9e9','#74add1','#4575b4','#313695']
colors_qualitative_short = ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e']
colors_qualitative_long = ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#e6ab02','#a6761d']
linestyles = ['-', '--', '-.', ':']


def getFileWithPaths(path):
    yaml_file = ""
    if('yaml' in path):
        yaml_file = LoadFile.loadYamlStandAlone("", python_path, path)
        path_components = [unique_path_component for unique_path_component in (yaml_file["unique_path_components"])]
        print(path_components)
        paths_to_amplitude_files = [yaml_file["general_path"] + str(unique_path_component) + "/" + yaml_file["result_path"] + yaml_file["file_name"] for unique_path_component in (yaml_file["unique_path_components"])]
    else:
        print("path", python_path + path)
        paths_to_amplitude_files = np.loadtxt(python_path + path, delimiter = "\n", dtype = bytes).astype(str)
        print(paths_to_amplitude_files)
    return (paths_to_amplitude_files, yaml_file)


def getYamlElement(
    yaml_file,
    yaml_key
):
    if(isinstance(yaml_file, dict)):
        if(yaml_key in yaml_file.keys()):
            return yaml_file[yaml_key]
        else:
            print("Tried to add title from yaml file, but there was no \"" + yaml_key + "\" key.")
            return None


def addTitleFromYaml(
    yaml_file,
    yaml_key="suptitle"
):
    suptitle = getYamlElement(yaml_file, yaml_key)
    if(suptitle is not None):
        plt.suptitle(suptitle)


def getLabelsFromYaml(
    yaml_file,
    yaml_key="unique_path_components"
):
    labels = getYamlElement(yaml_file, yaml_key)
    if(labels is not None):
        return labels

def getLabelTitleFromYaml(
    yaml_file,
    yaml_key="label_title"
):
    label_title = getYamlElement(yaml_file, yaml_key)
    if(label_title is not None):
        return label_title
    else:
        return ""


def plotIndividualAmplitudes(
    amplitude_files,
    yaml_file=""
):
    if("-compare" in sys.argv):
        colors = colors_qualitative_short
        labels = ['BK: seed 113.', 'BK: seed 114']
    else:
        colors = colors_qualitative_long*len(amplitude_files)
        labels = getLabelsFromYaml(yaml_file)
    for i in range(0,len(amplitude_files)):
        if(len(amplitude_files[i]) > 3):
            plt.plot(amplitude_files[i][0], -amplitude_files[i][3], color = colors[i])
        plt.errorbar(amplitude_files[i][0], amplitude_files[i][1], yerr=amplitude_files[i][2], ecolor='red', color=colors[i], label = labels[i], linestyle=linestyles[i%4])
    if("-log" in sys.argv):
        plt.xscale('log')
    plt.ylabel("\$A\$")
    plt.xlabel("\$\\nu\$")
    if("-dipregion" in sys.argv):
        plt.xlim(0.07,0.27)
        plt.ylim(0,30)
        plt.grid()
    else:
        plt.legend(title = getLabelTitleFromYaml(yaml_file))
    addTitleFromYaml(yaml_file)
    plt.show()


def plotAverageAmplitudes(
    amplitude_files,
    path_to_save_folder,
    yaml_file=""
):
    number_of_runs = str(amplitude_files.shape[0])
    label = "avg of " + number_of_runs + " runs"
    color = '#2c7bb6'
    ecolor = '#d7191c'

    amplitude_files = np.average(amplitude_files, 0)

    if(len(amplitude_files) > 3):
        # plots average of firction value
        plt.plot(amplitude_files[0], -amplitude_files[3], color = color)

    plt.errorbar(amplitude_files[0], amplitude_files[1], yerr=amplitude_files[2], ecolor=ecolor, label = label, color = color)

    if("-log" in sys.argv):
        plt.xscale('log')

    plt.ylabel("\$A\$")
    plt.xlabel("\$\\nu\$")
    plt.legend()
    addTitleFromYaml(yaml_file)
    plt.show()

    if(not "-notsave" in sys.argv):
        input("Now you are saving, press enter to continue.")
        np.savetxt(python_path + "/../../../results" + path_to_save_folder + "average_" + number_of_runs + "_runs.csv", amplitude_files, delimiter = ',')


def calculateIntervalError(
    amplitude_files,
    yaml_file
):
    print("Calculating the difference between the first and consecutive amplitude files.")
    print(amplitude_files.shape)
    if(len(amplitude_files.shape) == 3):
        colors = colors_qualitative_short*len(amplitude_files)
        amplitudes = amplitude_files[:,1,:]
        if("-averagecompare" in sys.argv):
            compared_to_file = np.average(amplitudes, axis = 0)
            print(compared_to_file)
            start_iteration = 0
        else:
            compared_to_file = amplitudes[0]
            start_iteration = 1
        compared_list = []
        for i in range(start_iteration, len(amplitudes)):
            difference =  abs(amplitudes[i] - compared_to_file)
            compared_list.append(difference)

        compared_list = np.array(compared_list)
        labels = getLabelsFromYaml(yaml_file)[start_iteration:]
        if("-saveinterval" in sys.argv):
            scatterbar_x_values = [1/label for label in labels]
            labels = ["1/" + str(label) for label in labels]
            scatterbar_x_label = "\$Save frequency\$"
        else:
            scatterbar_x_values = labels
            scatterbar_x_label = "\$Seed\$"
        label_title = getLabelTitleFromYaml(yaml_file)
        [plt.plot(amplitude_files[i,0,:],compared, label = labels[i], color = colors[i], linestyle=linestyles[i%4]) for i, compared in enumerate(compared_list)]
        plt.legend(title = label_title)
        if("-log" in sys.argv):
            plt.xscale('log')
        plt.ylabel("\$Error\$")
        plt.xlabel("\$\\nu\$")
        addTitleFromYaml(yaml_file)
        plt.show()
        [plt.plot(amplitude_files[i,0,:],np.cumsum(compared), label = labels[i], color = colors[i], linestyle=linestyles[i%4]) for i, compared in enumerate(compared_list)]
        plt.ylabel("\$ Cumulative Error\$")
        plt.xlabel("\$\\nu\$")
        plt.legend(title = label_title)
        if("-log" in sys.argv):
            plt.xscale('log')
        addTitleFromYaml(yaml_file)
        #plt.ylim(0, 0.35)
        plt.show()

        compared_sum = np.sum(compared_list, 1)

        #compared_sum
        plt.scatter(scatterbar_x_values, compared_sum, color = colors_diverging_short[0])
        plt.bar(scatterbar_x_values, compared_sum, color = colors_diverging_short[0])
        plt.plot(scatterbar_x_values, compared_sum, color = colors_diverging_short[1], linestyle = linestyles[1])
        plt.ylabel("\$ Cumulative Error\$")
        plt.xlabel(scatterbar_x_label)
        #plt.ylim(0, 0.35)
        addTitleFromYaml(yaml_file)
        plt.show()

        plt.bar(np.arange(len(compared_sum)),np.sort(compared_sum))
        plt.show()

        plt.plot(amplitude_files[0,0,:],compared_to_file, color = colors_diverging_short[4], label = "1/1")
        plt.ylabel("\$A\$")
        plt.xlabel("\$\\nu\$")
        plt.legend(title = label_title)
        addTitleFromYaml(yaml_file)
        if("-log" in sys.argv):
            plt.xscale('log')
        plt.show()

def mean_confidence_interval(
    amplitude_files,
    yaml_file,
    confidence=0.95
):
    label = "BK-pad: 95\% CI, 16 runs"
    color = colors_qualitative_short[3]
    amplitudes = amplitude_files[:,1,:]
    mean, standard_error = np.mean(amplitudes, axis=0), scipy.stats.sem(amplitudes)
    print(amplitudes.shape)
    height = standard_error * scipy.stats.t.ppf((1 + confidence) / 2., len(amplitudes)-1)
    plt.plot(amplitude_files[0,0,:],mean, color = color, label = label)
    plt.fill_between(amplitude_files[0,0,:], mean+height, mean-height, color = color, alpha=.5)
    if("-log" in sys.argv):
        plt.xscale('log')
    plt.ylabel("\$A\$")
    plt.xlabel("\$\\nu\$")
    plt.ylim(0, 45)
    plt.legend()
    addTitleFromYaml(yaml_file)
    plt.show()
    # matrix_CI = np.array([amplitude_files[0,0,:], mean, height, standard_error])
    # print(matrix_CI.shape)
    # np.savetxt(python_path + "/../../../results/" + yaml_file["save_CI_path"] + yaml_file["save_CI_name"], matrix_CI, delimiter = ',')


def plotAmplitudeFiles(
    amplitude_files,
    path_to_save_folder,
    yaml_file
):
    if("-individual" in sys.argv):
        plotIndividualAmplitudes(amplitude_files, yaml_file)

    if(not "-compare" in sys.argv):
        plotAverageAmplitudes(amplitude_files, path_to_save_folder, yaml_file)

    if("-saveinterval" or "-averagecompare" in sys.argv):
        calculateIntervalError(amplitude_files, yaml_file)
    if("-confidence" in sys.argv):
        mean_confidence_interval(amplitude_files, yaml_file)



def loadAmplitudeFiles(
    paths,
    path_to_save_folder,
    yaml_file
):
    amplitude_files = np.array([np.loadtxt(paths+ path, delimiter = ",") for path in paths])
    print(amplitude_files)
    #amplitude_files = np.array([np.loadtxt(path, delimiter = ",") for path in paths])
    #amplitude_files = np.array([np.loadtxt(python_path + "/../../../results/" + path, delimiter = ",") for path in paths])
    #plotAmplitudeFiles(amplitude_files, path_to_save_folder, yaml_file)


def run(path_to_paths, path_to_save_folder):
    paths, yaml_file = getFileWithPaths(path_to_paths)
    loadAmplitudeFiles(paths, path_to_save_folder, yaml_file)

if __name__ =="__main__":
    loadAmplitudeFiles("/Users/TheaMartine/Desktop/repo_master/thea/bk/results/test_friction_amplitude/results/friction_phase/","","")
    #run(sys.argv[1], sys.argv[2])

# argv[1]: path_to_paths on the form:
# argv[2]: path_to_save_folder on the form: 