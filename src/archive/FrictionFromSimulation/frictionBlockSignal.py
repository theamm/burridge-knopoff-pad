from frictionImports import *
import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class FrictionBlockSignal:
    def __init__(
                 self,
                 run_name = "",
                 numeric_method = "_midpoint_",
                 plot_start_interval = None,
                 plot_end_interval = None
                 ):

        self.loadFiles(run_name, numeric_method)
        self.setUpPlot(plot_start_interval, plot_end_interval)
        self.calculateAmplitude()

    def loadFiles(
                  self,
                  run_name,
                  numeric_method
                  ):
        self.results_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../results/"
        results_folder = "/"

        stream = open(self.results_path + run_name + "/parameters.yaml", 'r')
        self.run_parameters = yaml.load(stream, Loader = Loader)

        shared_names = self.run_parameters["Parameters"]["file_name"]

        self.block_velocity = np.loadtxt(self.results_path + run_name + results_folder + shared_names + numeric_method + "block_velocity.csv", delimiter=",")

    def calculateAmplitude(
                           self
                           ):
        half_point_velocity_index = int(self.run_parameters["Parameters"]["max_time"]/(2*self.run_parameters["Parameters"]["dt"]) -1)
        if(self.one_block_boolean):
            half_of_velocity = self.block_velocity[half_point_velocity_index:]
        else:
            half_of_velocity = self.block_velocity[0][half_point_velocity_index:]
        half_of_velocity_average = np.average(half_of_velocity)

        amplitude = np.sqrt(2)*np.sqrt(np.average(np.power(half_of_velocity - half_of_velocity_average,2)))
        print(amplitude* 25)

        plt.plot(np.abs(half_of_velocity - half_of_velocity_average))
        plt.show()
        print(half_point_velocity_index)
        print(half_of_velocity)

    def setUpPlot(
                  self,
                  plot_start_interval,
                  plot_end_interval
                  ):
        self.one_block_boolean = False

        if((plot_start_interval or plot_end_interval) is None):
            start = 0
            max_time = self.run_parameters["Parameters"]["max_time"]
        else:
            start = plot_start_interval
            max_time = plot_end_interval

        start_block_index = start/self.run_parameters["Parameters"]["dt"]
        max_time_block_index = max_time/self.run_parameters["Parameters"]["dt"] - 1
        amount_data_points = max_time_block_index - start_block_index

        if(len(self.block_velocity.shape) == 1):
            self.one_block_boolean = True
            self.x = np.linspace(start = start, stop = max_time, num = amount_data_points)
            self.block_indexes = np.linspace(start = start_block_index, stop = max_time_block_index, num = max_time_block_index - start_block_index, dtype=int)

            self.step_size = amount_data_points
        else:
            self.x = np.linspace(start = start, stop = max_time, num = amount_data_points)
            self.block_indexes = np.linspace(start = start_block_index, stop = max_time_block_index, num = max_time_block_index - start_block_index, dtype = int)
            self.step_size = amount_data_points

        print(self.block_indexes, len(self.block_indexes))

    def plotVelocity(
                   self
                   ):
       if(not self.one_block_boolean):
           for i in range(0, len(self.block_velocity)):
               plt.plot(self.x, self.block_velocity[i][self.block_indexes], label = "Block: " + str(i))
       else:
           plt.plot(self.x, self.block_velocity[self.block_indexes], label = "Block: " + "1")

       #plt.title("Position plot of blocks", fontdict={'fontname': 'Times New Roman', 'fontsize': 21}, y=1.03)
       plt.ylabel("$y$")
       plt.xlabel("$t$")
       plt.legend()
       plt.show()

def run():
    if(len(sys.argv) == 4):
        frictionBlockSignal = FrictionBlockSignal(sys.argv[1], "_midpoint_", int(sys.argv[2]), int(sys.argv[3]))
    elif(len(sys.argv) == 2):
        frictionBlockSignal = FrictionBlockSignal(sys.argv[1])
    else:
        raise Exception("Need to have either no interval values or two, start and end.")
    frictionBlockSignal.plotVelocity()


if __name__ == "__main__":
    run()