from frictionImports import *
from makePhaseAnimationData import calculateIntervalSize

import re

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/File')
from loadFromResults import LoadFile, SimplePlots, VectorArrayHelper, MathHelper

class SplitMultipleSpeedSegmentsFriction:
    def __init__(
    self,
    run_name = "",
    numeric_method = "_midpoint_"
    ):
        self.loader = LoadFile(run_name)
        self.simplePlots = SimplePlots(self.loader)
        self.VectorArrayHelper = VectorArrayHelper()
        self.mathHelper = MathHelper(self.loader)

       # self.singleIncrementRun()
        self.parallelIncrementRun()



    def singleIncrementRun(
    self
    ):
        self.pad_velocity = self.loader.loadFile("pad_friction")
        self.x = self.simplePlots.timeSimpleLinspaceFromParameters(self.pad_velocity, self.loader.run_parameters)

        self.amplitudeForSplittedSingleRun()
        self.plotPadProperty("pad_friction")

    def parallelIncrementRun(
    self
    ):
        file_name_speeds = self.generateFileNamesFromParameters()
        files = self.loader.loadLastHalfParallelRun("pad_friction", file_name_speeds)

        # TO DO: brake power * velocity summed over time.
        if("-brakeenergy" in sys.argv):
            self.brakeEnergy(files, file_name_speeds)

        x_values = self.simplePlots.intervalLinspaceParallelHalfRun(files[0], self.loader.run_parameters)

        amplitudes_and_errors = np.array([self.calculateAmplitude(file) for file in files])
        amplitudes = amplitudes_and_errors[:,0]
        errors = amplitudes_and_errors[:,1]
        averages = amplitudes_and_errors[:,2]
        max_files_plotted = 2
        print("Files longer than max files to be plotted: ", len(files) > max_files_plotted)

        if(len(files) > max_files_plotted):
            for i in range(0,max_files_plotted):
                plt.plot(x_values, files[i], label = file_name_speeds[i])
                #self.mathHelper.calculate_fft(files[i])
                plt.legend()
                if(not "-noplot" in sys.argv):
                    plt.show()

        else:
            for file in files:
                plt.plot(file, x_values)
                self.mathHelper.calculate_fft(file)
        self.plotAmplitude(amplitudes, errors, averages)

    def brakeEnergy(
        self,
        friction_forces,
        file_name_speeds
    ):
        file_name_speeds = self.simplePlots.sliderSpeedSimpleLinspaceFromParameters(len(friction_forces), self.loader.run_parameters)
        pad_velocity = self.loader.loadFile("pad_velocity")
        interval_size = calculateIntervalSize(self.loader)
        last_half_index = self.VectorArrayHelper.lastXPercentIndex(50, interval_size)
        pad_velocity_intervals = self.VectorArrayHelper.splitNumpyVectorToMatrix(pad_velocity, interval_size)
        pad_velocity_intervals = np.array(pad_velocity_intervals)
        pad_velocity_intervals_last_half = pad_velocity_intervals[:, last_half_index:]

        brake_energy = [np.sum(abs(friction_force*speed)) for friction_force, speed in zip(friction_forces, file_name_speeds)] # pad_velocity_intervals_last_half)]
        friction_combined = np.concatenate(friction_forces)
        pad_velocity_intervals_last_half_combined = np.concatenate(pad_velocity_intervals_last_half)

        x_values_continous = np.linspace(start=file_name_speeds[0], stop=file_name_speeds[-1], num=len(friction_combined))

        plt.figure()
        plt.subplot(212)
        plt.ylim(-1000, 300000)
        plt.plot(file_name_speeds, brake_energy, marker='x')
        plt.subplot(221)
        plt.ylim(-200,150)
        plt.plot(x_values_continous, friction_combined)
        plt.subplot(222)
        plt.ylim(-0.1,1.51)
        #plt.plot(pad_velocity_intervals_last_half_combined)
        plt.plot(file_name_speeds, file_name_speeds)
        plt.show()
        quit()

    def generateFileNamesFromParameters(
    self,
    general_name = "pad_friction"
    ):
        ## TODO: use yaml file
        interval_size = self.loader.getParameter("interval")
        slider_speed = self.loader.getParameter("slider_speed")
        slider_increment = self.loader.getParameter("increment")
        max_time = self.loader.getParameter("max_time")

        intervals = int(max_time / interval_size)

        file_name_speeds = []

        for i in range(0, intervals):
            current_slider_speed = np.round(slider_speed + slider_increment * i,4)
            print(current_slider_speed)
            file_name_speeds.append(self.addNecessaryZeroes(str(current_slider_speed)))

        return file_name_speeds


    def addNecessaryZeroes(
    self,
    decimal_number
    ):
        number_of_decimals = self.numberOfDecimals(decimal_number)
        if(number_of_decimals == 0):
            number_of_decimals = 1
        decimal_with_zeroes = decimal_number + (6-number_of_decimals)*"0"
        return decimal_with_zeroes

    def numberOfDecimals(
    self,
    decimal_number
    ):
        match = re.match(r"^[0-9]*\.(([0-9]*[1-9])?)0*$", decimal_number)
        return len(match.group(1)) if match is not None else 0



    def amplitudeForSplittedSingleRun(
    self
    ):
        interval_size = int(self.loader.run_parameters["Parameters"]["interval"]/self.loader.run_parameters["Parameters"]["dt"])
        splitted_values = self.VectorArrayHelper.splitNumpyVectorToMatrix(self.pad_velocity, interval_size)

        half_index = self.VectorArrayHelper.startSecondHalfIndex(splitted_values[0])

        a = []

        for i in range(0, len(splitted_values)):
            half_of_values = splitted_values[i][half_index:]
            amplitude_and_error = self.calculateAmplitude(half_of_values)
            amplitude = amplitude_and_error[0]
            errors = amplitude_and_error[1]
            a.append(amplitude)

        self.plotAmplitude(a, errors)

    def plotPadProperty(
    self,
    pad_property
    ):
        pad_property = self.loader.loadFile(pad_property)

        plot_values_dictionary = self.simplePlots.intervalLinspaceFromParameters(pad_property, self.loader.run_parameters)

        print(plot_values_dictionary['x_values'])

        self.simplePlots.plotOneWithTwoAxes(plot_values_dictionary['x_values'], pad_property)
        plt.show()

    def calculateAmplitude(
    self,
    interval
    ):
        interval_average = np.average(interval)
        amplitude = np.sqrt(2)*np.sqrt(np.average(np.power(interval - interval_average,2)))
        error = self.calculateError(interval, interval_average, 4)
        return np.array([amplitude, error, interval_average])

    def plotAmplitude(
    self,
    amplitude,
    errors,
    averages
    ):
        x_values = self.simplePlots.sliderSpeedSimpleLinspaceFromParameters(len(amplitude), self.loader.run_parameters)
        self.simplePlots.plotOneWithTwoAxes(x_values, amplitude)
        self.simplePlots.plotOneWithTwoAxes(x_values, averages)
        plt.errorbar(x_values, amplitude, yerr=errors)
        plt.xscale('log')
        if(not "-noplot" in sys.argv):
            plt.show()
        self.loader.saveArray(np.array([x_values, amplitude, errors, averages]), "amplitude_2")

    def calculateError(
    self,
    interval,
    interval_average,
    splits
    ):
        splitted_interval = np.split(interval, splits)
        split_averages = np.average(splitted_interval, axis = 1)

        sigma = np.sqrt(np.average(np.power(split_averages-interval_average, 2)))
        error = sigma/np.sqrt(splits)
        print(sigma, interval_average, error)
        return error




def run():
    splitMultipleSpeedSegmentsFriction = SplitMultipleSpeedSegmentsFriction(sys.argv[1])


if __name__ == "__main__":
    run()
