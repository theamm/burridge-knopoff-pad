from run_animation_layout_support_functions import *


# pos = position, vel = velocity, pha = phase, bar = bar plot

def pos_vel_pha_bar(
    run_name,
    shown_indexes,
    shift_amount_per_frame = 1,
    frames = 500,
    live_plotting = True
):
    if("--friction" in sys.argv):
        phase_data = load_friction_phase_data(run_name, "0")
    else:
        phase_data = load_phase_data(run_name)
    position_data, velocity_data = seperate_pos_vel_data(phase_data)

    position_plot = make_single_line_line_plot(position_data, shown_indexes, ax_number = 0)
    velocity_plot = make_single_line_line_plot(velocity_data, shown_indexes, ax_number = 1)
    phase_plot = make_single_line_phase_plot(position_data, velocity_data, shown_indexes, ax_number = 2)
    bar_plot = make_single_line_bar_plot(position_plot, ax_number = 3, line_number = 0)
    spring_plot = make_single_line_spring_plot(position_plot, ax_number = 1, line_number = 0)
    animation_plots = [position_plot, velocity_plot, phase_plot, bar_plot]

    bk_animation = BKAnimation(animation_plots, sleep_amount=0.001, max_lim_change=0.3)
    bk_animation.init_live_plot((221, 222, 223, 224), (20,10), ["Position", "Velocity", "Phase", "Bar"])
    bk_animation.update_subplots(update_lims=False)


    plot_shape = [len(animation_plots), len(position_plot.single_line_list)]
    def plot_frames(n = 0):

        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 0, n_line_number = plot_shape[1], index_type = 'y')
        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 2, n_line_number = plot_shape[1], index_type = 'xy')
        #bk_animation.change_x(1, 0, get_axis="y")
        bk_animation.change_y(3, 0)
        if(n % 10 == 0):
            bk_animation.check_lims(0, "y")
            bk_animation.check_lims(1, "y")
            bk_animation.check_lims(2, "x")
            bk_animation.check_lims(2, "y")
        else:
            bk_animation.update_subplots()
        sleep(bk_animation.sleep_amount)


    if(live_plotting):
        for i in range(0, frames):
            plot_frames(i)
    else:
        ani = animation.FuncAnimation(bk_animation.fig, plot_frames,500,interval=30)
        writer = animation.writers['ffmpeg'](fps=20)

        simulation_path = loader.get_simulation_path(run_name)
        ani.save(simulation_path + '/animation.mp4',writer=writer,dpi=100)

def pos_pha_spr(
    run_name,
    shown_indexes,
    shift_amount_per_frame = 1,
    frames = 500,
    live_plotting = True,
    sample_mode = False,
    sample_range = [0,2*100]
):
    if(sample_mode):
        if(not ppc.check_if_all_phase_data_exists(run_name)):
            print(bcolors.WARNING + "Making phase date since it did not exist." + bcolors.ENDC)
            ppc.create_phase_data(run_name)
        phase_data_block = load_phase_data(run_name, "_block")[sample_range[0]:sample_range[1]]
        position_data, velocity_data = seperate_pos_vel_data(phase_data_block)
    else:
        phase_data_block = load_phase_data(run_name)
        position_data, velocity_data = seperate_pos_vel_data(phase_data_block)
    phase_data_pad = load_phase_data(run_name, "_pad")
    print(phase_data_pad)
    position_data_pad = phase_data_pad[0]



    position_plot = make_single_line_line_plot(position_data, shown_indexes, ax_number = 0)
    bar_plot = make_single_line_bar_plot(position_plot, ax_number = 1, line_number = 0)
    phase_plot = make_single_line_phase_plot(position_data, velocity_data, shown_indexes, ax_number = 2)
    spring_plot = make_single_line_spring_plot(position_plot, ax_number = 3, line_number = 0, pad_position = position_data_pad, common_indexes=shown_indexes)
    animation_plots = [position_plot, bar_plot, phase_plot, spring_plot]

    bk_animation = BKAnimation(animation_plots, sleep_amount=0.001, max_lim_change=0.3)
    bk_animation.init_live_plot((231, 232, 233, 212), (20,10), ["Position", "Bar", "Phase", "Spring"])
    bk_animation.update_subplots(update_lims=False)


    plot_shape = [len(animation_plots), len(position_plot.single_line_list)]
    def plot_frames(n = 0):

        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 0, n_line_number = plot_shape[1], index_type = 'y')
        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 2, n_line_number = plot_shape[1], index_type = 'xy')
        bk_animation.shift_x_indexes(ax_number = 3, line_number = 2, shift_amount = shift_amount_per_frame)
        bk_animation.change_y(1, 0)
        bk_animation.change_x(3, 0, get_axis="y")
        bk_animation.change_x(3, 1)
        bk_animation.update_upper_springs(3, index_first_spring = 3)
        if(n % 10 == 0):
            print("hei")
            # bk_animation.check_lims(0, "y")
            # bk_animation.check_lims(1, "x")
            # bk_animation.check_lims(1, "y")
        bk_animation.update_subplots()
        sleep(bk_animation.sleep_amount)


    if(live_plotting):
        for i in range(0, frames):
            plot_frames(i)
    else:
        ani = animation.FuncAnimation(bk_animation.fig, plot_frames, frames = frames,interval=30)
        writer = animation.writers['ffmpeg'](fps=20)

        simulation_path = loader.get_simulation_path(run_name)
        ani.save(simulation_path + '/animation_vid_7.mp4',writer=writer,dpi=100)

def pos_pos_spr(
    run_name,
    shown_indexes,
    shift_amount_per_frame = 1,
    frames = 500,
    live_plotting = True,
    sample_mode = False,
    sample_range = [0,2*100]
):
    if(sample_mode):
        if(not ppc.check_if_all_phase_data_exists(run_name)):
            print(bcolors.WARNING + "Making phase date since it did not exist." + bcolors.ENDC)
            ppc.create_phase_data(run_name)
        phase_data_block = load_phase_data(run_name, "_block")[sample_range[0]:sample_range[1]]
        position_data, velocity_data = seperate_pos_vel_data(phase_data_block)
    else:
        phase_data_block = load_phase_data(run_name)
        position_data, velocity_data = seperate_pos_vel_data(phase_data_block)
    phase_data_pad = load_phase_data(run_name, "_pad")
    print(phase_data_pad)
    position_data_pad = phase_data_pad[0]



    position_plot = make_single_line_line_plot(position_data, shown_indexes, ax_number = 0)
    bar_plot = make_single_line_bar_plot(position_plot, ax_number = 1, line_number = 0)
    position_pad_plot = make_single_line_line_plot([position_data_pad], shown_indexes, ax_number = 2)#, x_values = np.arange(len(position_data_pad)))
    spring_plot = make_single_line_spring_plot(position_plot, ax_number = 3, line_number = 0, pad_position = position_data_pad, common_indexes=shown_indexes)
    animation_plots = [position_plot, bar_plot, position_pad_plot, spring_plot]

    bk_animation = BKAnimation(animation_plots, sleep_amount=0.001, max_lim_change=0.3)
    bk_animation.init_live_plot((231, 232, 233, 212), (20,10), ["Position", "Bar", "Position pad", "Spring"])
    bk_animation.update_subplots(update_lims=False)


    plot_shape = [len(animation_plots), len(position_plot.single_line_list)]
    def plot_frames(n = 0):

        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 0, n_line_number = plot_shape[1], index_type = 'y')
        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 2, n_line_number = 1, index_type = 'y')
        bk_animation.shift_x_indexes(ax_number = 3, line_number = 2, shift_amount = shift_amount_per_frame)
        bk_animation.change_y(1, 0)
        bk_animation.change_x(3, 0, get_axis="y")
        bk_animation.change_x(3, 1)
        bk_animation.update_upper_springs(3, index_first_spring = 3)
        bk_animation.update_subplots()
        sleep(bk_animation.sleep_amount)


    if(live_plotting):
        for i in range(0, frames):
            plot_frames(i)
    else:
        ani = animation.FuncAnimation(bk_animation.fig, plot_frames, frames = frames,interval=30)
        writer = animation.writers['ffmpeg'](fps=20)

        simulation_path = loader.get_simulation_path(run_name)
        ani.save(simulation_path + '/animation_vid_bk.mp4',writer=writer,dpi=100)

def pos_pha_spr_friction(
    run_name,
    shown_indexes,
    shift_amount_per_frame = 1,
    frames =3699,
    live_plotting = True,
    sample_range = [0,2*100],
    velocity_index = 8
):
    phase_data_block = load_friction_phase_data(run_name, "block_phase_" + str(velocity_index))
    frames_per_velocity = phase_data_block.shape[1]
    velocities = loadYaml(run_name, "/results/friction_phase/increment_run.yaml")["velocities"]
    for i in range(velocity_index + 1, len(velocities)):
        phase_data_block = np.concatenate((phase_data_block, load_friction_phase_data(run_name, "block_phase_" + str(i))), axis=1)
    position_data, velocity_data = seperate_pos_vel_data(phase_data_block)

    number_of_data_points = len(position_data[0])
    if(frames > number_of_data_points):
        frames = number_of_data_points

    position_plot = make_single_line_line_plot(position_data, shown_indexes, ax_number = 0)
    bar_plot = make_single_line_bar_plot(position_plot, ax_number = 1, line_number = 0)
    phase_plot = make_single_line_phase_plot(position_data, velocity_data, shown_indexes, ax_number = 2)
    spring_plot = make_single_line_spring_plot(position_plot, ax_number = 3, line_number = 0, common_indexes=shown_indexes)
    animation_plots = [position_plot, bar_plot, phase_plot, spring_plot]

    bk_animation = BKAnimation(animation_plots, sleep_amount=0.000, max_lim_change=0.3)
    bk_animation.init_live_plot((231, 232, 233, 212), (20,10), ["Position", "Bar", "Phase", "Spring"], show_plot = live_plotting, suptitle="Velocity: " + str(velocities[velocity_index]))


    plot_shape = [len(animation_plots), len(position_plot.single_line_list)]
    only_shift_first = False # used for data points after the remaining data points are fewer than the  showed ones
    only_shift_first_index = number_of_data_points - shown_indexes[1] - 1 # 0 indexation
    def init_plot_frames():
        bk_animation.update_subplots(update_lims=False)
    def plot_frames(n):
        nonlocal velocity_index, only_shift_first
        bk_animation.update_suptitle("Velocity: %(velocity)1.3f | Frame: %(frame)i" % {'velocity': velocities[velocity_index], 'frame': n})
        if(n % frames_per_velocity == 0 and n > 0):
            velocity_index += 1

        if(n % only_shift_first_index == 0 and n > 0):
            only_shift_first = True
        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 0, n_line_number = plot_shape[1], index_type = 'y', only_shift_first = only_shift_first)
        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 2, n_line_number = plot_shape[1], index_type = 'xy', only_shift_first = only_shift_first)
        #bk_animation.shift_x_indexes(ax_number = 3, line_number = 2, shift_amount = shift_amount_per_frame)
        bk_animation.change_y(1, 0)
        bk_animation.change_x(3, 0, get_axis="y")
        #bk_animation.change_x(3, 1)
        bk_animation.update_upper_springs(3, index_first_spring = 1)

        bk_animation.update_subplots()
        sleep(bk_animation.sleep_amount)


    if(live_plotting):
        init_plot_frames()
        for i in range(0, frames):
            plot_frames(i)
    else:
        ani = animation.FuncAnimation(bk_animation.fig, plot_frames, init_func = init_plot_frames, frames = frames,interval=30)
        writer = animation.writers['ffmpeg'](fps=20)

        simulation_path = loader.get_simulation_path(run_name)
        ani.save(simulation_path + '/animation_vid_7.mp4',writer=writer,dpi=100)

def pos_pos_spr_friction(
    run_name,
    shown_indexes,
    shift_amount_per_frame = 1,
    frames =3699,
    live_plotting = True,
    sample_range = [0,2*100],
    velocity_index = 14
):
    phase_data_block = load_friction_phase_data(run_name, "block_phase_" + str(velocity_index))
    phase_data_pad = load_friction_phase_data(run_name, "pad_phase_" + str(velocity_index))
    frames_per_velocity = phase_data_block.shape[1]
    velocities = loadYaml(run_name, "/results/friction_phase/increment_run.yaml")["velocities"]
    for i in range(velocity_index + 1, len(velocities)):
        phase_data_block = np.concatenate((phase_data_block, load_friction_phase_data(run_name, "block_phase_" + str(i))), axis=1)
        phase_data_pad = np.concatenate((phase_data_pad, load_friction_phase_data(run_name, "pad_phase_" + str(i))), axis=1)

    position_data, velocity_data = seperate_pos_vel_data(phase_data_block)
    if("--nopad" in sys.argv):
        phase_shape_block = phase_data_block.shape
        block_average = np.zeros(phase_shape_block[1])
        number_of_blocks = int(phase_shape_block[0]/2)
        for i in range(0, number_of_blocks):
            block_average += phase_data_block[2*i]
        position_data_pad = block_average/number_of_blocks

        position_data_pad_title = "Block avg. position"
        position_data_pad_y_label = "$\\frac{\sum_1^n  u_i}{n}$"
        show_pad = False
        index_first_spring = 1
    else:
        position_data_pad = phase_data_pad[0]
        position_data_pad_title = "Pad position"
        position_data_pad_y_label = "$x$"
        show_pad = True
        index_first_spring = 3


    number_of_data_points = len(position_data[0])
    if(frames > number_of_data_points):
        frames = number_of_data_points

    position_plot = make_single_line_line_plot(position_data, shown_indexes, ax_number = 0, y_min = -3.0, y_max = 2.0, x_label="Time steps shown", y_label="$u_i$")
    bar_plot = make_single_line_bar_plot(position_plot, ax_number = 1, line_number = 0, x_label="Block", y_label="$u_i$")
    position_pad_plot = make_single_line_line_plot([position_data_pad], shown_indexes, ax_number = 2, y_min = -3.0, y_max = 2.0, x_label="Time steps shown", y_label=position_data_pad_y_label)#, x_values = np.arange(len(position_data_pad)))
    spring_plot = make_single_line_spring_plot(position_plot, ax_number = 3, line_number = 0, common_indexes=shown_indexes, pad_position=position_data_pad, x_label="$x$", y_label="Model", show_pad = show_pad)
    animation_plots = [position_plot, bar_plot, position_pad_plot, spring_plot]

    bk_animation = BKAnimation(animation_plots, sleep_amount=0.000, max_lim_change=0.3)
    bk_animation.init_live_plot((231, 232, 233, 212), (20,10), ["Position", "Bar", position_data_pad_title, "Spring"], show_plot = live_plotting, suptitle="Velocity: " + str(velocities[velocity_index]))


    plot_shape = [len(animation_plots), len(position_plot.single_line_list)]
    only_shift_first = False # used for data points after the remaining data points are fewer than the  showed ones
    only_shift_first_index = number_of_data_points - shown_indexes[1] - 1 # 0 indexation
    def init_plot_frames():
        bk_animation.update_subplots(update_lims=False)
    def plot_frames(n):
        nonlocal velocity_index, only_shift_first
        bk_animation.update_suptitle("Velocity: %(velocity)1.3f | Frame: %(frame)i" % {'velocity': velocities[velocity_index], 'frame': n})
        if(n % frames_per_velocity == 0 and n > 0):
            velocity_index += 1

        if(n % only_shift_first_index == 0 and n > 0):
            only_shift_first = True
        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 0, n_line_number = plot_shape[1], index_type = 'y', only_shift_first = only_shift_first)
        shift_z_indexes_of_lines_in_ax(bk_animation, shift_amount_per_frame, ax_number = 2, n_line_number = 1, index_type = 'y', only_shift_first = only_shift_first)
        #bk_animation.shift_x_indexes(ax_number = 3, line_number = 2, shift_amount = shift_amount_per_frame)
        bk_animation.change_y(1, 0)
        bk_animation.change_x(3, 0, get_axis="y")
        if(show_pad):
            bk_animation.change_x(3, 1)
        bk_animation.update_upper_springs(3, index_first_spring = index_first_spring)

        bk_animation.update_subplots()
        sleep(bk_animation.sleep_amount)


    if(live_plotting):
        init_plot_frames()
        for i in range(0, frames):
            plot_frames(i)
            # if((i+1)%10 == 0):
            #     input()
    else:
        ani = animation.FuncAnimation(bk_animation.fig, plot_frames, init_func = init_plot_frames, frames = frames,interval=30)
        writer = animation.writers['ffmpeg'](fps=20)

        simulation_path = loader.get_simulation_path(run_name)
        ani.save(simulation_path + '/animation_vid_7.mp4',writer=writer,dpi=100)

if __name__ == "__main__":
    shown_indexes = [0, 100]
    if(len(sys.argv) >= 2):
        live_plotting = False
        sample_mode = False
        if('-l' in sys.argv):
            live_plotting = True
        if('-d' in sys.argv):
            sample_mode = True
        #pos_vel_pha_bar(sys.argv[1], shown_indexes, live_plotting=live_plotting)
        if('--friction' in sys.argv):
            #pos_pha_spr_friction(sys.argv[1], shown_indexes, live_plotting=live_plotting)
            pos_pos_spr_friction(sys.argv[1], shown_indexes, live_plotting=live_plotting)
        else:
            pos_pos_spr(sys.argv[1], shown_indexes, live_plotting=live_plotting, sample_mode=sample_mode)
