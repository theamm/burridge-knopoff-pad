# Animation of BK-pad output

The goal is to make useful movies of the output of the BK-pad simulations. Looking at a movie can tell some different aspects and properties of the output than still images can. An attempt is made to make the code usable from other classes, as is all of the python code in this project. This seems to always fail somewhat, but this "readme" is an attempt to fail a bit less.

## Outline of how it function

During the project there where several layouts developed using the same "animation engine". These are located in "/run_animation_layout.py". This file contain many layouts. Each is contained in a folder with some set configurations to ease the process of running them. The closer to the end of the file a layout is the newer the layout is. Understanding this file can be difficult to understand a demo version is made in "/demo_animation_layout.py".


## Animation specific

* The velocity if run with "-friction" flag shifts when the first data point for that velocity hits the right side of the screen. 

## Specifications

* Send in one single array and plot the data
* Send in two arrays and plot
  * Against each other
  * In subplots
* Specify/tune these parameters
  * plot_size - amount of data points plotted per frame
  * movement_per_frame - amount of data points moved(forward or backward) per frame
  * n_frames - how many frames in the movie
  * fps - frames per second
  * sleep_amount - how long should each frame be shown
* Record and save movies
* Live plot data
* Using list to transport data of what is plotted in each subplot
  * Support for different amount of lines in each.

## How it works

There are threes classes that are involved when making a live or movie plot. They are

1. BKAnimation
2. SinglePlot
3. SingleLine

, where BKAnimation contains a main figure with subplots stored in SinglePlot instances that again contains lineplots stored in SingleLine instances. Below is a description of all the classes.

### BKAnimation

* single_plots,
* frame_interval = 10,
* movement_per_frame = 10,
* n_frames = 10,
* fps  = 10,
* sleep_amount = 1

The values set are defaults that are overrided when set.

key functions:

* __init__
  * Constructur, possible to set above variables when initiating
  * ```bk_animation = BKAnimation([single_plot, single_plot_2])```
* init_live_plot
  * initialize figure and setup specified figures
  * ```bk_animation.init_live_plot((211, 212), (20,20))```
* init_subplots
  * sets up each of the subplots
* init_specified_subplot
  * sets up each line in a subplot
* update_subplots
  * call after updating values in a plot
* update_specified_plots
  * update  each og the lines in a subplot
* update_live_*
  * updates specific values in a line


### SinglePlot

Contains:

* ax_number,
* x_lim = None,
* y_lim = None,
* single_line_list = []
* line_type = 'line

key functions:

* __init__
  * constructor
  * ```single_plot = SinglePlot(ax_number = 0)```
  * ```single_plot = SinglePlot(ax_number = 1,single_line_list = [single_line])```
* append_single_line
  * append a SingleLine object ot the SinglePlot
* change_*_lim
  * change limits of *


### SingleLine

Contains:

* x_values
* y_values
* x_indexes
* y_indexes
* ax_number
* line_number

key functions:

* __init__
  * constructor
  * ```single_line = SingleLine(x_values, y_values, x_indexes = [0,100], y_indexes = [0,100], ax_number= 0, line_number = 0 )```
  * Note: x_values and y_valeus must be of the same size
* print_all
  * prinst current variable values
