from time import sleep
import numpy as np

import sys
import os
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/..")

from helpFunctions import LoadFile

from animationTools import BKAnimation, animation
from animationSupportClasses import SingleLine, SinglePlot, bcolors

loader = LoadFile()

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/../PhasePlot")

import phase_plot_creator as ppc