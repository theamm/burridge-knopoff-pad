from animation_run_imports import *

def load_phase_data(
    run_name,
    phase_part = ""
):
    phase_data = lazy_load_data(\
        run_name, \
        "phase_plot_at_threshold" + phase_part, \
        use_shared_names = False \
        )
    return phase_data

def load_friction_phase_data(
    run_name,
    phase_part = ""
):
    phase_data = lazy_load_data(\
        run_name, \
        "results/friction_phase/" + phase_part, \
        use_shared_names = False \
        )
    return phase_data

def lazy_load_data(
    run_name,
    file_name,
    use_shared_names
):
    parameters = loader.load_yaml_parameters(run_name)
    phase_data = loader.load_simulation_output(\
        parameters,\
        run_name, \
        file_name, \
        use_shared_names = use_shared_names \
        )
    return phase_data

def loadYaml(
    run_name,
    file_name
):
    yaml = loader.loadYaml(run_name, file_name)
    return yaml

def make_single_line_line_plot(
    y_data,
    common_indexes, 
    ax_number,
    x_values = [],
    y_min = 0,
    y_max = 0,
    x_label = "",
    y_label = ""
):
    if(y_min == y_max == 0):
        y_lim = {\
            'min': np.min(y_data), \
            'max': np.max(y_data)\
            }
    else:
        y_lim = {\
            'min': y_min, \
            'max': y_max\
            }
    x_lim = {'min': common_indexes[0], 'max': common_indexes[1] - 1}

    line_list = []
    for i, y_values in enumerate(y_data):
        if(len(x_values) == 0):
            x_values = np.arange(len(y_values))
        single_line = SingleLine(\
            x_values, \
            y_values, \
            x_indexes = common_indexes, \
            y_indexes = common_indexes, \
            ax_number = ax_number, \
            line_number = i)
   
        line_list.append(single_line)

    single_plot = SinglePlot(\
        ax_number, \
        y_lim = y_lim, \
        x_lim = x_lim, \
        single_line_list = line_list, \
        plot_type = "line",
        x_label = x_label,
        y_label = y_label)

    return single_plot

def make_single_line_phase_plot(
    y_data,
    x_data,
    common_indexes, 
    ax_number,
    x_label = "",
    y_label = ""
):
    y_lim = {\
        'min': np.min(y_data), \
        'max': np.max(y_data)\
        }
    x_lim = {\
        'min': np.min(x_data), \
        'max': np.max(x_data)\
        }

    line_list = []
    for i, (y_values, x_values) in enumerate(zip(y_data, x_data)):
        single_line = SingleLine(\
            x_values, \
            y_values, \
            x_indexes = common_indexes, \
            y_indexes = common_indexes, \
            ax_number = ax_number, \
            line_number = i)
   
        line_list.append(single_line)

    single_plot = SinglePlot(\
        ax_number, \
        y_lim = y_lim, \
        x_lim = x_lim, \
        single_line_list = line_list, \
        plot_type = "line",
        x_label = x_label,
        y_label = y_label)

    return single_plot

def make_single_line_bar_plot(
    single_plot,
    ax_number,
    line_number,
    x_label = "",
    y_label = ""
):
    n_bar_values = len(single_plot.single_line_list)
    bar_values = np.array([single_plot.get_last_plotted_y_value(i) for i in range(0, n_bar_values)])
    bar_position = np.array(np.arange(0, n_bar_values))
    bar_lim = {'min': -0.5, 'max': n_bar_values - 0.5}
    y_lim = single_plot.y_lim
    if(y_lim['min'] > 0.0):
        y_lim['min'] = 0.0
    elif(y_lim['max'] < 0.0):
        y_lim['max'] = 0.0
    line_dependencies = {'ax_numbers': [single_plot.ax_number], 'line_numbers': [bar_position]}
    bar_line = SingleLine(bar_position, bar_values, x_indexes=bar_position, y_indexes=bar_position, ax_number = ax_number, line_number = line_number, line_dependencies=line_dependencies)
    bar_plot = SinglePlot(
        ax_number, 
        single_line_list = [bar_line], 
        x_lim=bar_lim, 
        y_lim=y_lim, 
        plot_type = 'bar',
        x_label = x_label,
        y_label = y_label)

    return bar_plot

def make_single_line_spring_plot(
    single_plot,
    ax_number,
    line_number,
    pad_position = [],
    common_indexes = [],
    height_of_plot = 0.1,
    x_label = "",
    y_label = "",
    show_pad = False
):
    n_spring_values = len(single_plot.single_line_list)
    spring_values = np.array([single_plot.get_last_plotted_y_value(i) for i in range(0, n_spring_values)])
    spring_positions = np.array(np.arange(0, n_spring_values))
    spring_lim = {'min': -height_of_plot/2, 'max': height_of_plot}
    line_dependencies = {'ax_numbers': [single_plot.ax_number], 'line_numbers': [spring_positions]}
    spring_indexes = [spring_positions[0], spring_positions[-1]]

    spring_line = SingleLine(x_values = spring_values, \
        y_values = np.array([0.0]*n_spring_values), \
        starting_position = {"x":spring_positions, "y": 0.0}, \
        x_indexes=spring_indexes, \
        y_indexes=spring_indexes, \
        ax_number = ax_number, \
        line_number = line_number, \
        line_dependencies=line_dependencies,
        marker="s",
        marker_size=5.0)

    line_shifter = 1
    line_list = [spring_line]

    if(len(pad_position) > 0 and len(common_indexes) > 0 and show_pad):
        pad_line = SingleLine(x_values = np.array([pad_position[common_indexes[-1]]]*2), \
            y_values = np.array([height_of_plot/2]*2), \
            starting_position = {"x":np.array(spring_positions[[0,-1]]), "y": 0.0}, \
            x_indexes = np.array([0, 1]), \
            y_indexes = np.array([0, 1]), \
            ax_number = ax_number, \
            line_number = line_number + 1, \
            line_dependencies= {'ax_numbers': [ax_number], 'line_numbers': [np.array([line_number + 2]*2)]}, \
            line_width= 10.0,
            color = 'tab:grey')
        pad_values = SingleLine(x_values = np.array(pad_position), \
            y_values = np.array([height_of_plot/2]),
            x_indexes = [common_indexes[-1]]*2,
            y_indexes = [0]*2,
            ax_number = ax_number,
            line_number = line_number + 2,
            marker = '+'
            )
        line_shifter += 2
        line_list += [pad_line, pad_values]



    for i, (spring_value, spring_position) in enumerate(zip(spring_values, spring_positions)):
        x_values = np.array([spring_position + spring_value, spring_position])
        y_values = np.array([0, height_of_plot/2])
        common_indexes = np.array([0,1])
        single_line = SingleLine(\
            x_values, \
            y_values, \
            x_indexes = common_indexes, \
            y_indexes = common_indexes, \
            ax_number = ax_number, \
            line_number = i + line_shifter,
            color = 'tab:red')
        line_list.append(single_line)

    spring_plot = SinglePlot(ax_number, \
        single_line_list = line_list, \
        x_lim={'min': -3.0 + spring_positions[0], \
        'max': 3.0 + spring_positions[-1]}, \
        y_lim=spring_lim, \
        plot_type = 'spring',
        x_label = x_label,
        y_label = y_label)

    return spring_plot


def seperate_pos_vel_data(
    phase_data
):
    pos_data = []
    vel_data = []

    for i in range(0, int(len(phase_data)/2)):
        pos_data.append(phase_data[2*i])
        vel_data.append(phase_data[2*i + 1])

    return (pos_data, vel_data)

def shift_z_indexes_of_lines_in_ax(
    bk_animation, 
    shift_amount_per_frame, 
    ax_number, 
    n_line_number,
    index_type,
    only_shift_first = False
):
    for line_number in range(0, n_line_number):
        if(index_type == 'y'):
            bk_animation.shift_y_indexes(ax_number, line_number, shift_amount_per_frame, only_shift_first = only_shift_first)
            if(only_shift_first):
                bk_animation.shift_x_indexes(ax_number, line_number, shift_amount_per_frame, only_shift_first = only_shift_first)    
        elif(index_type == 'xy'):
            bk_animation.shift_xy_indexes(ax_number, line_number, shift_amount_per_frame, only_shift_first = only_shift_first)
        elif(index_type == 'x'):
            bk_animation.shift_x_indexes(ax_number, line_number, shift_amount_per_frame, only_shift_first = only_shift_first)
        else:
            print("unknown access type")
    