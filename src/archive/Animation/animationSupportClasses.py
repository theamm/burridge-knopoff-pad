import numpy as np

class bcolors:
    WARNING = '\033[93m'
    ENDC = '\033[0m'
    OKBLUE = '\033[31;1;4m'

class SinglePlot():
    def __init__(
        self,
        ax_number,
        x_lim = None,
        y_lim = None,
        single_line_list = [],
        starting_postions = [],
        plot_type = 'line',
        scale_10_percent = True,
        x_label = "",
        y_label = ""
    ):
        self.ax_number = ax_number
        self.single_line_list = single_line_list
        self.x_lim = x_lim
        self.y_lim = y_lim
        self.plot_type = plot_type
        self.starting_positions = starting_postions
        self.x_label = x_label
        self.y_label = y_label
        if(len(single_line_list) == 0 or y_lim == None or x_lim == None):
            print(bcolors.WARNING + "Warning: initiated Singleplot without SingleLine element(s), y_lim or x_lim. Remember to initiate before plotting." + bcolors.ENDC)
        if(len(starting_postions) == 0):
            for single_line in single_line_list:
                self.set_starting_postions(starting_position=single_line.starting_position)

    def set_starting_postions(
        self,
        n = 1,
        starting_position = {"x": 0.0, "y": 0.0}
    ):
        self.starting_positions + [starting_position] * n
    def append_single_line(
        self,
        single_line
    ):
        self.single_line_list.append(single_line)
        self.set_starting_postions(starting_position=single_line.starting_position)

    def set_x_lim(
        self,
        x_lim_min,
        x_lim_max
    ):
        self.x_lim = {'min': x_lim_min, 'max': x_lim_max}

    def set_y_lim(
        self,
        y_lim_min,
        y_lim_max
    ):
        self.y_lim = {'min': y_lim_min, 'max': y_lim_max}

    def change_x_lim(
        self,
        x_lim_min_add,
        x_lim_max_add
    ):
        self.x_lim = {'min': self.x_lim['min'] + x_lim_min_add, 'max': self.x_lim['max'] + x_lim_max_add}

    def change_y_lim(
        self,
        y_lim_min_add,
        y_lim_max_add
    ):
        self.y_lim = {'min': self.y_lim['min'] + y_lim_min_add, 'max': self.y_lim['max'] + y_lim_max_add}

    def shift_x_indexes(
        self,
        line_number,
        shift_amount,
        only_shift_first = False
    ):
        self.single_line_list[line_number].shift_x_indexes(shift_amount, only_shift_first = only_shift_first)

    def shift_y_indexes(
        self,
        line_number,
        shift_amount,
        only_shift_first = False
    ):
        self.single_line_list[line_number].shift_y_indexes(shift_amount, only_shift_first = only_shift_first)

    def change_x(
        self,
        line_number,
        new_height
    ):
        self.single_line_list[line_number].set_x_values(new_height)

    def change_y(
        self,
        line_number,
        new_height
    ):
        self.single_line_list[line_number].set_y_values(new_height)

    def get_last_plotted_x_value(
        self,
        line_number
    ):
        return self.single_line_list[line_number].get_x_values(only_last_value_by_index=True)

    def get_last_plotted_y_value(
        self,
        line_number
    ):
        return self.single_line_list[line_number].get_y_values(only_last_value_by_index=True)

class SingleLine():
    def __init__(
        self,
        x_values,
        y_values,
        x_indexes,
        y_indexes,
        ax_number,
        line_number,
        starting_position = {"x": 0.0, "y": 0.0},
        line_dependencies = None,
        marker = ".",
        marker_size = 0.5,
        line_width = 1.0,
        color = 'tab:blue'
    ):
        self.x_values = x_values
        self.y_values = y_values
        self.x_i = np.array(x_indexes)
        self.y_i= np.array(y_indexes)
        self.ax_number = ax_number
        self.line_number = line_number
        self.starting_position = starting_position
        self.line_dependencies = line_dependencies
        self.marker = marker
        self.marker_size = marker_size
        self.line_width = line_width
        self.color = color

    def print_all(
        self
    ):
        print("\nx_values: " + str(self.x_values))
        print("y_values: " + str(self.y_values))
        print("x_indexes: " + str(self.x_i))
        print("y_indexes: " + str(self.y_i))
        print("ax_number: " + str(self.ax_number))
        print("line_number: " + str(self.line_number))
        print("line_dependencies: " + str(self.line_dependencies))
        print("starting_position: " + str(self.starting_position))

    def shift_x_indexes(
        self,
        shift_amount,
        only_shift_first = False
    ):
        if(only_shift_first):
            self.x_i[0] += shift_amount
        else:
            self.x_i += shift_amount

    def shift_y_indexes(
        self,
        shift_amount,
        only_shift_first = False
    ):
        if(only_shift_first):
            self.y_i[0] += shift_amount
        else:
            self.y_i += shift_amount

    def get_x_indexes(
        self,
        only_last_index = False
    ):
        if(not only_last_index):
            return self.x_i
        else:
            return self.x_i[-1]

    def set_x_indexes(
        self,
        new_value
    ):
        self.x_i = new_value

    def get_y_indexes(
        self,
        only_last_index = False
    ):
        if(not only_last_index):
            return self.y_i
        else:
            return self.y_i[-1]

    def set_y_indexes(
        self,
        new_value
    ):
        self.y_i = new_value

    def get_x_values(
        self,
        only_last_value_by_index = False
    ):
        if(not only_last_value_by_index):
            return self.x_values
        else:
            return self.x_values[self.get_x_indexes(only_last_index=True)]

    def set_x_values(
        self,
        new_value
    ):
        self.x_values = new_value

    def get_y_values(
        self,
        only_last_value_by_index = False
    ):
        if(not only_last_value_by_index):
            return self.y_values
        else:
            print("y_indexes: " + str(self.y_i))
            return self.y_values[self.get_y_indexes(only_last_index=True)]

    def set_y_values(
        self,
        new_value
    ):
        self.y_values = new_value

    def get_starting_position(
        self,
        only_last_value_by_index = False
    ):
        if(not only_last_value_by_index):
            return self.starting_position
        else:
            return self.starting_position[self.get_y_indexes(only_last_index=True)]
