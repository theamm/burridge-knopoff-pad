from matplotlib import pyplot as plt, animation, artist
import numpy as np

from animationSupportClasses import SingleLine, SinglePlot, bcolors

class BKAnimation():
    def __init__(
        self,
        single_plots,
        frame_interval = 10,
        movement_per_frame = 10,
        n_frames = 10,
        fps  = 10,
        sleep_amount = 1,
        max_lim_change = 0.1
    ):
        self.single_plots = single_plots

        self.frame_interval = frame_interval
        self.movement_per_frame = movement_per_frame
        self.n_frames = n_frames
        self.fps = fps
        self.sleep_amount = sleep_amount 
        self.max_lim_change = max_lim_change

    ## START: INITIALIZE ##

    def init_live_plot(
        self,
        positions,
        figure_shape,
        titles,
        show_plot = True,
        suptitle = ""
    ):
        #plt.ion()
        self.fig = plt.figure(figsize=figure_shape)
        #self.axs = [self.fig.add_subplot(pos, title = title)  for pos, title in zip(positions, titles)]
        self.fig.suptitle(suptitle, fontsize=12)

        self.init_subplots()
        #self.fig.canvas.manager.full_screen_toggle()
        if(show_plot):
            self.fig.show()
        self.fig.canvas.draw()

    def init_subplots(
        self
    ):
        for single_plot in self.single_plots:
            print("printing plot: " + str(single_plot.ax_number) + ", length: " + str(len(single_plot.single_line_list)) + ", plot type: " + single_plot.plot_type)
            self.init_specified_subplot(single_plot)
            self.init_lims(single_plot)
            self.init_labels(single_plot)

        #quit()

    def init_specified_subplot(
        self,
        single_plot
    ):
        for single_line in single_plot.single_line_list:
            print("printing line:  ax_number: "  + str(single_line.ax_number) + ", line_number: " + str(single_line.line_number))
            if(single_plot.plot_type == 'bar'):
                self.init_bar_plot(single_line, single_plot.ax_number)
            elif(single_plot.plot_type == 'spring'):
                self.init_spring_plot(single_line, single_plot.ax_number)
            else:
                self.init_line_plot(single_line, single_plot.ax_number)

    def init_line_plot(
        self,
        single_line,
        ax_number
    ):
        self.axs[ax_number].plot(\
            single_line.x_values[single_line.x_i[0]:(single_line.x_i[1])] + single_line.starting_position["x"],\
            single_line.y_values[single_line.y_i[0]:(single_line.y_i[1])] + single_line.starting_position["y"],\
            marker = single_line.marker,
            markersize = single_line.marker_size
            )

    def init_spring_plot(
        self,
        single_line,
        ax_number
    ):
        single_line.print_all()
        self.axs[ax_number].plot(\
            single_line.x_values[single_line.x_i[0]:(single_line.x_i[1] + 1)] + single_line.starting_position["x"],\
            single_line.y_values[single_line.y_i[0]:(single_line.y_i[1] + 1)] + single_line.starting_position["y"],\
            marker = single_line.marker,
            markersize = single_line.marker_size,
            linewidth = single_line.line_width,
            color = single_line.color
        )

    def init_bar_plot(
        self,
        single_line,
        ax_number
    ):
        self.axs[ax_number].bar(single_line.x_values[single_line.x_i], single_line.y_values[single_line.y_i])

    def init_lims(
        self,
        single_plot
    ):
        self.update_live_x_lim(single_plot)
        self.update_live_y_lim(single_plot)

    def init_labels(
        self,
        single_plot
    ):
        self.axs[single_plot.ax_number].set_xlabel(single_plot.x_label)
        self.axs[single_plot.ax_number].set_ylabel(single_plot.y_label)

    ## END: INITIALIZE ##

    ## START: UPDATE ##

    def update_subplots(
        self,
        update_lims = False
    ):
        for single_plot in self.single_plots:
            self.update_specified_subplot(single_plot)
            if(update_lims):
                self.update_live_x_lim(single_plot)
                self.update_live_y_lim(single_plot)
        self.fig.canvas.draw()
        
    def update_specified_subplot(
        self,
        single_plot
    ):
        print("ax: " + str(single_plot.ax_number))
        for single_line in single_plot.single_line_list:
            self.update_live_x_values(single_line, single_plot.plot_type)
            self.update_live_y_values(single_line, single_plot.plot_type)

    def update_live_x_values(
        self,
        single_line,
        plot_type
    ):
        if(plot_type == 'spring'):
            self.axs[single_line.ax_number].lines[single_line.line_number].set_xdata(single_line.x_values[single_line.x_i[0]:(single_line.x_i[1] + 1)] + single_line.starting_position["x"])
        elif(self.axs[single_line.ax_number].lines):
            # if(single_line.ax_number == 0):
            #     print("x indexes: ", single_line.x_i)
            #     print(len(single_line.x_values[single_line.x_i[0]:single_line.x_i[1]] + single_line.starting_position["x"]))
            #     print(len(single_line.x_values))
            #     print(len(single_line.y_values[single_line.x_i[0]:single_line.x_i[1]] + single_line.starting_position["x"]))
            #     print(len(single_line.y_values))
            #     print("y_indexes: ", single_line.y_i)
            self.axs[single_line.ax_number].lines[single_line.line_number].set_xdata(single_line.x_values[single_line.x_i[0]:single_line.x_i[1]] + single_line.starting_position["x"])
        elif(plot_type == 'bar'):
            print("bar")
        else:
            print("Please initiate ax plot before updating.")
            quit()

    def update_live_y_values(
        self,
        single_line,
        plot_type
    ):
        if(plot_type == 'spring'):
            return
        elif(self.axs[single_line.ax_number].lines):
            self.axs[single_line.ax_number].lines[single_line.line_number].set_ydata(single_line.y_values[single_line.y_i[0]:single_line.y_i[1]] + single_line.starting_position["y"])
        elif(plot_type == 'bar'):
            for rect, h in zip(self.axs[single_line.ax_number].patches, single_line.get_y_values()):
                rect.set_height(h)
        else:
            print("Please initiate ax plot before updating.")
            quit()

    

    def update_live_x_lim(
        self,
        single_plot
    ):
        if(self.axs[single_plot.ax_number].lines or single_plot.plot_type == 'bar'):
            self.axs[single_plot.ax_number].set_xlim(single_plot.x_lim['min'], single_plot.x_lim['max'])
        else:
            print("Please initiate ax plot before updating.")
            quit()

    def update_live_y_lim(
        self,
        single_plot
    ):
        if(self.axs[single_plot.ax_number].lines or single_plot.plot_type == 'bar'):
            self.axs[single_plot.ax_number].set_ylim(single_plot.y_lim['min'], single_plot.y_lim['max'])
        else:
            print("Please initiate ax plot before updating.")
            quit()

    def update_upper_springs(
        self,
        ax_number,
        index_first_spring,
        block_line_index = 0
    ):
        block_spring_plot = self.single_plots[ax_number].single_line_list[block_line_index]
        block_line_x_values = block_spring_plot.get_x_values()
        block_line_start_positions = block_spring_plot.get_starting_position()['x']
        pad_shift = self.single_plots[ax_number].get_last_plotted_x_value(2)
        if(index_first_spring > 1):
            block_line_x_values += pad_shift
        for i, upper_spring in enumerate(self.single_plots[ax_number].single_line_list[index_first_spring:]):
            upper_spring_x_values = upper_spring.get_x_values()
            upper_spring_x_values[0] = block_line_x_values[i] + block_line_start_positions[i]
            if(index_first_spring > 1):
                upper_spring_x_values[1] = block_line_start_positions[i] + pad_shift

            self.single_plots[ax_number].change_x(i+index_first_spring, upper_spring_x_values)

    def update_suptitle(
        self,
        suptitle
    ):
        self.fig.suptitle(suptitle)

    def check_lims(
        self,
        ax_number,
        axis_checked,
        reduce_extra_lim_amount = 3
    ):
        ax = self.axs[ax_number]
        if(axis_checked == 'y'):
            current_lims = self.single_plots[ax_number].y_lim
        elif(axis_checked == 'x'):
            current_lims = self.single_plots[ax_number].x_lim
        compared_lims = {'min': None, 'max': None}
        for line in ax.lines:
            if(axis_checked == 'y'):
                data = line.get_ydata()
            elif(axis_checked == 'x'):
                data = line.get_xdata()
            else:
                print(bcolors.WARNING + "Warning: tried to update non-existing axis \"" + axis_checked + "\"" + bcolors.ENDC)
                break
            max = np.max(data)
            min = np.min(data)

            if(compared_lims['max'] == None or compared_lims['max'] < max):
                compared_lims['max'] = max
            if(compared_lims['min'] == None or compared_lims['min'] > min):
                compared_lims['min'] = min
        
        new_max = current_lims['max']
        new_min = current_lims['min']

        max_lim_diff = (compared_lims["max"] - compared_lims["min"])*self.max_lim_change
        if(compared_lims['max'] > current_lims['max'] or (current_lims['max'] - compared_lims['max']) > max_lim_diff):
            new_max = compared_lims['max'] + max_lim_diff / reduce_extra_lim_amount
            new_min = compared_lims['min'] - max_lim_diff / reduce_extra_lim_amount
        elif(compared_lims['min'] < current_lims['min'] or (compared_lims['min'] - current_lims['min']) > max_lim_diff):
            new_max = compared_lims['max'] + max_lim_diff / reduce_extra_lim_amount
            new_min = compared_lims['min'] - max_lim_diff / reduce_extra_lim_amount
        if(axis_checked == 'y'):
            ax.set_ylim(new_max, new_min)
        elif(axis_checked == 'x'):
            ax.set_xlim(new_max, new_min)
    
    ## END: UPDATE ##

    ## START: HELP FUNCTIONS ##

    def change_xy_lim(
        self,
        ax_number,
        lim_min_add,
        lim_max_add
    ):
        self.change_x_lim(ax_number, lim_min_add, lim_max_add)
        self.change_y_lim(ax_number, lim_min_add, lim_max_add)

    def change_x_lim(
        self,
        ax_number,
        x_lim_min_add,
        x_lim_max_add
    ):
        self.single_plots[ax_number].change_x_lim(x_lim_min_add, x_lim_max_add)

    def change_y_lim(
        self,
        ax_number,
        y_lim_min_add,
        y_lim_max_add
    ):
        self.single_plots[ax_number].change_y_lim(y_lim_min_add, y_lim_max_add)

    def shift_xy_indexes(
        self,
        ax_number,
        line_number,
        shift_amount,
        only_shift_first = False
    ):
        self.shift_x_indexes(ax_number, line_number, shift_amount, only_shift_first = only_shift_first)
        self.shift_y_indexes(ax_number, line_number, shift_amount, only_shift_first = only_shift_first)

    def shift_x_indexes(
        self,
        ax_number,
        line_number,
        shift_amount,
        only_shift_first = False
    ):
        self.single_plots[ax_number].shift_x_indexes(line_number, shift_amount, only_shift_first = only_shift_first)

    def shift_y_indexes(
        self,
        ax_number,
        line_number,
        shift_amount,
        only_shift_first = False
    ):
        self.single_plots[ax_number].shift_y_indexes(line_number, shift_amount, only_shift_first = only_shift_first)

    def change_x(
        self,
        ax_number,
        line_number,
        get_axis = "x"
    ):
        if(get_axis == "x"):
            new_height = self.get_last_plotted_x_value(ax_number, line_number)
        elif(get_axis == "y"):
            new_height = self.get_last_plotted_y_value(ax_number, line_number)
        new_height = np.array(new_height)
        self.single_plots[ax_number].change_x(line_number, new_height)

    def change_y(
        self,
        ax_number,
        line_number,
        get_axis = "y"
    ):
        if(get_axis == "x"):
            new_height = self.get_last_plotted_x_value(ax_number, line_number)
        elif(get_axis == "y"):
            new_height = self.get_last_plotted_y_value(ax_number, line_number)
        self.single_plots[ax_number].change_y(line_number, new_height)
    ## END: HELP FUNCTIONS ## 

    ## START: GETTERS AND SETTERS ##

    def get_last_plotted_x_value(
        self,
        ax_number,
        line_number
    ):
        line_dependencies = self.single_plots[ax_number].single_line_list[line_number].line_dependencies
        if(line_dependencies):
            x_values = []
            for i, ax_number in enumerate(line_dependencies['ax_numbers']):
                for line_dependency in line_dependencies['line_numbers'][i]:
                    x_values.append(self.single_plots[ax_number].get_last_plotted_x_value(line_dependency))
            return x_values
        return self.single_plots[ax_number].get_last_plotted_x_value(line_number)

    def get_last_plotted_y_value(
        self,
        ax_number,
        line_number
    ):
        line_dependencies = self.single_plots[ax_number].single_line_list[line_number].line_dependencies
        if(line_dependencies):
            y_values = []
            for i, ax_number in enumerate(line_dependencies['ax_numbers']):
                for line_dependency in line_dependencies['line_numbers'][i]:
                    y_values.append(self.single_plots[ax_number].get_last_plotted_y_value(line_dependency))
            return y_values
        return self.single_plots[ax_number].get_last_plotted_y_value(line_number)

    def get_frame_interval(
        self
    ):
        return self.frame_interval

    def set_frame_interval(
        self,
        new_value
    ):
        self.frame_interval = new_value

    def get_movement_per_frame(
        self
    ):
        return self.movement_per_frame

    def set_movement_per_frame(
        self,
        new_value
    ):
        self.movement_per_frame = new_value

    def get_n_frames(
        self
    ):
        return self.n_frames

    def set_n_frames(
        self,
        new_value
    ):
        self.n_frames = new_value

    def get_fps(
        self
    ):
        return self.fps

    def set_fps(
        self,
        new_value
    ):
        self.fps = new_value

    def get_sleep_amount(
        selfSinglePlot
    ):
        return self.sleep_amount

    def set_sleep_amount(
        self,
        new_value
    ):
        self.sleep_amount = new_value

    ## END: GETTERS AND SETTERS ##