from animation_run_imports import *

def create_sine_wave(
    x, #data
    a, #amplitude
    f, #ordinary frequency
    p #phase
):
    return a * np.sin(np.pi * f * x + p)

def new_test():
    amplitude = 1
    frequency = 0.01
    phase = 1

    common_indexes = [0, 100]
    ax_numbers = [0, 1, 2, 3]
    shift_amount_per_frame = 3

    x = np.arange(8000.0)
    sin_line_1 = create_sine_wave(x, amplitude, frequency, phase)
    sin_line_2 = create_sine_wave(x, - amplitude, frequency, 2*phase)

    y_lim = {'min': -amplitude, 'max': amplitude}
    x_lim = {'min': common_indexes[0], 'max': common_indexes[1]}

    single_line_1 = SingleLine(x, sin_line_1, starting_position={"x":0.5, "y": 0.0}, x_indexes = common_indexes, y_indexes = common_indexes, ax_number = ax_numbers[0], line_number = 0, marker = "x", marker_size=5.0)
    single_line_2 = SingleLine(x, sin_line_2, starting_position={"x":0.0, "y": 0.0}, x_indexes = common_indexes, y_indexes = common_indexes, ax_number = ax_numbers[0], line_number = 1, marker = "^", marker_size=5.0)
    single_line_3 = SingleLine(sin_line_1, sin_line_2, x_indexes = common_indexes, y_indexes = common_indexes, ax_number = ax_numbers[1], line_number = 0)
    
    single_plot_1 = SinglePlot(ax_numbers[0], y_lim=y_lim, x_lim=x_lim, single_line_list=[single_line_1, single_line_2])
    #single_plot_1.append_single_line(1)

    single_plot_2 = SinglePlot(ax_numbers[1], y_lim=y_lim, x_lim=y_lim)
    single_plot_2.append_single_line(single_line_3)


    bar_values = np.array([single_plot_1.get_last_plotted_y_value(0), single_plot_1.get_last_plotted_y_value(1)])
    bar_position = np.array([0, 1])
    bar_lim = {'min': -0.5, 'max': 1.5}
    line_dependencies = {'ax_numbers': [0], 'line_numbers': [[0,1]]}
    single_line_bar = SingleLine(bar_position, bar_values, x_indexes=bar_position, y_indexes=bar_position, ax_number = ax_numbers[2], line_number = 0, line_dependencies=line_dependencies)
    single_plot_3 = SinglePlot(ax_numbers[2], y_lim=y_lim, x_lim=bar_lim, single_line_list=[single_line_bar], plot_type = 'bar')
    
    single_line_spring = SingleLine(bar_values, \
        bar_position, \
        starting_position={"x":np.array([0.1, 0.0]), "y": 0.0}, \
        x_indexes=bar_position, \
        y_indexes=bar_position, \
        ax_number = ax_numbers[3], 
        line_number = 0, \
        line_dependencies=line_dependencies, \
        marker = "s", marker_size = 10.0)
    single_plot_4 = SinglePlot(ax_numbers[3], \
        y_lim=bar_lim, \
        x_lim=y_lim, \
        single_line_list=[single_line_spring], \
        plot_type = 'spring')

    bk_animation = BKAnimation([single_plot_1, single_plot_2, single_plot_3, single_plot_4])

    single_line_bar.print_all()
    sleep(1)
    bk_animation.init_live_plot((223, 221, 222, 224), (20,20), ["Position", "Phase", "Velocity", "Spring"])
    
    for i in range(0, 500):
        bk_animation.shift_y_indexes(ax_numbers[0], 0, shift_amount_per_frame)
        bk_animation.shift_y_indexes(ax_numbers[0], 1, shift_amount_per_frame)
        bk_animation.shift_xy_indexes(ax_numbers[1], 0, shift_amount_per_frame)
        bk_animation.change_y(ax_numbers[2], 0)
        bk_animation.change_x(ax_numbers[3], 0, get_axis="y")

        bk_animation.update_subplots()
        sleep(0.02)
        print("NEW FRAME \n\n")

def make_block_single_line(
    y_values,
    ax_number,
    line_number,
    common_indexes,
    x_values = [],
    line_dependencies = None
):
    if(len(x_values) == 0):
        x_values = np.arange(len(y_values))
    single_line = SingleLine(x_values, y_values, x_indexes = common_indexes, y_indexes = common_indexes, ax_number = ax_number, line_number = line_number, line_dependencies=line_dependencies)
    return single_line


def make_block_single_plot(
    single_lines,
    ax_number,
    x_lim,
    y_lim,
    plot_type = 'line'
):
    single_plot = SinglePlot(ax_number, y_lim=y_lim, x_lim=x_lim, single_line_list=single_lines, plot_type=plot_type)
    return single_plot

def plot_blocks(run_name, number_of_blocks = 2, common_indexes = [0, 20], frames = 500, live_plotting = True):
    parameters = loader.load_yaml_parameters(run_name)
    phase_data = loader.load_simulation_output(parameters, run_name, "phase_plot_at_threshold", use_shared_names = False)
    print(phase_data.shape)


    y_lim_position = {'min': np.min(phase_data[number_of_blocks * 2]), 'max': np.max(phase_data[number_of_blocks * 2])}
    y_lim_velocity = {'min': np.min(phase_data[number_of_blocks * 2 +1]), 'max': np.max(phase_data[number_of_blocks * 2+1])}
    #y_lim_position = {'min': -0.7, 'max': -0.6}
    x_lim = {'min': common_indexes[0], 'max': common_indexes[1]}
    shift_amount_per_frame = 1

    position_lines = []
    velocity_lines = []
    for i in range(0, number_of_blocks):
        position_lines.append(make_block_single_line(phase_data[2*i], 0, i, common_indexes))
        velocity_lines.append(make_block_single_line(phase_data[2*i + 1], 3, i, common_indexes))
    position_plot = make_block_single_plot(position_lines, 0, x_lim, y_lim_position)
    velocity_plot = make_block_single_plot(velocity_lines, 3, x_lim, y_lim_velocity)

    phase_line = make_block_single_line(position_lines[0].y_values, 1, 0, common_indexes, x_values=velocity_lines[0].y_values)
    phase_line_2 = make_block_single_line(position_lines[1].y_values, 1, 1, common_indexes, x_values=velocity_lines[1].y_values)
    phase_plot = make_block_single_plot([phase_line, phase_line_2], 1, y_lim_velocity, y_lim_position)#{'min': -0.67, 'max': -0.645})

    bar_values = np.array([position_plot.get_last_plotted_y_value(0), position_plot.get_last_plotted_y_value(1)])
    bar_position = np.array([0, 1,])
    bar_lim = {'min': -0.5, 'max': 1.5}
    line_dependencies = {'ax_numbers': [0], 'line_numbers': [[0,1]]}
    bar_line = make_block_single_line(bar_values, 2, 0, bar_position, x_values=bar_position, line_dependencies=line_dependencies)
    bar_plot = make_block_single_plot([bar_line], 2, x_lim=bar_lim, y_lim=y_lim_position, plot_type = 'bar')

    bk_animation = BKAnimation([position_plot, phase_plot, bar_plot, velocity_plot], sleep_amount=0.01, max_lim_change=0.5)
    bk_animation.init_live_plot((221, 222, 224, 223), (20,10), ["Position", "Phase", "Position bar", "Velocity"])
    print(bk_animation.get_last_plotted_y_value(2, 0))
    sleep(0.5)
    bk_animation.update_subplots(update_lims=True)
    bk_animation.check_lims(1, 'x')
    def plot_frames(n = 0):
        bk_animation.check_lims(1, "x")
        bk_animation.check_lims(1, "y")
        bk_animation.shift_y_indexes(0, 0, shift_amount_per_frame)
        bk_animation.shift_y_indexes(0, 1, shift_amount_per_frame)
        bk_animation.shift_y_indexes(3, 0, shift_amount_per_frame)
        bk_animation.shift_y_indexes(3, 1, shift_amount_per_frame)
        bk_animation.shift_xy_indexes(1, 0, shift_amount_per_frame)
        bk_animation.shift_xy_indexes(1, 1, shift_amount_per_frame)
        print(bk_animation.get_last_plotted_y_value(2, 0))
        bk_animation.change_y(2, 0)
        bk_animation.update_subplots()
        sleep(bk_animation.sleep_amount)
    
    if(live_plotting):
        for i in range(0, frames):
            plot_frames()
    else:
        ani = animation.FuncAnimation(bk_animation.fig, plot_frames,300,interval=30)
        writer = animation.writers['ffmpeg'](fps=30)

        ani.save('demo.mp4',writer=writer,dpi=100)

if __name__ == "__main__":
        if('-demo' in sys.argv):
            new_test()
        elif(len(sys.argv) == 2):
            plot_blocks(sys.argv[1])