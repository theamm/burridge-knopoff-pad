# Runnable files and necesarry results to run them:

## Project Thesis
1. check Energy
    Need to input the run_name as an argument
    Need to have a stationary pad
2. plot friction law
    if saving is wanted, change save directory.
3. plot pad different step size
    input the run_name as an argument
4. 

## PhasePlot
Only relevant if using threshold speed, which was used in the thesis by Sandnes without documentation.
It plots what is happening after the threshold or within the threshold. 
It finds the start index for when the slider velocity is equal to the threshold velocity.
It then calculates an interval based on some hard coded values. 
The result is a phase file for both the pad and the block with x number of values for both position and velcoity, where x is equal to 1/(interval*dt).
After understanding the above, it was chosen not to spend more time on these plots.   