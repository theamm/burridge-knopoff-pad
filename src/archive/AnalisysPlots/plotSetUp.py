import numpy as np
colors_diverging_short = ['#d7191c','#fdae61','#ffffbf','#abd9e9','#2c7bb6']
colors_diverging_long = ['#a50026','#d73027','#f46d43','#fdae61','#fee090','#ffffbf','#e0f3f8','#abd9e9','#74add1','#4575b4','#313695']
colors_qualitative_short = ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e']
colors_qualitative_long = ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#e6ab02','#a6761d']
linestyles = ['-', '--', '-.', ':']


def setSvgLatexMatplotlibSettings(
    pyplot
):
    pyplot.rcParams["svg.fonttype"] = "none"
    pyplot.rcParams["font.size"] = 16
    pyplot.rcParams["savefig.directory"] = "../../Figures/Master"
    pyplot.rcParams["savefig.format"] = "svg"


def checkStepAsContinous(
    yaml_file
):
    debug_continuous_slider_speed = yaml_file["Debug"]["debug_continuous_slider_speed"]
    if(debug_continuous_slider_speed):
        return False
    else:
        return True


def checkIfMultipleBlocks(
    block_data
):
    if(len(block_data.shape) == 1):
        return False
    else:
        return True


def createTimeStepValues(
    block_data,
    yaml_file
):
    multiple_blocks = checkIfMultipleBlocks(block_data)
    start = 0
    stop = yaml_file["Parameters"]["max_time"]

    if(yaml_file["Parameters"]["start_speed_continuous"] > yaml_file["Parameters"]["end_speed_continuous"]):
        stop, start = start, stop

    if(multiple_blocks):
        n_time_steps = block_data.shape[1]
    else:
        n_time_steps = len(block_data)

    time_steps = np.linspace(start=start, stop=stop, num=n_time_steps)
    return time_steps


def createSliderVelocityStepValues(
    block_data,
    yaml_file,
    step_as_continous=False
):
    multiple_blocks = checkIfMultipleBlocks(block_data)
    if(not step_as_continous):
        start = yaml_file["Parameters"]["start_speed_continuous"]
        stop = yaml_file["Parameters"]["end_speed_continuous"]
    else:
        start = yaml_file["Parameters"]["slider_speed"]
        increment = yaml_file["Parameters"]["increment"]
        if(increment < 0):
            start = start - increment
            # this is done to match that each velocity interval
            # is the speed starting at it own speed and ending at the next
        interval = yaml_file["Parameters"]["interval"]
        max_time = yaml_file["Parameters"]["max_time"]

        stop = max_time // interval * increment + start

    if(multiple_blocks):
        n_time_steps = block_data.shape[1]
    else:
        n_time_steps = len(block_data)

    time_steps = np.linspace(start=start, stop=stop, num=n_time_steps)
    return time_steps


def choseColourFromDirection(
    yaml_file
):
    if(not checkStepAsContinous(yaml_file)):
        start_speed_continuous = yaml_file["Parameters"]["start_speed_continuous"]
        end_speed_continuous = yaml_file["Parameters"]["end_speed_continuous"]
    else:
        start_speed_continuous = yaml_file["Parameters"]["slider_speed"]
        increment = yaml_file["Parameters"]["increment"]
        interval = yaml_file["Parameters"]["interval"]
        max_time = yaml_file["Parameters"]["max_time"]

        end_speed_continuous = max_time // interval * increment +\
          start_speed_continuous

    direction = end_speed_continuous - start_speed_continuous

    if(direction < 0):
        return 3
    else:
        return 4
