from analysisSharedFunctions import loadYaml, loadFile
from plotSetUp import setSvgLatexMatplotlibSettings, \
    colors_diverging_short as colors_short, \
    linestyles, \
    createTimeStepValues, \
    createSliderVelocityStepValues, \
    choseColourFromDirection
import sys
from matplotlib import pyplot as plt

setSvgLatexMatplotlibSettings(plt)

if("--artistic" in sys.argv):
    plt.style.use("dark_background")

def prettyPlotPhase(
    position_data,
    velocity_data,
    y_label="\$x\$",
    x_label="\$\\dot{x}\$",
    alpha=1.0,
    color_index=0,
    line_style_index=0,
    label=""
):
    plt.plot(position_data, velocity_data,
             color=colors_short[color_index],
             linestyle=linestyles[line_style_index],
             label=label,
             linewidth=0.4,
             alpha=alpha
             )
    plt.ylabel(y_label)
    plt.xlabel(x_label)


def prettyPlotPad(
    time_steps,
    pad_data,
    yaml_file,
    y_label="\$x\$",
    x_label="\$t\$",
    label="",
    alpha=1.0,
    y_lim=[-1.1, 0.15]
):
    color_index = choseColourFromDirection(yaml_file)
    plt.plot(time_steps, pad_data,
             color=colors_short[color_index],
             linestyle=linestyles[0],
             label=label,
             alpha=alpha
             )

    plt.ylim(y_lim[0], y_lim[1])

    plt.ylabel(y_label)
    plt.xlabel(x_label)


def plot_pad(
    run_name,
    alpha=1.0,
    label="Pad"
):
    yaml_file = loadYaml(run_name)
    pad_position = loadFile(run_name, yaml_file, file_name="pad_position")
    pad_velocity = loadFile(run_name, yaml_file, file_name="pad_velocity")
    time_steps = createTimeStepValues(pad_position, yaml_file)

    prettyPlotPad(time_steps, pad_position, yaml_file,
                  alpha=alpha,
                  label=label)
    plt.grid()
    plt.show()
    prettyPlotPhase(pad_position, pad_velocity)


def plot_single_pad(
    run_name
):
    plot_pad(run_name)
    #plt.legend()
    plt.show()


def run():
    if(len(sys.argv) == 2 or (len(sys.argv) == 3 and "--artistic" in sys.argv)):
        plot_single_pad(sys.argv[1])
    # elif(len(sys.argv) > 2):
    #     if("-m" in sys.argv):
    #         plot_compare_pads(sys.argv[1:-1])
    #     else:
    #         plot_compare_pads(sys.argv[1:])


if __name__ == "__main__":
    run()
