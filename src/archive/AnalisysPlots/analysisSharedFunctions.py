import os
import sys
import yaml
import numpy as np
import scipy.fftpack

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

results_path = os.path.dirname(os.path.realpath(__file__)) + \
  "/../../../results/"
yaml_path = "/parameters.yaml"
numeric_method = "_midpoint_"


def loadYaml(
    run_name,
    results_path=results_path,
    yaml_path=yaml_path
):
    stream = open(results_path + run_name + yaml_path, 'r')
    return yaml.load(stream, Loader=Loader)


def getLoader(
):
    sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/..")
    from helpFunctions import LoadFile
    loader = LoadFile()
    return loader


def loadFile(
    run_name,
    yaml_file,
    file_name,
    results_folder="/results/",
    file_ending=".csv"
):
    shared_names = yaml_file["Parameters"]["file_name"]
    full_file_path = results_path + run_name 
    full_file_name = shared_names + numeric_method + file_name + file_ending
    file = np.loadtxt(full_file_path +"/" + full_file_name, delimiter=",")

    return file


def loadPhaseFile(
    run_name,
    file_name,
    friction_folder="/friction_phase/",
    use_shared_names=False
):
    loader = getLoader()

    file_name = friction_folder + file_name

    parameters = loader.load_yaml_parameters(run_name)
    phase_data = loader.load_simulation_output(
        parameters,
        run_name,
        file_name,
        use_shared_names=use_shared_names
        )
    return phase_data

def fourierSpectrum(
    run_name,
    data_vector
):

    loader = getLoader()
    parameters = loader.load_yaml_parameters(run_name)
    sample_points = len(data_vector)
    sample_spacing = loader.get_parameter(parameters, "dt") * loader.get_parameter(parameters, "save_interval_dt")

    y_values = scipy.fftpack.fft(data_vector)


    x_values = np.linspace(0.0, 1.0/(2.0*sample_spacing), sample_points/2)

    y_values = 2.0/sample_points*np.abs(y_values[:sample_points//2])

    return {"x_values": x_values, "y_values": y_values}
