import os
import sys
from matplotlib import pyplot as plt
import numpy as np

from analysisSharedFunctions import loadYaml, loadFile
from plotSetUp import setSvgLatexMatplotlibSettings, \
    colors_qualitative_short as colors_short, \
    linestyles


class CheckEnergy:
    def __init__(
        self,
        run_name="",
        comparison_name="",
        numeric_method="_midpoint_"
    ):
        self.loadFiles(run_name, numeric_method)
        self.setUpPlot()
        self.checkPossibleEnergytests()

        setSvgLatexMatplotlibSettings(pyplot=plt)

        if(comparison_name == ""):
            self.singleResult()
        else:
            self.comparisonResult(comparison_name, numeric_method)

    def testTerminalMessage(self):
        print("Can only run test 1 if no fiction, neighbor springs, \
            damper is activated and the pad behaves as it's fastened to the roof surface. ")
        print("Can only run test 2 if no friction, stationary springs \
            damper is activated and the pad behaves as it's fastened to the roof surface. ")
        quit()

    def singleResult(self):
        if(self.only_pulling_springs):
            print("Only pullings springs in input.")
            energy = self.onlyPullingSpringsEnergyTest(self.block_velocity, self.block_position)
        elif(self.only_neighbor_springs):
            print("Only neighbor springs in input.")
            energy = self.onlyNeighborSpringsEnergyTest(self.block_velocity, self.block_position)
        else:
            self.testTerminalMessage()

        self.plotEnergyTest(energy, zoom=False)
        self.plotEnergyTest(energy)

    def comparisonResult(
        self,
        comparison_name,
        numeric_method
    ):
        self.loadComparisonFiles(comparison_name, numeric_method)
        self.setUpPlotComparison()
        if(self.only_pulling_springs):
            energy = self.onlyPullingSpringsEnergyTest(self.block_velocity, self.block_position)
            energy_comparison = self.onlyPullingSpringsEnergyTest(self.block_velocity_comparison, self.block_position_comparison)
        elif(self.only_neighbor_springs):
            energy = self.onlyNeighborSpringsEnergyTest(self.block_velocity, self.block_position)
            energy_comparison = self.onlyNeighborSpringsEnergyTest(self.block_velocity_comparison, self.block_position_comparison)
        else:
            self.testTerminalMessage()

        self.plotEnergyTestComparison(energy, energy_comparison, zoom=False)
        self.plotEnergyTestComparison(energy, energy_comparison)

    def loadFiles(self, run_name, numeric_method):
        self.results_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../results/"

        self.run_parameters = loadYaml(run_name)

        self.block_position = loadFile(run_name, self.run_parameters, "block_position")
        self.block_velocity = loadFile(run_name, self.run_parameters, "block_velocity")

    def loadComparisonFiles(self, comparison_name, numeric_method):
        print("Remember, this code assumes that the only difference between the two runs is the stepsize.")

        self.block_position_comparison = loadFile(comparison_name, self.run_parameters, "block_position")
        self.block_velocity_comparison = loadFile(comparison_name, self.run_parameters, "block_velocity")

    def setUpPlot(self):

        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]

        max_time = self.run_parameters["Parameters"]["max_time"]

        if(len(self.block_position.shape) == 1):
            self.one_block_boolean = True
            self.x = np.linspace(start=start, stop=stop, num=len(self.block_position))

            self.step_size = max_time/len(self.block_position)
        else:
            self.x = np.linspace(start=start, stop=stop, num=len(self.block_position[0]))
            self.step_size = max_time/len(self.block_position[0])

    def setUpPlotComparison(self):
        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]

        max_time = self.run_parameters["Parameters"]["max_time"]

        if(len(self.block_position_comparison.shape) == 1):
            self.one_block_boolean_comparison=True
            self.x_comparison = np.linspace(start=start, stop=stop, num=len(self.block_position_comparison))
            self.step_size_comparison = max_time/len(self.block_position_comparison)
        else:
            self.x_comparison = np.linspace(start=start, stop=stop, num=len(self.block_position_comparison[0]))
            self.step_size_comparison = max_time/len(self.block_position_comparison[0])
            print(max_time, len(self.block_position_comparison[0]), self.step_size_comparison)


    def checkPossibleEnergytests(self):
        self.checkOnlyPullingEnergyTest()
        self.checkOnlyNeighborEnergyTest()

    def checkOnlyPullingEnergyTest(self):
        no_friction = self.run_parameters["Debug"]["debug_no_friction"]
        no_neighbor_springs = self.run_parameters["Debug"]["debug_no_neighbor_springs"]
        no_pad = self.run_parameters["Debug"]["debug_no_pad"]

        self.only_pulling_springs = all(value == True for value in \
            [no_friction, no_neighbor_springs, no_pad])

    def checkOnlyNeighborEnergyTest(self):
        no_friction = self.run_parameters["Debug"]["debug_no_friction"]
        no_stationary_springs = self.run_parameters["Debug"]["debug_no_stationary_springs"]
        no_damper = self.run_parameters["Debug"]["debug_no_damper"]
        no_pad = self.run_parameters["Debug"]["debug_no_pad"]
        if(no_stationary_springs and no_damper and not no_pad):
            no_pad = True

        self.only_neighbor_springs = all(value == True for value in \
            [no_friction, no_stationary_springs, no_pad])

    def onlyPullingSpringsEnergyTest(self, block_velocity, block_position):

        N = self.run_parameters["Parameters"]["N"]
        m_scale_mass = self.run_parameters["Parameters"]["m_scale_mass"]
        m_mass_x = self.run_parameters["Parameters"]["m_mass_x"]
        m_scale_P = self.run_parameters["Parameters"]["m_scale_P"]
        m_k_P0 = self.run_parameters["Parameters"]["m_k_P0"]

        m_u = m_scale_mass*m_mass_x/N
        k_p = m_scale_P*m_k_P0/N

        energy_only_pulling_springs_test = 1/2 * m_u * \
            np.sum(np.power(block_velocity, 2), axis=0) + \
            1/2 * k_p * np.sum(np.power(block_position, 2), axis=0)
        return(energy_only_pulling_springs_test)

    def onlyNeighborSpringsEnergyTest(self, block_velocity, block_position):
        N = self.run_parameters["Parameters"]["N"]
        m_scale_mass = self.run_parameters["Parameters"]["m_scale_mass"]
        m_mass_x = self.run_parameters["Parameters"]["m_mass_x"]
        m_scale_C = self.run_parameters["Parameters"]["m_scale_C"]
        m_k_P0 = self.run_parameters["Parameters"]["m_k_P0"]

        m_u = m_scale_mass*m_mass_x/N
        k_c = m_scale_C*m_k_P0*N

        last_energy_ledd = block_position[1:,] - block_position[:-1,]

        energy_only_neighbor_springs_test = 1/2 * m_u * np.sum(np.power(block_velocity, 2), axis=0) + \
            1/2 * k_c * np.sum(np.power(last_energy_ledd, 2), axis=0)

        return(energy_only_neighbor_springs_test)

    def calculateError(
        self,
        energy_array
    ):
        return energy_array[-1] - energy_array[0]

    def compareError(
        self,
        energy_test_result,
        energy_test_result_comparison
    ):
        error = self.calculateError(energy_test_result)
        error_comparison = self.calculateError(energy_test_result_comparison)

        ratio = error/error_comparison

        print(error, error_comparison, ratio)

    def plotEnergyTest(self, energy_test_result, zoom=True):
        plt.plot(self.x, energy_test_result, label="Energy",
                 color=colors_short[0],
                 linestyle=linestyles[0])

        if(not zoom):
            plt.ylim(0, np.max(energy_test_result)*(1.10))

        plt.ylabel("\$E\$")
        plt.xlabel("\$t\$")
        plt.legend()
        plt.show()

    def plotEnergyTestComparison(
        self,
        energy_test_result,
        energy_test_result_comparison,
        zoom=True
    ):
        self.compareError(energy_test_result, energy_test_result_comparison)
        plt.plot(self.x, energy_test_result,
                 label=str(self.step_size),
                 color=colors_short[2],
                 linestyle=linestyles[2])
        plt.plot(self.x_comparison, energy_test_result_comparison,
                 label=str(self.step_size_comparison),
                 color=colors_short[1],
                 linestyle=linestyles[1])

        if(not zoom):
            plt.ylim(0, np.max([np.max(energy_test_result), np.max(energy_test_result_comparison)])*(1.10))

        plt.ylabel("\$E\$")
        plt.xlabel("\$t\$")
        plt.legend(title="\$dt\$")
        plt.show()


def run():
    if(len(sys.argv) > 2):
        checkEnergy = CheckEnergy(sys.argv[1], sys.argv[2])
    elif(len(sys.argv) == 2):
        checkEnergy = CheckEnergy(sys.argv[1])
    else:
        checkEnergy = CheckEnergy()


if __name__ == "__main__":
    run()
