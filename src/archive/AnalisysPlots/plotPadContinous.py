from analysisSharedFunctions import loadYaml, loadFile
from plotSetUp import setSvgLatexMatplotlibSettings, \
    colors_qualitative_short as colors_short, \
    linestyles, \
    createTimeStepValues, \
    createSliderVelocityStepValues, \
    choseColourFromDirection, \
    checkStepAsContinous
import sys
from matplotlib import pyplot as plt


setSvgLatexMatplotlibSettings(plt)

if("-m" in sys.argv):
    multiplot_figure = plt.figure()
    axs = [multiplot_figure.add_subplot(pos) for pos in [221, 222, 223, 224]]


def plotVerticalStepLines(
    yaml_file,
    ax_number=0
):
    start = yaml_file["Parameters"]["slider_speed"]
    increment = yaml_file["Parameters"]["increment"]
    interval = yaml_file["Parameters"]["interval"]
    max_time = yaml_file["Parameters"]["max_time"]

    number_of_intervals = max_time // interval
    for i in range(0, number_of_intervals):
        if("-m" in sys.argv):
            axs[ax_number].axvline(x=start + increment * i,
                        linewidth=0.4,
                        color="grey",
                        alpha=0.5,
                        linestyle="-")
        else:
            plt.axvline(x=start + increment * i,
                        linewidth=0.4,
                        color="grey",
                        alpha=0.5,
                        linestyle="-")


def plot_multifigure(
    time_steps,
    pad_data,
    yaml_file,
    ax_number,
    y_label="\$x\$",
    x_label="\$t\$",
    label="",
    alpha=1.0,
    x_lim=[-0.1, 2.01],
    y_lim=[-1.6, 0.6],
    step_as_continous=False
):
    color_index = choseColourFromDirection(yaml_file)
    axs[ax_number].plot(time_steps, pad_data,
                        color=colors_short[color_index],
                        linestyle=linestyles[0],
                        label=label,
                        linewidth=0.6,
                        alpha=alpha
                        )

    if(step_as_continous):
        plotVerticalStepLines(yaml_file, ax_number)

    axs[ax_number].set_xlim(x_lim[0], x_lim[1])
    axs[ax_number].set_ylim(y_lim[0], y_lim[1])

    axs[ax_number].set_ylabel(y_label)
    axs[ax_number].set_xlabel(x_label)


def prettyPlotPad(
    time_steps,
    pad_data,
    yaml_file,
    y_label="\$x\$",
    x_label="\$t\$",
    label="",
    alpha=1.0,
    y_lim=[-1.6, 0.6],
    step_as_continous=False
):
    color_index = choseColourFromDirection(yaml_file)
    plt.plot(time_steps, pad_data,
             color=colors_short[color_index],
             linestyle=linestyles[0],
             label=label,
             linewidth=0.4,
             alpha=alpha
             )

    if(step_as_continous):
        plotVerticalStepLines(yaml_file)

    plt.ylim(y_lim[0], y_lim[1])

    plt.ylabel(y_label)
    plt.xlabel(x_label)


def plot_pad(
    run_name,
    alpha=1.0,
    label="Pad"
):
    yaml_file = loadYaml(run_name)
    pad_position = loadFile(run_name, yaml_file, file_name="pad_position")
    #pad_velocity = loadFile(run_name, yaml_file, file_name="pad_velocity")
    step_as_continous = checkStepAsContinous(yaml_file)
    time_steps = createSliderVelocityStepValues(pad_position, yaml_file,
                                                step_as_continous)


    prettyPlotPad(time_steps, pad_position, yaml_file,
                  x_label="\$\\nu\$", alpha=alpha,
                  label=label, step_as_continous=step_as_continous)

    if("-m" in sys.argv):
        x_lims = [[-0.01, 0.15], [0.29, 0.45], [0.65, 0.81], [1.07, 1.23]]
        for i, x_lim in enumerate(x_lims):
            plot_multifigure(time_steps, pad_position, yaml_file,
                x_label="\$\\nu\$", ax_number=i, x_lim=x_lim, alpha=alpha,
                step_as_continous=step_as_continous)

def plot_single_pad(
    run_name
):
    plot_pad(run_name)
    plt.legend()
    plt.show()


def plot_compare_pads(
    run_names
):
    labels = ["Increasing", "Decreasing"]
    alphas = [1.0, 0.8]
    for i, run_name in enumerate(run_names):
        plot_pad(run_name, alpha=alphas[i], label=labels[i])
    plt.legend()
    plt.show()



def run():
    if(len(sys.argv) == 2):
        plot_single_pad(sys.argv[1])
    elif(len(sys.argv) > 2):
        if("-m" in sys.argv and "-s" in sys.argv):
            plot_compare_pads(sys.argv[1:-2])
        elif("-m" in sys.argv or "-s" in sys.argv):
            plot_compare_pads(sys.argv[1:-1])
        else:
            plot_compare_pads(sys.argv[1:])


if __name__ == "__main__":
    run()
