from analysisSharedFunctions import loadYaml, loadFile, \
                                    loadPhaseFile, fourierSpectrum
from plotSetUp import setSvgLatexMatplotlibSettings, \
    colors_qualitative_short as colors_short, \
    colors_diverging_long as colors_long, \
    linestyles, \
    createTimeStepValues, \
    createSliderVelocityStepValues
import sys
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import PolyCollection
from matplotlib import colors
#from matplotlib import cm


setSvgLatexMatplotlibSettings(plt)


def choseColourFromDirection(
    yaml_file
):
    start_speed_continuous = yaml_file["Parameters"]["start_speed_continuous"]
    end_speed_continuous = yaml_file["Parameters"]["end_speed_continuous"]

    direction = end_speed_continuous - start_speed_continuous

    if(direction < 0):
        return 3
    else:
        return 4


def prettyPlotPad(
    time_steps,
    pad_data,
    yaml_file,
    y_label="\$x\$",
    x_label="\$t\$",
    alpha=1.0
):
    color_index = choseColourFromDirection(yaml_file)
    plt.plot(time_steps, pad_data,
             color=colors_short[color_index],
             linestyle=linestyles[0],
             label="Pad",
             linewidth=0.4,
             alpha=alpha
             )

    plt.ylim(-1.6, 0.6)
    plt.ylabel(y_label)
    plt.xlabel(x_label)


def plot3D(
    run_name
):
    import numpy as np
    from matplotlib import cm
    yaml_file = loadYaml(run_name)
    pad_position = loadFile(run_name, yaml_file, file_name="pad_position")
    start_velocity = yaml_file["Parameters"]["slider_speed"]
    increment = yaml_file["Parameters"]["increment"]

    zs = range(20)
    verts = []
    amplitudes = []
    frequencies = []
    #plt.figure()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(0, 150):
        #plt.subplot(121)
        phase_data = loadPhaseFile(run_name, "pad_phase_" + str(i))

        fourier_dictionary = fourierSpectrum(run_name, phase_data[0])
        amount_data_half = len(phase_data[0])//5
        verts.append(list(zip(fourier_dictionary['x_values'], fourier_dictionary['y_values'])))
        amplitudes.append(fourier_dictionary['y_values'][1:])
        frequencies.append(fourier_dictionary['x_values'][1:])
    #     plt.plot(fourier_dictionary['x_values'], label=str(i))
    # plt.legend()
    # plt.show()


    cax = ax.matshow(amplitudes, cmap="ocean_r",
                     extent=[frequencies[0][0], frequencies[0][-1], 1.49, 0.0])
    ax.set_xlabel("\$f\$")
    ax.set_ylabel("\$\\nu\$")
    cbar = fig.colorbar(cax)
    cbar.set_label("\$A\$", rotation=270)
    plt.show()


def plotPhase3D(
    run_name
):
    import numpy as np
    from matplotlib import cm
    yaml_file = loadYaml(run_name)
    pad_position = loadFile(run_name, yaml_file, file_name="pad_position")
    start_velocity = yaml_file["Parameters"]["slider_speed"]
    increment = yaml_file["Parameters"]["increment"]

    zs = range(1500)
    verts = []
    amplitudes = []
    frequencies = []
    #plt.figure()

    jet = plt.get_cmap('viridis')
    cNorm  = colors.Normalize(vmin=0, vmax=1.49)
    scalarMap = cm.ScalarMappable(norm=cNorm, cmap=jet)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    for i in range(0, 1500):
        velocity = start_velocity+increment*i
        phase_data = loadPhaseFile(run_name, "pad_phase_" + str(i))
        alpha = 0.7
        ax.plot(phase_data[0], phase_data[1], alpha=alpha,
                zs=(velocity), color=scalarMap.to_rgba(velocity))

    ax.view_init(elev=2.5, azim=-45)
    plt.rcParams["savefig.bbox"] = "tight"
    ax.set_ylabel("\$x\$")
    ax.set_xlabel("\$\dot{x}\$")
    ax.set_zlabel("\$\\nu\$")
    ax.set_yticks(np.arange(-0.6,0.6,0.3))
    plt.show()
    plt.rcParams["savefig.bbox"] = "standard"


def prettyPlotPhase(
    phase_data,
    y_label="\$x\$",
    x_label="\$\\dot{x}\$",
    alpha=1.0,
    color_index=0,
    line_style_index=0,
    label=""
):
    plt.plot(phase_data[1], phase_data[0],
             color=colors_short[color_index],
             linestyle=linestyles[line_style_index],
             label=label,
             linewidth=0.9,
             alpha=alpha
             )
    plt.ylabel(y_label)
    plt.xlabel(x_label)


def prettyPlotFourier(
    fourier_dictionary,
    y_label="\$A\$",
    x_label="\$f\$",
    alpha=1.0,
    color_index=0,
    line_style_index=0,
    label=""
):

    plt.plot(fourier_dictionary["x_values"][1:],
             fourier_dictionary["y_values"][1:],
             color=colors_short[color_index],
             linestyle=linestyles[line_style_index],
             label=label)
    #plt.xlim(0.0, 2.0)
    plt.ylabel(y_label)
    plt.xlabel(x_label)


def plotPad(
    run_name,
    alpha=1.0
):
    yaml_file = loadYaml(run_name)
    pad_position = loadFile(run_name, yaml_file, file_name="pad_position")
    #pad_velocity = loadFile(run_name, yaml_file, file_name="pad_velocity")
    time_steps = createSliderVelocityStepValues(pad_position, yaml_file)

    prettyPlotPad(time_steps, pad_position, yaml_file,
                  x_label="\$\\nu\$", alpha=alpha)


def plotPhase(
    run_name
):
    yaml_file = loadYaml(run_name)
    #pad_position = loadFile(run_name, yaml_file, file_name="pad_position")
    start_velocity = yaml_file["Parameters"]["slider_speed"]
    increment = yaml_file["Parameters"]["increment"]

    slider_velocities = [8, 11, 67, 72, 111, 130]
    phase_labels = [str(round(abs(increment*velocity),2)) for velocity in slider_velocities]
    if(increment < 0):
        slider_velocities = [int(149 - slider_velocity) for slider_velocity in slider_velocities]

    plt.figure()
    for i, velocity in enumerate(slider_velocities):
        phase_data = loadPhaseFile(run_name, "pad_phase_" + str(velocity))
        plt.subplot(121)
        prettyPlotPhase(phase_data, color_index=-1,
                        line_style_index=(i%4),
                        label=phase_labels[i])
        fourier_dictionary = fourierSpectrum(run_name, phase_data[0])
        plt.subplot(122)
        prettyPlotFourier(fourier_dictionary,
                          color_index=-1,
                          line_style_index=(i%4),
                          label="Pad"
                          )
        plt.subplot(121)
        plt.grid()
        plt.ylim(-0.6, 0)
        #plt.ylim(-0.6, -0.5)
        plt.xlim(-0.1, 0.1)
        #plt.xlim(-0.05, 0.05)
        plt.subplot(122)
        plt.grid()
        plt.ylim(-0.01, 0.1)
        #plt.ylim(-0.001, 0.02)
        plt.xlim(-0.01, 4)
        #plt.xlim(-0.01, 2)
        plt.legend()
        plt.suptitle("\$\\nu=\$" + phase_labels[i])
        plt.show()


def plotFourier(
    run_name
):
    yaml_file = loadYaml(run_name)
    #pad_position = loadFile(run_name, yaml_file, file_name="pad_position")
    start_velocity = yaml_file["Parameters"]["slider_speed"]
    increment = yaml_file["Parameters"]["increment"]

    plt.figure()
    for i in range(0, 150):
        phase_data = loadPhaseFile(run_name, "pad_phase_" + str(i))
        fourier_dictionary = fourierSpectrum(run_name, phase_data[0])
        prettyPlotFourier(fourier_dictionary,
                          color_index=(i%11),
                          line_style_index=(i%4))
        plt.suptitle("\$\\nu=\$" + str(start_velocity + i*increment))
        #plt.legend()
    plt.xlim(0,1.5)
    plt.show()



def plotComparePads(
    run_names
):
    for run_name in run_names:
        plotPad(run_name, alpha=0.7)
    plt.show()



def run():
    if(len(sys.argv) == 2):
        #plotPhase(sys.argv[1])
        #plotFourier(sys.argv[1])
        #plot3D(sys.argv[1])
        plotPhase3D(sys.argv[1])
    elif(len(sys.argv) > 2):
        plotComparePads(sys.argv[1:])


if __name__ == "__main__":
    run()
