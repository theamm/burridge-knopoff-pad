import numpy as np
from matplotlib import pyplot as plt
import scipy.fftpack

import sys
import os
from plotSetUp import setSvgLatexMatplotlibSettings
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + "/..")

from helpFunctions import LoadFile
from plotSetUp import colors_diverging_short

loader = LoadFile()
setSvgLatexMatplotlibSettings(pyplot=plt)
#NB! this is a spike, which in non-extreme-programming terms is a sketch/demo of some implementation

def load_csv_data(
    run_name,
    output_type,
    use_shared_names=False
):
    if("-frictionphase"):
        file_name = "/results/friction_phase/" + output_type
    else:
        file_name = output_type
    parameters = loader.load_yaml_parameters(run_name)
    phase_data = loader.load_simulation_output(
        parameters,
        run_name,
        file_name,
        use_shared_names=use_shared_names
    )
    return phase_data


def make_sin_curve(
    data,
    amplitude=1,
    period=1,
    phase_shift=1,
    vertical_shift=0
):
    return ((amplitude * np.sin(period * 2*np.pi * data + phase_shift)) + vertical_shift)


def calculate_fourier_spectrum(
    data,
    suptitle=""
):
    # plt.plot(data)
    # plt.show()

    sample_points = len(data)
    sample_spacing = 0.5

    x_range = np.linspace(0.0, sample_points*0.005, sample_points)

    y_values = scipy.fftpack.fft(data)


    x_values = np.linspace(0.0, 1.0/sample_spacing, sample_points/2)

    y_values = 2.0/sample_points*np.abs(y_values[:sample_points//2])
    print(y_values)
    plt.plot(x_values[1:], y_values[1:])
    plt.xlim(0.0, 2.0)
    plt.suptitle(suptitle)
    # plt.show()
    # plt.plot(x_values, y_values)
    # plt.xlim(0.0, 2.0)
    # plt.show()


def test_scipy_fftpack(
):
    sample_points = 800
    sample_spacing = 1/800.0
    max_value = sample_points * sample_spacing
    x_range = np.linspace(0, max_value, num=sample_points)
    sine_range = make_sin_curve(x_range, 1.0, 50.0, 0.0, 0.0)
    sine_range_2 = make_sin_curve(x_range, 0.5, 80.0, 0, 0)

    x_values = np.linspace(0.0, 1.0/(2.0*sample_spacing), sample_points/2)
    plt.plot(x_range, sine_range + sine_range_2, color="#7fcdbb", linewidth=2.0)
    plt.xlabel("\$x\$")
    plt.ylabel("\$g(x)=\sin(50*x)+0.5*\sin(80x)\$")
    plt.show()

    y_values = scipy.fftpack.fft(sine_range + sine_range_2)

    y_values = 2.0/sample_points * np.abs(y_values[:sample_points//2])
    print(y_values)
    plt.plot(x_values, y_values, color="#7fcdbb", linewidth=2.0)
    plt.ylabel("\$A\$")
    plt.xlabel("\$f\$")
    plt.show()


def make_fourier_spectrum(
    run_name,
    output_name="pad_phase_"
):
    for i in range(100, 120):
        loaded_data_pad = load_csv_data(run_name, output_name + str(i))
        position_data_pad = loaded_data_pad[0]

        loaded_data_block = load_csv_data(run_name, "block_phase_" + str(i))
        position_data_block_1 = loaded_data_block[0]
        # position_data_block_6 = loaded_data_block[2]
        # position_data_block_7 = loaded_data_block[4]
        # position_data_block_50 = loaded_data_block[6]

        #calculate_fourier_spectrum(position_data_pad, suptitle = str(i))
        #plt.show()
        calculate_fourier_spectrum(-position_data_block_1)
        #calculate_fourier_spectrum(position_data_block_6, suptitle=str(i))
        # calculate_fourier_spectrum(position_data_block_7)
        # calculate_fourier_spectrum(position_data_block_50)

    plt.xlim(0,0.4)
    plt.show()

    test_scipy_fftpack()

def run(
    run_name
):
    make_fourier_spectrum(run_name)


if __name__ == "__main__":
    if(len(sys.argv) > 1):
        run_name = sys.argv[1]
    run(run_name)
