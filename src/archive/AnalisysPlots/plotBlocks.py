from analysisSharedFunctions import loadYaml, loadFile, \
                                    loadPhaseFile, fourierSpectrum
from plotSetUp import setSvgLatexMatplotlibSettings, \
    colors_qualitative_short as colors_short, \
    colors_qualitative_long as colors_long, \
    linestyles, \
    checkIfMultipleBlocks, \
    createTimeStepValues
import sys
from matplotlib import pyplot as plt
import numpy as np



setSvgLatexMatplotlibSettings(plt)


def prettyPlotBlocks(
    time_steps,
    block_data,
    yaml_file,
    legend_title="",
    legend_loc="lower right",
    y_label="",
    x_label="\$t\$"
):
    multiple_blocks = checkIfMultipleBlocks(block_data)
    print(multiple_blocks)
    print(block_data.shape)
    if(multiple_blocks):
        for i, block in enumerate(block_data):
            plt.plot(time_steps, block,
                     color=colors_short[i % len(colors_short)],
                     linestyle=linestyles[i % len(linestyles)],
                     label="Block: " + str(i)
                     ) 
    else:
        plt.plot(time_steps, block_data,
                 color=colors_short[0 % len(colors_short)],
                 linestyle=linestyles[0 % len(linestyles)],
                 label="Block: " + str(0)
                 )
    plt.legend(title=legend_title, loc=legend_loc)
    plt.ylabel(y_label)
    plt.xlabel(x_label)
    plt.show()


def prettyPlotPhase(
    phase_data,
    y_label="\$x\$",
    x_label="\$\\dot{x}\$",
    alpha=1.0,
    color_index=0,
    line_style_index=0,
    label=""
):
    plt.plot(phase_data[1], phase_data[0],
             color=colors_long[color_index],
             linestyle=linestyles[line_style_index],
             label=label,
             linewidth=0.7,
             alpha=alpha
             )
    plt.ylabel(y_label)
    plt.xlabel(x_label)


def prettyPlotFourier(
    fourier_dictionary,
    y_label="\$A\$",
    x_label="\$f\$",
    alpha=1.0,
    color_index=0,
    line_style_index=0,
    label=""
):

    plt.plot(fourier_dictionary["x_values"][1:],
             fourier_dictionary["y_values"][1:],
             color=colors_long[color_index],
             linestyle=linestyles[line_style_index],
             label=label)
    #plt.xlim(0.0, 2.0)
    plt.ylabel(y_label)
    plt.xlabel(x_label)


def plot_blocks(
    run_name
):
    yaml_file = loadYaml(run_name)
    block_positions = loadFile(run_name, yaml_file,
                               file_name="block_position")
    block_velocities = loadFile(run_name, yaml_file,
                                file_name="block_velocity")
    time_steps = createTimeStepValues(block_positions, yaml_file)

    prettyPlotBlocks(time_steps, block_positions, yaml_file,
                     y_label="\$u_j\$")
    prettyPlotBlocks(time_steps, block_velocities, yaml_file,
                     y_label="\$\dot{u_j}\$")
    for i in range(0, len(block_positions)):
            plt.plot(block_velocities[i], block_positions[i], label = "Block: " + str(i))
#    prettyPlotBlocks(block_positions, np.transpose(block_velocities), yaml_file,
   # y_label="\$\dot{u_j}\$")
    plt.show()


def plot_phase(
    run_name
):
    yaml_file = loadYaml(run_name)
    increment = yaml_file["Parameters"]["increment"]
    blocks = yaml_file["Parameters"]["blocks"]
    print(blocks)
    slider_velocities = [8, 11, 67, 72, 111, 130]
    phase_labels = [str(round(abs(increment*velocity),2)) for velocity in slider_velocities]
    if(increment < 0):
        slider_velocities = [int(149 - slider_velocity) for slider_velocity in slider_velocities]
    plt.figure()
    for i, velocity in enumerate(slider_velocities):
        phase_data = loadPhaseFile(run_name, "block_phase_" + str(velocity))

        for j in range(0,len(phase_data)//2):
            # time_steps = createTimeStepValues(phase_data[0], yaml_file)
            # prettyPlotBlocks(time_steps, phase_data[j*2], yaml_file,
            #                  y_label="\$u_j\$")
            plt.subplot(121)
            prettyPlotPhase(phase_data[[2*j,2*j+1]], color_index=(j%11),
                            line_style_index=(j%4),
                            label=str(blocks[i])
                            )
            # plt.show()
            fourier_dictionary = fourierSpectrum(run_name, phase_data[2*j])
            plt.subplot(122)
            prettyPlotFourier(fourier_dictionary,
                              color_index=(j%11),
                              line_style_index=(j%4),
                              label=str(blocks[j])
                              )

        plt.subplot(121)
        plt.grid()
        plt.ylim(-3, 2)
        #plt.ylim(-2, 1)
        plt.xlim(-3, 3)
        #plt.xlim(-1, 3)
        plt.subplot(122)
        plt.grid()
        plt.ylim(-0.01, 1.9)
        #plt.ylim(-0.01, 0.5)
        plt.xlim(-0.01, 4)
        #plt.xlim(-0.01, 2)
        plt.legend(title="Block:")
        plt.suptitle("\$\\nu=\$" + phase_labels[i])
        #plt.xlim(0,3.5)
        plt.show()


def run():
    if(len(sys.argv) > 1):
        plot_blocks(sys.argv[1])
        plot_phase(sys.argv[1])


if __name__ == "__main__":
    run()
