import sys
import os

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))
from matplotlib import pyplot as plt
import numpy as np

import matplotlib as mpl

mpl.rcParams["svg.fonttype"] = "none"
mpl.rcParams["font.size"] = 16


mpl.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
mpl.rc("text", usetex=True)

from helpFunctions import LoadFile

loader = LoadFile()

store_data = []


def velocity_x_values(parameters, n_data_points):
    if loader.get_debug_parameter(parameters, "debug_continuous_slider_speed"):
        if loader.get_parameter(parameters, "interval") is 0:
            speeds = loader.get_start_end_speed(parameters)
            max_time = loader.get_parameter(parameters, "max_time")
            step_size = loader.get_parameter(parameters, "dt")
            save_interval_dt = loader.get_parameter(parameters, "save_interval_dt")
            speed_difference = speeds["end_speed"] - speeds["start_speed"]
            n_steps = max_time / step_size
            step_speed_increment = speed_difference / n_steps
            end_speed = speeds["end_speed"] - step_speed_increment * (
                save_interval_dt - 1
            )
            print(step_size, save_interval_dt, n_data_points, end_speed)
            return np.linspace(speeds["start_speed"], end_speed, num=n_data_points)


def direction_label(parameters):
    speeds = loader.get_start_end_speed(parameters)
    if speeds["start_speed"] > speeds["end_speed"]:
        speed_direction = "decreasing"
    else:
        speed_direction = "increasing"

    return speed_direction


def save_interval_label(paramters):
    save_interval = str(loader.get_parameter(paramters, "save_interval_dt"))
    return save_interval


def plot_run(run_name):
    parameters = loader.load_yaml_parameters(run_name)
    data = loader.load_simulation_output(parameters, run_name, "pad_friction")
    store_data.append(data)
    x_values = velocity_x_values(parameters, len(data))
    print(len(data))
    print(x_values)
    print("heeellllo: ", data[-1], x_values[-1])
    label = direction_label(parameters)
    label = save_interval_label(parameters)
    plt.plot(x_values, data, label=label)
    plt.xlabel("surface velocity")
    plt.ylabel("pad position")


def plot_runs(run_list):
    for run_name in run_list:
        plot_run(run_name)
    plt.legend(title="Save every:")
    plt.show()


def check_data_temp():
    diff_data = []
    counter = 0
    for point in store_data[0]:
        diff_data.append(point - store_data[1][2 * counter])
        counter += 1
    plt.plot(diff_data)
    plt.show()
    print(sum(diff_data))


def run():
    if len(sys.argv) > 2:
        run_name = sys.argv[1:]
        plot_runs(run_name)
        # check_data_temp()
    elif len(sys.argv) == 2:
        plot_runs([sys.argv[1]])
    else:
        print(
            'No input given, please provide one or more paths within the "results" folder'
        )


if __name__ == "__main__":
    run()
