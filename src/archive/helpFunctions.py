import os
import sys

from matplotlib import pyplot as plt

import numpy as np

import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class LoadFile:
    def __init__(self):
        self.loadedYaml = False
        self.results_path = (
            os.path.dirname(os.path.realpath(__file__)) + "/../../results/"
        )
        self.numeric_method = "_midpoint_"

    def set_results_path(self, results_path):
        self.results_path = results_path

    def set_numeric_method(self, numeric_method):
        self.numeric_method = numeric_method

    def load_file(self, filename):
        print("Filename:", filename)
        return np.loadtxt(filename, delimiter=",")

    def load_simulation_output(
        self, yaml_parameters, run_name, output_name, use_shared_names=True
    ):
        numeric_method = ""
        if use_shared_names:
            shared_names = self.get_parameter(yaml_parameters, "file_name")
            numeric_method = self.numeric_method
        else:
            shared_names = ""
        path_to_file = (
            self.results_path
            + run_name
            + "/"
            + shared_names
            + numeric_method
            + output_name
            + ".csv"
        )
        return self.load_file(path_to_file)

    def get_simulation_path(self, run_name=""):
        return self.results_path + run_name

    def check_if_yaml_has_been_loaded(self):
        if self.loadedYaml:
            print("Some yaml has been loaded before with this instance-loader.")
        else:
            print("There has not been a yaml loaded before on this loader.")

    def load_yaml_parameters(self, run_name):

        self.loadedYaml = True
        stream = open(self.results_path + run_name + "/parameters.yaml", "r")
        return yaml.load(stream, Loader=Loader)

    def loadYaml(self, run_name, file_name):
        stream = open(self.results_path + run_name + file_name, "r")
        return yaml.load(stream, Loader=Loader)

    def get_parameter(self, run_parameters, parameter_name):
        return run_parameters["Parameters"][parameter_name]

    def get_debug_parameter(self, run_parameters, debug_parameter_name):
        return run_parameters["Debug"][debug_parameter_name]

    def get_start_end_speed(self, parameters):
        start_speed = self.get_parameter(parameters, "start_speed_continuous")
        end_speed = self.get_parameter(parameters, "end_speed_continuous")
        return {"start_speed": start_speed, "end_speed": end_speed}

    def check_if_file_exist_in_run_folder(self, run_name, file_name=""):
        if os.path.isfile(self.results_path + run_name + file_name):
            return True
        else:
            return False

    def save_csv(self, run_name, file_name, array_or_matrix):
        file_path = self.results_path + run_name + file_name
        print("Saving to path: ", file_path)
        np.savetxt(file_path, array_or_matrix, delimiter=",")
