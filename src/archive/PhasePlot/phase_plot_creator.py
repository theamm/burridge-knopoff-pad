from phase_plot_imports import loader, np, sys


def create_phase_data(run_name, pad_phase_data=True, block_phase_data=True):
    parameters = loader.load_yaml_parameters(run_name)
    threshold_dictionary = get_threshold_speed(parameters)
    start_index = threshold_dictionary["threshold_speed_start_index"]
    indexes = make_indexes(start_index, threshold_dictionary, 100)
    if block_phase_data:
        path_in_results_folder = run_name + "/phase_plot_at_threshold_block.csv"
        position = loader.load_simulation_output(parameters, run_name, "block_position")
        velocity = loader.load_simulation_output(parameters, run_name, "block_velocity")
        phase_data = get_phase_selection(position, velocity, indexes)
        save_phase_data(
            phase_data, run_name, path_in_results_folder, block_phase_data=True
        )
    if pad_phase_data:
        path_in_results_folder = run_name + "/phase_plot_at_threshold_pad.csv"
        position = loader.load_simulation_output(parameters, run_name, "pad_position")
        velocity = loader.load_simulation_output(parameters, run_name, "pad_velocity")
        phase_data = get_phase_selection(position, velocity, indexes)
        save_phase_data(phase_data, run_name, path_in_results_folder)


def load_phase_data_creator(run_name, phase_part="_block"):
    path_in_results_folder = (
        run_name + "/phase_plot_at_threshold" + phase_part + ".csv"
    )
    if loader.check_if_file_exist_in_run_folder(
        path_in_results_folder
    ) and pad_or_block(phase_part):
        print("Found phase plot at: ", path_in_results_folder)

        parameters = loader.load_yaml_parameters(run_name)
        phase_data = load_phase_data(run_name, phase_part)
        if "_block" == phase_part:
            position_selection = []
            velocity_selection = []
            for i in range(0, int(len(phase_data) / 2)):
                position_selection.append(phase_data[2 * i])
                velocity_selection.append(phase_data[2 * i + 1])
        else:
            position_selection = phase_data[0]
            velocity_selection = phase_data[1]
        return {
            "position_selection": position_selection,
            "velocity_selection": velocity_selection,
            "parameters": parameters,
        }
    else:
        print(
            'Sorry, the phase plot does not exist for this run.\n \
            Remove the "-load"-flag to produce it.'
        )
        exit()


def get_threshold_speed(parameters):
    threshold_speed = loader.get_parameter(parameters, "threshold_speed")
    speeds = loader.get_start_end_speed(parameters)
    max_time = loader.get_parameter(parameters, "max_time")
    save_interval = loader.get_parameter(parameters, "save_interval_dt")
    dt = loader.get_parameter(parameters, "dt")
    steps_in_time_unit = 1 / (dt * save_interval)
    n_steps = np.floor(max_time * steps_in_time_unit)

    threshold_speed_start_index = int(
        np.ceil(
            (threshold_speed - speeds["start_speed"])
            / (speeds["end_speed"] - speeds["start_speed"])
            * n_steps
        )
    )
    return {
        "threshold_speed": threshold_speed,
        "threshold_speed_start_index": threshold_speed_start_index,
        "start_speed": speeds["start_speed"],
        "end_speed": speeds["end_speed"],
        "n_steps": n_steps,
        "steps_in_time_unit": steps_in_time_unit,
    }


def make_indexes(start_index, threshold_dictionary, time_units_from_start=1000):
    start_index = start_index + make_indexes_within_interval(
        start_index, threshold_dictionary, 50
    )
    max_indexes_from_start = (
        threshold_dictionary["n_steps"]
        - threshold_dictionary["threshold_speed_start_index"]
    )
    if start_index == max_indexes_from_start:
        print("Can't have the start index be equal to the end index.")
        quit()
    end_index = start_index + make_indexes_within_interval(
        start_index, threshold_dictionary, time_units_from_start
    )
    print(start_index, end_index)
    # start_index += 5000
    indexes = range(start_index, end_index)
    return indexes


def make_indexes_within_interval(
    start_index, threshold_dictionary, time_units_from_start
):
    indexes_from_start = np.floor(
        time_units_from_start * threshold_dictionary["steps_in_time_unit"]
    )
    max_indexes_from_start = (
        threshold_dictionary["n_steps"]
        - threshold_dictionary["threshold_speed_start_index"]
    )
    if indexes_from_start > max_indexes_from_start:
        print(
            "Selected units greater than the size of the run. Using maximum amount of units."
        )
        print("Selected: ", time_units_from_start)
        print(
            "Maximum: ",
            max_indexes_from_start / threshold_dictionary["steps_in_time_unit"],
        )
        indexes_from_start = max_indexes_from_start
    return int(indexes_from_start)


def check_if_all_phase_data_exists(run_name, check="both"):
    found_block = False
    found_pad = False
    if check in ["block", "both"]:
        path_in_results_folder = run_name + "/phase_plot_at_threshold_block.csv"
        if not loader.check_if_file_exist_in_run_folder(path_in_results_folder):
            return False
        else:
            found_block = True
    if check in ["pad", "both"]:
        path_in_results_folder = run_name + "/phase_plot_at_threshold_pad.csv"
        if not loader.check_if_file_exist_in_run_folder(path_in_results_folder):
            return False
        else:
            found_pad = True
    if found_block and found_pad:
        return True
    else:
        print("check-parameters must be one of [pad, block, both].")
        return False


def save_phase_data(
    phase_data, run_name, path_in_results_folder, block_phase_data=False
):
    position_selection = phase_data["position"]
    velocity_selection = phase_data["velocity"]
    write_to_file = True
    file_name = "/phase_plot_at_threshold_"
    if (
        loader.check_if_file_exist_in_run_folder(path_in_results_folder)
        and not "-yes" in sys.argv
    ):
        write_to_file = bool_interaction_with_user(
            'Phase file exists, to overwrite type"yes" or anything elso to skip:'
        )
    if write_to_file:
        if block_phase_data and len(position_selection) > 1:
            save_matrix = []
            for i in range(0, len(position_selection)):
                save_matrix.append(position_selection[i])
                save_matrix.append(velocity_selection[i])
            save_matrix = np.array(save_matrix)
            file_name += "block.csv"
        else:
            save_matrix = np.array([position_selection, velocity_selection])
            file_name += "pad.csv"
        loader.save_csv(run_name, file_name, save_matrix)
    else:
        print("Skipped printing to file.")


def bool_interaction_with_user(output_to_user):
    response_from_user = input(output_to_user)
    if response_from_user == "yes":
        return True
    else:
        return False


def get_phase_selection(position, velocity, indexes):
    if len(position.shape) == 1:
        position_selection = position[indexes]
        velocity_selection = velocity[indexes]
    else:
        position_selection = position[:, indexes]
        velocity_selection = velocity[:, indexes]
    return {"position": position_selection, "velocity": velocity_selection}


def pad_or_block(phase_part):
    return phase_part in ["_pad", "_block"]


def load_phase_data(run_name, phase_part=""):
    phase_data = lazy_load_data(
        run_name, "phase_plot_at_threshold" + phase_part, use_shared_names=False
    )
    return phase_data


def lazy_load_data(run_name, file_name, use_shared_names):
    parameters = loader.load_yaml_parameters(run_name)
    phase_data = loader.load_simulation_output(
        parameters, run_name, file_name, use_shared_names=use_shared_names
    )
    return phase_data
