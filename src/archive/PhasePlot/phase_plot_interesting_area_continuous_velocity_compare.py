from phase_plot_imports import *

import re

from phase_plot_main import get_saved_phase_data, create_and_get_phase_data

# This script only makes sence with continuously increasing (or decreasing) slider velocity.

def plot_phase_single(run_names):
    for run_name in run_names:
        parameters = loader.load_yaml_parameters(run_name)
        if "-load" in sys.argv:
            phase_data = get_saved_phase_data(run_name)
        else:
            phase_data = create_and_get_phase_data(run_name=run_name)

        threshold_speed = loader.get_parameter(parameters, "threshold_speed")
        print(threshold_speed)
        plt.plot(
            phase_data["position_selection"],
            phase_data["velocity_selection"],
            label=threshold_speed,
        )
    plt.legend(title="Threshold speed")
    plt.show()


def filter_out_run_names(input_from_user):
    pattern = re.compile("^(!?-)\w+$")
    run_names = []
    for i in range(1, len(input_from_user)):
        matching_strings = re.match(pattern, input_from_user[i])
        if matching_strings == None:
            run_names.append(input_from_user[i])

    return run_names

    print(run_names)


def run():
    if len(sys.argv) >= 2:
        run_names = filter_out_run_names(sys.argv)
        plot_phase_single(run_names)
    else:
        print(
            'No input given, please provide one or more paths within the "results" folder'
        )


if __name__ == "__main__":
    run()
