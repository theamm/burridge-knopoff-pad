from phase_plot_imports import *
from phase_plot_main import (
    get_saved_phase_data,
    create_and_get_phase_data,
    plot_phase,
    plot_phase_intervals,
    plot_one_phase_at_the_time,
)


def plot_phase_single(run_name):
    if "-load" in sys.argv:
        phase_data = get_saved_phase_data(run_name)
    else:
        phase_data = create_and_get_phase_data(run_name=run_name)

    threshold_speed = loader.get_parameter(phase_data["parameters"], "threshold_speed")
    block_numbers = loader.get_parameter(phase_data["parameters"], "blocks")
    if not block_numbers:
        block_numbers = range(0, loader.get_parameter(phase_data["parameters"], "N"))

    if "-section" in sys.argv:
        section_index = sys.argv.index("-section") + 1
        plot_phase_intervals(
            phase_data["position_selection"],
            phase_data["velocity_selection"],
            section_size=int(sys.argv[section_index]),
            custom_legend_title="Surface velocity: " + str(threshold_speed),
            block_numbers=block_numbers,
        )
    elif "-slide" in sys.argv:
        plot_one_phase_at_the_time(
            phase_data["position_selection"],
            phase_data["velocity_selection"],
            custom_legend_title="Surface velocity: " + str(threshold_speed),
            block_numbers=block_numbers,
        )
    else:
        plot_phase(
            phase_data["position_selection"],
            phase_data["velocity_selection"],
            figure_number=0,
            custom_legend_title="Surface velocity: " + str(threshold_speed),
            block_numbers=block_numbers,
        )
    plt.show()


def run():
    if len(sys.argv) >= 2:
        plot_phase_single(sys.argv[1])
    else:
        print(
            'No input given, please provide one or more paths within the "results" folder'
        )


if __name__ == "__main__":
    run()
