from phase_plot_imports import *

from matplotlib.widgets import Slider


def get_threshold_speed(parameters):
    threshold_speed = loader.get_parameter(parameters, "threshold_speed")
    speeds = loader.get_start_end_speed(parameters)
    max_time = loader.get_parameter(parameters, "max_time")
    save_interval = loader.get_parameter(parameters, "save_interval_dt")
    dt = loader.get_parameter(parameters, "dt")
    steps_in_time_unit = 1 / (dt * save_interval)
    n_steps = np.floor(max_time * steps_in_time_unit)

    threshold_speed_start_index = int(
        np.ceil(
            (threshold_speed - speeds["start_speed"])
            / (speeds["end_speed"] - speeds["start_speed"])
            * n_steps
        )
    )
    print( "threshold_speed", threshold_speed,
        "threshold_speed_start_index", threshold_speed_start_index,
        "start_speed", speeds["start_speed"],
        "end_speed", speeds["end_speed"],
        "n_steps", n_steps,
        "steps_in_time_unit", steps_in_time_unit)
    return {
        "threshold_speed": threshold_speed,
        "threshold_speed_start_index": threshold_speed_start_index,
        "start_speed": speeds["start_speed"],
        "end_speed": speeds["end_speed"],
        "n_steps": n_steps,
        "steps_in_time_unit": steps_in_time_unit,
    }


def bool_interaction_with_user(output_to_user):
    response_from_user = input(output_to_user)
    if response_from_user == "yes":
        return True
    else:
        return False


def save_phase_data(
    position_selection,
    velocity_selection,
    run_name,
    file_name="/phase_plot_at_threshold_",
):
    write_to_file = True
    if (
        loader.check_if_file_exist_in_run_folder(run_name, file_name)
        and not "-yes" in sys.argv
    ):
        write_to_file = bool_interaction_with_user(
            'Phase file exists, to overwrite type"yes" or anything elso to skip:'
        )
    if write_to_file:
        if "-block" in sys.argv and len(position_selection) > 1:
            save_matrix = []
            for i in range(0, len(position_selection)):
                save_matrix.append(position_selection[i])
                save_matrix.append(velocity_selection[i])
            save_matrix = np.array(save_matrix)
            file_name += "block.csv"
        else:
            save_matrix = np.array([position_selection, velocity_selection])
            file_name += "pad.csv"
        loader.save_csv(run_name, file_name, save_matrix)
    else:
        print("Skipped printing to file.")


def make_indexes_within_interval(
    start_index, threshold_dictionary, time_units_from_start
):
    indexes_from_start = np.floor(
        time_units_from_start * threshold_dictionary["steps_in_time_unit"]
    )
    max_indexes_from_start = (
        threshold_dictionary["n_steps"]
        - threshold_dictionary["threshold_speed_start_index"]
    )
    if indexes_from_start > max_indexes_from_start:
        print(
            "Selected units greater than the size of the run. Using maximum amount of units."
        )
        print("Selected: ", time_units_from_start)
        print(
            "Maximum: ",
            max_indexes_from_start / threshold_dictionary["steps_in_time_unit"],
        )
        indexes_from_start = max_indexes_from_start
    return int(indexes_from_start)


def make_indexes(start_index, threshold_dictionary, time_units_from_start=1000):
    start_index = start_index + make_indexes_within_interval(
        start_index, threshold_dictionary, 20
    )
    max_indexes_from_start = (
        threshold_dictionary["n_steps"]
        - threshold_dictionary["threshold_speed_start_index"]
    )
    if start_index == max_indexes_from_start:
        print("Can't have the start index be equal to the end index.")
        quit()
    end_index = start_index + make_indexes_within_interval(
        start_index, threshold_dictionary, time_units_from_start
    )
    print("start: ", start_index, "end: ", end_index)
    indexes = range(start_index, end_index)
    return indexes


def get_saved_phase_data(run_name):
    if loader.check_if_file_exist_in_run_folder(
        run_name, "/phase_plot_at_threshold.csv"
    ):
        path_in_results_folder = run_name + "/phase_plot_at_threshold.csv"
        print("Found phase plot at: ", path_in_results_folder)

        phase_data = loader.load_file(loader.results_path + path_in_results_folder)
        if "-block" in sys.argv:
            position_selection = []
            velocity_selection = []
            for i in range(0, int(len(phase_data) / 2)):
                position_selection.append(phase_data[2 * i])
                velocity_selection.append(phase_data[2 * i + 1])
        else:
            position_selection = phase_data[0]
            velocity_selection = phase_data[1]
        parameters = loader.load_yaml_parameters(run_name)
        return {
            "position_selection": position_selection,
            "velocity_selection": velocity_selection,
            "parameters": parameters,
        }
    else:
        print(
            'Sorry, the phase plot does not exist for this run.\n \
            Remove the "-load"-flag to produce it.'
        )
        exit()


def create_and_get_phase_data(run_name):
    parameters = loader.load_yaml_parameters(run_name)
    threshold_dictionary = get_threshold_speed(parameters)
    start_index = threshold_dictionary["threshold_speed_start_index"]
    indexes = make_indexes(start_index, threshold_dictionary, 1)
    if "-block" in sys.argv:
        position = loader.load_simulation_output(parameters, run_name, "block_position")
        velocity = loader.load_simulation_output(parameters, run_name, "block_velocity")
    else:
        position = loader.load_simulation_output(parameters, run_name, "pad_position")
        velocity = loader.load_simulation_output(parameters, run_name, "pad_velocity")

    if len(position.shape) == 1:
        position_selection = position[indexes]
        velocity_selection = velocity[indexes]
    else:
        position_selection = position[:, indexes]
        velocity_selection = velocity[:, indexes]

    save_phase_data(position_selection, velocity_selection, run_name)

    return {
        "position_selection": position_selection,
        "velocity_selection": velocity_selection,
        "parameters": parameters,
    }


def plot_position_velocity(
    single_position,
    single_velocity,
    color_number,
    figure_number=1,
    custom_label="",
    custom_legend_title="",
):
    color_map = plt.cm.Set1
    plt.figure(figure_number)
    (plot_a,) = plt.plot(
        single_position,
        single_velocity,
        color=color_map(color_number),
        label="Phase plot" + custom_label,
    )
    plt.legend(title=custom_legend_title)
    plt.xlabel("position")
    plt.ylabel("velocity")
    plt.figure(figure_number + 1)
    plt.plot(
        single_position,
        color=color_map(color_number),
        label="Position plot" + custom_label,
    )
    plt.plot(
        single_velocity,
        color=color_map(color_number),
        linestyle="--",
        label="Velocity plot" + custom_label,
    )
    plt.legend(title=custom_legend_title)
    plt.xlabel("position/velocity")
    plt.ylabel("surface velocity")
    return plot_a


def plot_phase(
    position,
    velocity,
    figure_number,
    custom_label="",
    custom_legend_title="",
    block_numbers=None,
):
    if "-block" in sys.argv:
        max_position_amplitude = []
        max_velocity_amplitude = []
        for counter, (position, velocity) in enumerate(zip(position, velocity)):
            plot_position_velocity(position, velocity, counter, 2 * figure_number, custom_label = custom_label + ". Block " + str(block_numbers[counter]), custom_legend_title=custom_legend_title)
            if "-bar" in sys.argv:
                max_position_amplitude.append(np.max(position))
                max_velocity_amplitude.append(np.max(velocity))
        if "-bar" in sys.argv:
            plt.figure(100)
            plt.bar(
                block_numbers,
                max_position_amplitude,
                align="edge",
                width=0.5,
                label="Position",
            )
            plt.bar(
                block_numbers,
                max_velocity_amplitude,
                align="edge",
                width=-0.5,
                label="Velocity",
            )
            plt.legend(title="Max amplitues")
    else:
        plot_position_velocity(position, velocity, 0, 1, custom_label=custom_label)


def plot_phase_intervals(
    position,
    velocity,
    section_size,
    custom_label="",
    custom_legend_title="",
    block_numbers=None,
):
    n_elements = len(position)
    n_sections = int(np.floor(n_elements / section_size))
    print("sections:", n_sections, "elemenets: ", n_elements, "section size: ", section_size)
    rest = np.mod(n_elements, section_size)
    for i in range(0, n_sections):
        start = i * section_size
        end = (i + 1) * section_size
        section = range(start, end)
        plot_phase(
            position[section],
            velocity[section],
            i,
            custom_label=custom_label,
            custom_legend_title=custom_legend_title,
            block_numbers=block_numbers[start:end],
        )
    if rest > 0:
        section = range(-rest, 0)
        print("start: ", start, "end: ", end)
        plot_phase(
            position[section],
            velocity[section],
            n_sections,
            custom_label=custom_label,
            custom_legend_title=custom_legend_title,
            block_numbers=block_numbers[-rest:],
        )


def plot_one_phase_at_the_time(
    position, velocity, custom_label="", custom_legend_title="", block_numbers=None
):
    plot_number = 0
    plot_a = plot_position_velocity(
        position[plot_number],
        velocity[plot_number],
        0,
        0,
        custom_label=custom_label + ". Block " + str(block_numbers[0]),
        custom_legend_title=custom_legend_title,
    )

    def update_section_frame(val):
        frame_number = int(np.floor(slider_frame.val))
        # plot_a = plot_position_velocity(position[frame_number], velocity[frame_number], 0, 0, \
        # custom_label = custom_label + ". Block " + str(block_numbers[frame_number]), \
        # custom_legend_title=custom_legend_title)
        plt.figure(0)
        plot_a.set_ydata = velocity[frame_number]
        plt.draw()

    frame = plt.axes([0.25, 0.1, 0.65, 0.03])
    slider_frame = Slider(frame, "Frame", 0, len(position), valinit=0, valfmt="%d")
    slider_frame.on_changed(update_section_frame)
    plt.show()
