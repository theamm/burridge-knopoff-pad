import os
import sys
import numpy as np
from matplotlib import pyplot as plt

import numpy.fft as fft

import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


results_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../results/"
results_folder = ""
numeric_method = "_midpoint_"

def loadFile(
    run_name,
    shared_names,
    selected = 'pad_position.csv'
):
    data = np.loadtxt(results_path + run_name +"/"+  results_folder + shared_names + numeric_method + selected, delimiter = ',')
    return data

def loadYaml(
    run_name
):
    stream = open(results_path +"/"+ run_name + "/parameters.yaml", 'r')
    run_parameters = yaml.load(stream, Loader = Loader)
    return run_parameters

def x_values_for_run(
    number_data_points,
    parameters
):
        start = 0
        stop = parameters["Parameters"]["max_time"]

        x_values = np.linspace(start = start, stop = stop, num = number_data_points)
        return x_values

def plot_runs(
    run_list
):
    for run_name in run_list:
        parameters = loadYaml(run_name)
        shared_names = parameters["Parameters"]["file_name"]
        data = loadFile(run_name, shared_names)
        #plot_fft_frequencies(data)
        x = x_values_for_run(len(data), parameters)
        plt.plot(x, data, label = parameters["Parameters"]["dt"], linewidth = 0.5, marker = 'x')
    plt.legend(title = 'dt')
    plt.show()

def calculate_fft(
    data
):
    half_point = int(0.5*len(data))
    data = data[half_point:]
    spectrum = fft.fft(data)

    frequency = fft.fftfreq(len(spectrum))

    threshold = 0.5 * max(abs(spectrum))
    mask = abs(spectrum) > threshold
    peaks = frequency[mask]
    print(peaks)

    return {'Spectrum' : spectrum, 'Frequency' : frequency}

def plot_fft_frequencies(
    data
):
    fft_results = calculate_fft(data)
    plt.plot(fft_results['Frequency'], fft_results['Spectrum'])
    plt.show()

def run_plot():
    if(len(sys.argv) > 2):
        run_name = sys.argv[1:]
        plot_pad = plot_runs(run_name)
    elif(len(sys.argv) == 2):
        plot_pad = plot_runs([sys.argv[1]])
    else:
        print("This will probably not work, have you remembered to give input to the command?")
        plot_pad = PlotPads()

if __name__ == "__main__":
    run_plot()
