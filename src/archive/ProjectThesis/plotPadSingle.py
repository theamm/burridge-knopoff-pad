import os
import sys
import numpy as np
from matplotlib import pyplot as plt

import matplotlib as mpl

mpl.rcParams['svg.fonttype'] = "none"
mpl.rcParams['font.size'] = 16


mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
mpl.rc('text', usetex=True)

import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class PlotPad:

    def __init__(self, run_name = "", numeric_method = "_midpoint_"):
        self.loadFiles(run_name, numeric_method)
        self.setUpPlot()


    def loadFiles(self, run_name, numeric_method):
        self.results_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../results/"

        results_folder = "/"

        stream = open(self.results_path + run_name + "/parameters.yaml", 'r')
        self.run_parameters = yaml.load(stream, Loader = Loader)

        shared_names = self.run_parameters["Parameters"]["file_name"]

        self.pad_position = np.loadtxt(self.results_path + run_name + results_folder + shared_names + numeric_method + "pad_position.csv", delimiter = ',')
        if(not only_position):
            self.pad_velocity = np.loadtxt(self.results_path + run_name + results_folder + shared_names + numeric_method + "pad_velocity.csv", delimiter = ',')
            self.pad_friction = np.loadtxt(self.results_path + run_name + results_folder + shared_names + numeric_method + "pad_friction.csv", delimiter = ',')

    def setUpPlot(self):
        self.one_block_boolean = False

        self.linewidth = 0.9

        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]

        save_interval = 1

        if("-i" in sys.argv):
            try:
                save_interval = self.run_parameters["Parameters"]["save_interval_dt"]
            except ValueError:
                print("Look like this run does not contain a \"save_interval_dt\" parameter.")

        number_of_points = len(self.pad_position)

        self.x = np.linspace(start = start, stop = stop, num = number_of_points)

        self.percent_interval = int(1.0*number_of_points)
        print(self.percent_interval)

    def plot_pad_position(self):
        plt.plot(self.x[0:self.percent_interval], self.pad_position[0:self.percent_interval], label = "Pad position", linewidth = self.linewidth)

        plt.ylabel("$y$")
        plt.xlabel("$t$")
        plt.legend()
        plt.show()

    def plot_pad_velocity(self):
        plt.plot(self.x, self.pad_velocity, label = "Pad velocity", linewidth = self.linewidth - 0.2)

        plt.xlabel("$y$")
        plt.ylabel("$t$")
        plt.legend()
        plt.show()

    def plot_pad_friction(self):
        plt.plot(self.x, self.pad_friction, label = "Pad friction", linewidth = self.linewidth - 0.2)

        plt.xlabel("$y$")
        plt.ylabel("$t$")
        plt.legend()
        plt.show()

    def plot_pad_position_vers_velocity(self):
        plt.plot(self.pad_position, self.pad_velocity)
        plt.show()

    def plot_segments(self):
        if('-s' in sys.argv):
            try:
                n_segments = int(sys.argv[-1])
                print("Number of intervals: ", n_segments)
            except ValueError:
                print("Please enter the number of segments as the last argument when using \'-s\'")

            n_data_points_in_segment = int(len(self.x)/n_segments)

            for i in range(n_segments):
                start_index = i * n_data_points_in_segment
                end_index = (i + 1) * n_data_points_in_segment

                if(i == n_segments - 1):
                    end_index = -1

                plt.plot(self.x[start_index:end_index], self.pad_position[start_index:end_index], label = "Segment " + str(i))
                plt.legend
                plt.show()





def plotastic():
    if(len(sys.argv) > 2):
        plot_pad = PlotPad(sys.argv[1], sys.argv[2])
    elif(len(sys.argv) == 2):
        plot_pad = PlotPad(sys.argv[1])
    else:
        print("This will probably not work, have you remembered to give input to the command?")
        plot_pad = PlotPad()

    plot_pad.plot_pad_position()
    plot_pad.plot_segments()
    if(not only_position):
        plot_pad.plot_pad_velocity()
        plot_pad.plot_pad_friction()
        plot_pad.plot_pad_position_vers_velocity()
only_position = True

if __name__ == "__main__":
    plotastic()
