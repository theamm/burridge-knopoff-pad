import os
import sys
import numpy as np
from matplotlib import pyplot as plt

import matplotlib as mpl

mpl.rcParams["svg.fonttype"] = "none"
mpl.rcParams["font.size"] = 16

mpl.rcParams["savefig.directory"] = "../../Figures"


mpl.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
mpl.rc("text", usetex=True)

import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class Plot_results:
    def __init__(self, run_name="", numeric_method="_midpoint_"):
        self.loadFiles(run_name, numeric_method)
        self.setUpPlot()

    def loadFiles(self, run_name, numeric_method):
        self.results_path = (
            os.path.dirname(os.path.realpath(__file__)) + "/../../../results/"
        )

        results_folder = "/"

        stream = open(self.results_path + run_name + "parameters.yaml", "r")
        self.run_parameters = yaml.load(stream, Loader=Loader)

        shared_names = self.run_parameters["Parameters"]["file_name"]

        self.block_position = np.loadtxt(
            self.results_path
            + run_name
            + results_folder
            + shared_names
            + numeric_method
            + "block_position.csv",
            delimiter=",",
        )
        # print(len(self.block_position), len(self.block_position[0]))
        self.block_velocity = np.loadtxt(
            self.results_path
            + run_name
            + results_folder
            + shared_names
            + numeric_method
            + "block_velocity.csv",
            delimiter=",",
        )

        self.pad_position = np.loadtxt(
            self.results_path
            + run_name
            + results_folder
            + shared_names
            + numeric_method
            + "pad_position.csv",
            delimiter=",",
        )
        self.pad_velocity = np.loadtxt(
            self.results_path
            + run_name
            + results_folder
            + shared_names
            + numeric_method
            + "pad_velocity.csv",
            delimiter=",",
        )

    def setUpPlot(self):
        self.one_block_boolean = False

        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]

        if len(self.block_position.shape) == 1:
            self.one_block_boolean = True
            self.x = np.linspace(start=start, stop=stop, num=len(self.block_position))
        else:
            self.x = np.linspace(
                start=start, stop=stop, num=len(self.block_position[0])
            )

    def example_function(self):
        sigma = 1.0
        mu = 0
        return (1.0 / np.sqrt(2 * np.pi * np.square(sigma))) * np.exp(
            -np.square(self.x - mu) / (2 * np.square(sigma))
        )

    def example_plot(self):
        self.y = self.example_function()
        fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, sharex=True)

        if not self.one_block_boolean:
            for i in range(0, len(self.block_position)):
                ax1.plot(self.x, self.block_position[i], label="Block: " + str(i))
                ax2.plot(self.x, self.block_velocity[i], label="Velocity: " + str(i))
        else:
            ax1.plot(self.x, self.block_position, label="Block: " + "1")
            ax2.plot(self.x, self.block_velocity, label="Velocity: " + "1")
        ax3.plot(self.x, self.pad_position)
        ax4.plot(self.x, self.pad_velocity)
        # plt.title("Example plot", fontdict={'fontname': 'Times New Roman', 'fontsize': 21}, y=1.03)
        # plt.ylabel("$y$")
        plt.xlabel("$t$")
        # plt.legend()
        plt.show()

    def plot_blocks_position(self):
        if not self.one_block_boolean:
            for i in range(0, len(self.block_position)):
                plt.plot(
                    self.x,
                    self.block_position[i] - self.block_velocity[i],
                    label="Block: " + str(i),
                )
        else:
            plt.plot(self.x, self.block_position, label="Block: " + "1")

        # plt.title("Position plot of blocks", fontdict={'fontname': 'Times New Roman', 'fontsize': 21}, y=1.03)
        plt.ylabel("$y$")
        plt.xlabel("$t$")
        plt.legend()
        plt.show()

    def plot_pad_position(self):
        plt.plot(self.x, self.pad_position, label="Pad position")

        plt.ylabel("$y$")
        plt.xlabel("$t$")
        plt.legend()
        plt.show()

    def plot_blocks_position_vers_velocity(self):
        if not self.one_block_boolean:
            for i in range(0, len(self.block_position)):
                plt.plot(
                    self.block_position[i],
                    self.block_velocity[i],
                    label="Block: " + str(i),
                )
        else:
            plt.plot(self.block_position, self.block_velocity, label="Block: " + "1")
        plt.ylabel("$x$")
        plt.xlabel("$v$")
        plt.show()


def plotastic():
    if len(sys.argv) > 2:
        example = Plot_results(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 2:
        example = Plot_results(sys.argv[1])
    else:
        example = Plot_results()
    example.example_plot()
    example.plot_blocks_position()
    example.plot_pad_position()
    example.plot_blocks_position_vers_velocity()


if __name__ == "__main__":
    plotastic()
