import os
import sys
from matplotlib import pyplot as plt

from tikzplotlib import save as tikz_save

import matplotlib as mpl

mpl.rcParams['svg.fonttype'] = "none"
mpl.rcParams['font.size'] = 16


mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
mpl.rc('text', usetex=True)

import numpy as np
import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class CheckEnergy:
    def __init__(self, run_name = "", comparison_name = "", numeric_method = "_midpoint_"):
        self.loadFiles(run_name, numeric_method)
        self.setUpPlot()
        self.checkPossibleEnergytests()

        if(comparison_name == ""):
            self.singleResult()
        else:
            self.comparisonResult(comparison_name, numeric_method)

    def singleResult(self):
        if(self.only_pulling_springs):
            print("Only pullings springs in input.")
            energy = self.onlyPullingSpringsEnergyTest(self.block_velocity, self.block_position)
        elif(self.only_neighbor_springs):
            print("Only neighbor springs in input.")
            energy = self.onlyNeighborSpringsEnergyTest(self.block_velocity, self.block_position)
        else:
            print("Can only run test 1 if no fiction, neighbor springs, \
                damper is activated and the pad behaves as it's fastened to the roof surface. ")
            print("Can only run test 2 if no friction, stationary springs \
                damper is activated and the pad behaves as it's fastened to the roof surface. ")
            quit()

        self.plotEnergyTest(energy, zoom = False)
        self.plotEnergyTest(energy)



    def comparisonResult(self, comparison_name, numeric_method):
        self.loadComparisonFiles(comparison_name, numeric_method)
        self.setUpPlotComparison()
        if(self.only_pulling_springs):
            energy = self.onlyPullingSpringsEnergyTest(self.block_velocity, self.block_position)
            energy_comparison = self.onlyPullingSpringsEnergyTest(self.block_velocity_comparison, self.block_position_comparison)
        elif(self.only_neighbor_springs):
            energy = self.onlyNeighborSpringsEnergyTest(self.block_velocity, self.block_position)
            energy_comparison = self.onlyNeighborSpringsEnergyTest(self.block_velocity_comparison, self.block_position_comparison)
        else:
            print("Can only run test 1 if no fiction, neighbor springs, \
                damper is activated and the pad behaves as it's fastened to the roof surface. ")
            print("Can only run test 2 if no friction, stationary springs \
                damper is activated and the pad behaves as it's fastened to the roof surface. ")
            quit()

        self.plotEnergyTestComparison(energy, energy_comparison, zoom = False)
        self.plotEnergyTestComparison(energy, energy_comparison)

    def loadFiles(self, run_name, numeric_method):
        self.results_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../results/"
        results_folder = "/results/"

        stream = open(self.results_path + run_name + "/parameters.yaml", 'r')
        self.run_parameters = yaml.load(stream, Loader = Loader)

        shared_names = self.run_parameters["Parameters"]["file_name"]

        self.block_position = np.loadtxt(self.results_path + run_name +"/" + shared_names + numeric_method + "block_position.csv", delimiter=",")
        self.block_velocity = np.loadtxt(self.results_path + run_name + "/"+ shared_names + numeric_method + "block_velocity.csv", delimiter=",")

    def loadComparisonFiles(self, comparison_name, numeric_method):
        print("Remember, this code assumes that the only difference between the two runs is the stepsize.")

        results_folder = "/results/"

        shared_names = self.run_parameters["Parameters"]["file_name"]

        self.block_position_comparison = np.loadtxt(self.results_path + comparison_name + results_folder + shared_names + numeric_method + "block_position.csv", delimiter=",")
        self.block_velocity_comparison = np.loadtxt(self.results_path + comparison_name + results_folder + shared_names + numeric_method + "block_velocity.csv", delimiter=",")

    def setUpPlot(self):

        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]

        max_time = self.run_parameters["Parameters"]["max_time"]

        if(len(self.block_position.shape) == 1):
            self.one_block_boolean = True
            self.x = np.linspace(start = start, stop = stop, num = len(self.block_position))

            self.step_size = max_time/len(self.block_position)
        else:
            self.x = np.linspace(start = start, stop = stop, num = len(self.block_position[0]))
            self.step_size = max_time/len(self.block_position[0])


    def setUpPlotComparison(self):
        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]

        max_time = self.run_parameters["Parameters"]["max_time"]

        if(len(self.block_position_comparison.shape) == 1):
            self.one_block_boolean_comparison = True
            self.x_comparison = np.linspace(start = start, stop = stop, num = len(self.block_position_comparison))
            self.step_size_comparison = max_time/len(self.block_position_comparison)
        else:
            self.x_comparison = np.linspace(start = start, stop = stop, num = len(self.block_position_comparison[0]))
            self.step_size_comparison = max_time/len(self.block_position_comparison[0])


    def checkPossibleEnergytests(self):
        self.checkOnlyPullingEnergyTest()
        self.checkOnlyNeighborEnergyTest()

    def checkOnlyPullingEnergyTest(self):
        no_friction = self.run_parameters["Debug"]["debug_no_friction"]
        no_neighbor_springs = self.run_parameters["Debug"]["debug_no_neighbor_springs"]
        no_damper = self.run_parameters["Debug"]["debug_no_damper"]
        no_pad = self.run_parameters["Debug"]["debug_no_pad"]

        self.only_pulling_springs = all(value == True for value in [no_friction, no_neighbor_springs, no_damper, no_pad])

    def checkOnlyNeighborEnergyTest(self):
        no_friction = self.run_parameters["Debug"]["debug_no_friction"]
        no_stationary_springs = self.run_parameters["Debug"]["debug_no_stationary_springs"]
        no_damper = self.run_parameters["Debug"]["debug_no_damper"]
        no_pad = self.run_parameters["Debug"]["debug_no_pad"]

        self.only_neighbor_springs = all(value == True for value in [no_friction, no_stationary_springs, no_damper, no_pad])


    def onlyPullingSpringsEnergyTest(self, block_velocity, block_position):

        N = self.run_parameters["Parameters"]["N"]
        m_scale_mass = self.run_parameters["Parameters"]["m_scale_mass"]
        m_mass_x = self.run_parameters["Parameters"]["m_mass_x"]
        m_scale_P = self.run_parameters["Parameters"]["m_scale_P"]
        m_k_P0 = self.run_parameters["Parameters"]["m_k_P0"]

        m_u = m_scale_mass*m_mass_x/N
        k_p = m_scale_P*m_k_P0/N

        energy_only_pulling_springs_test = 1/2 * m_u * np.sum(np.power(block_velocity, 2), axis=0) + \
            1/2 * k_p * np.sum(np.power(block_position, 2), axis = 0)
        return(energy_only_pulling_springs_test)

    def onlyNeighborSpringsEnergyTest(self, block_velocity, block_position):
        N = self.run_parameters["Parameters"]["N"]
        m_scale_mass = self.run_parameters["Parameters"]["m_scale_mass"]
        m_mass_x = self.run_parameters["Parameters"]["m_mass_x"]
        m_scale_C = self.run_parameters["Parameters"]["m_scale_C"]
        m_k_P0 = self.run_parameters["Parameters"]["m_k_P0"]

        m_u = m_scale_mass*m_mass_x/N
        k_c = m_scale_C*m_k_P0*N

        last_energy_ledd = block_position[1:,] - block_position[:-1,]

        energy_only_neighbor_springs_test = 1/2 * m_u * np.sum(np.power(block_velocity, 2), axis=0) + \
            1/2 * k_c * np.sum(np.power(last_energy_ledd, 2), axis = 0)

        return(energy_only_neighbor_springs_test)

    def plotEnergyTest(self, energy_test_result, zoom = True):
        plt.plot(self.x, energy_test_result, label = "Energy")

        if(not zoom):
            plt.ylim(0, np.max(energy_test_result)*(1.10))

        plt.ylabel("$E$")
        plt.xlabel("$t$")
        plt.legend()
        plt.show()

    def plotEnergyTestComparison(self, energy_test_result, energy_test_result_comparison, zoom = True):
        plt.plot(self.x, energy_test_result, label = str(self.step_size))
        plt.plot(self.x_comparison, energy_test_result_comparison, label = str(self.step_size_comparison))

        if(not zoom):
            plt.ylim(0, np.max([np.max(energy_test_result), np.max(energy_test_result_comparison)])*(1.10))

        plt.ylabel("$E$")
        plt.xlabel("$t$")
        plt.legend()
        plt.show()

    def saveAsTikz(self, filename):
        tikz_save(self.results_path + 'mytikz.tex', figureheight='8cm', figurewidth='8cm')




def run():
    if(len(sys.argv) > 2):
        checkEnergy = CheckEnergy(sys.argv[1], sys.argv[2])
    elif(len(sys.argv) == 2):
        checkEnergy = CheckEnergy(sys.argv[1])
    else:
        checkEnergy = CheckEnergy()

if __name__ == "__main__":
    run()
