import os
import sys
import numpy as np
from matplotlib import pyplot as plt

import matplotlib as mpl

mpl.rcParams["svg.fonttype"] = "none"
mpl.rcParams["font.size"] = 16

mpl.rcParams["savefig.directory"] = "../../Figures"
plt.rcParams["savefig.format"] = "svg"

mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
mpl.rc('text', usetex=True)

import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class PlotPads:
    def __init__(self, run_name = [""], numeric_method = "_midpoint_", time_steps_plotted = 300000):
        self.loadFiles(run_name, numeric_method)
        self.setUpPlot(run_name)
        self.time_steps_plotted = time_steps_plotted

    def loadYaml(
        self,
        run_name
    ):
        stream = open(self.results_path + run_name + "/parameters.yaml", 'r')
        run_parameters = yaml.load(stream, Loader = Loader)
        return run_parameters


    def loadFiles(self, run_name, numeric_method):
        self.results_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../results/"

        results_folder = "/"

        self.run_parameters = self.loadYaml(run_name[0])

        shared_names = self.run_parameters["Parameters"]["file_name"]


        self.pad_position = [np.loadtxt(self.results_path + run_name[0] + results_folder + shared_names + numeric_method + "pad_position.csv", delimiter = ',')]
        self.pad_velocity = [np.loadtxt(self.results_path + run_name[0] + results_folder + shared_names + numeric_method + "pad_velocity.csv", delimiter = ',')]

        self.label_names = self.getPadLabels(run_name, use_dictionary_names= False)

        if(len(run_name) > 1):
            for i in range(1,len(run_name)):
                new_pad_position = np.loadtxt(self.results_path + run_name[i] + results_folder + shared_names + numeric_method + "pad_position.csv", delimiter = ',')
                new_pad_velocity = np.loadtxt(self.results_path + run_name[i] + results_folder + shared_names + numeric_method + "pad_velocity.csv", delimiter = ',')

                self.pad_position = np.vstack((self.pad_position, new_pad_position))
                self.pad_velocity = np.vstack((self.pad_velocity, new_pad_velocity))

    def getPadLabels(self, run_name, use_dictionary_names = False):
        if(use_dictionary_names):
            short_names = {'blocks_3_no_damper_no_neighbor_test': 'ND,NN', 'blocks_3_no_damper_test': 'ND', \
                        'blocks_3_no_friction_test': 'NF', 'blocks_3_no_debug_test': 'BKP', \
                        'blocks_3_no_friction_no_damper_no_neighbor_test': 'NF,ND,NN', \
                        'blocks_3_no_debug_long_test': 'BKP Long'}

            label_names = [short_names[x] for x in run_name]
        else:
            label_names = ["seed: 101", "seed: 101", "seed: 102", "seed: 103"]#["no pad", "inf. weight"]#["$\sigma=0.1$", "$\sigma=0.01$", "$\sigma=0.001$"]

        return (label_names)

    def x_values_for_run(
        self,
        run_name,
        number_data_points = 0
    ):
        run_parameters = self.loadYaml(run_name)
        start = 0
        stop = run_parameters["Parameters"]["max_time"]

        if( number_data_points == 0 ):
            dt = run_parameters["Parameters"]["dt"]
            save_every_interval_dt = run_parameters["Parameters"]["save_interval_dt"]

            number_data_points = (stop - start)/(dt*save_every_interval_dt)


        x_values = np.linspace(start = start, stop = stop, num = number_data_points)
        return x_values


    def setUpPlot(self, run_name):
        self.one_block_boolean = False

        self.linewidth = 0.9

        start = 0
        stop = self.run_parameters["Parameters"]["max_time"]

        self.x = np.array([self.x_values_for_run(file) for file in run_name])


    def plot_pad_position(self):

        if(len(self.x.shape) > 1):
            plot_untill = int(np.ceil(self.time_steps_plotted/(self.x[0][-1])*len(self.x[0])))
        else:
            plot_untill = int(np.ceil(5000/(self.x[-1])*len(self.x)))

        for i in range(len(self.pad_position)):
            plt.plot(self.x[i][:plot_untill], self.pad_position[i][:plot_untill], label = self.label_names[i], linewidth =self.linewidth)

        #plt.ylim(-0.42,0.45)

        plt.ylabel("$y$")
        plt.xlabel("$t$")
        plt.legend(loc = 1)
        plt.show()

    def plot_pad_velocity(self):

        if(len(self.x.shape) > 1):
            plot_untill = int(np.ceil(self.time_steps_plotted/(self.x[0][-1])*len(self.x[0])))
        else:
            plot_untill = int(np.ceil(5000/(self.x[-1])*len(self.x)))

        for i in range(len(self.pad_position)):
            plt.plot(self.x[i][:plot_untill], self.pad_velocity[i][:plot_untill], label = self.label_names[i], linewidth =self.linewidth -0.2)

        plt.xlabel("$t$")
        plt.ylabel("$y$")
        plt.legend()
        plt.show()

    def plot_pad_position_different_time_step(self):
        print("hi")



def plotastic():
    if(len(sys.argv) > 2):
        run_name = sys.argv[1:]
        plot_pad = PlotPads(run_name)
    elif(len(sys.argv) == 2):
        plot_pad = PlotPads([sys.argv[1]])
    else:
        print("This will probably not work, have you remembered to give input to the command?")
        plot_pad = PlotPads()
    plot_pad.plot_pad_position()
    plot_pad.plot_pad_velocity()

if __name__ == "__main__":
    plotastic()
