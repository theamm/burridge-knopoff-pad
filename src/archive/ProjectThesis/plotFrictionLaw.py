import numpy as np
from matplotlib import pyplot as plt

import matplotlib as mpl

plt.rcParams["svg.fonttype"] = "none"
plt.rcParams['font.size'] = 16

#mpl.rcParams["savefig.directory"] = "../../Figures"

#plt.style.use('classic')

import sys

class FrictionLaw:

    def __init__(self):
        self.sigma = 0.01
        self.F_0 = 0.1
        self.y = np.linspace(start = -10, stop = 10, num =1000)

    def frictionLaw(self):
        self.phi = self.F_0 * ((1 - self.sigma)/(1 + np.abs(self.y)/(1 - self.sigma)) * np.sign(self.y))
        self.setMidPointsToOne()

    def setMidPointsToOne(self):
        length_of_phi = len(self.phi)
        if(length_of_phi % 2 == 0):
            self.phi[int(length_of_phi/2 - 1)] = -1*self.F_0
            self.phi[int(length_of_phi/2)] = 1*self.F_0

            self.y[int(length_of_phi/2 - 1)] = 0
            self.y[int(length_of_phi/2)] = 0
        else:
            self.phi[int((length_of_phi-1)/2) -1] = -1*self.F_0
            self.phi[int((length_of_phi-1)/2)] = 1*self.F_0

            self.y[int((length_of_phi-1)/2 - 1)] = 0
            self.y[int((length_of_phi - 1)/2)] = 0

    def frictionLawCarlson(self):
        sigma = 0.1
        y_bigger_than_zero = np.linspace(start = 0.001, stop = 10, num = 1000)
        y = np.hstack((np.array([0,0]), y_bigger_than_zero))
        phi = np.hstack((np.array([-2, 1]), ((1 - sigma)/(1 + np.abs(y_bigger_than_zero)/(1 - sigma)) * np.sign(y_bigger_than_zero))))

        fig, ax = plt.subplots()
        ax.axhline(y=0, color = 'k')
        ax.axvline(x=0, color = 'k')
        ax.plot(y, phi, label = "Friction Law")
        ax.set_ylim(-1, 1.5)
        ax.set_xlim(-3, 3)
        if('-l' in sys.argv):
            plt.xscale('log')
            ax.set_xlim(0, 1)
        ax.set_ylabel("$\phi(y)$")
        ax.set_xlabel("$y$")
        ax.legend()
        plt.show()


    def plotFrictionLaw(self):
        self.frictionLaw()

        plt.plot(self.y, self.phi)
        plt.ylabel("\$\phi(y)\$")
        plt.xlabel("\$y\$")
        plt.axes()
    #    plt.savefig("../../Figures/Master/Friction_law_demo.svg", format="svg")
        plt.show()


def plot_friction_law():
    friction = FrictionLaw()
    friction.plotFrictionLaw()
    friction.frictionLawCarlson()

if __name__ == "__main__":
    plot_friction_law()
