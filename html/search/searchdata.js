var indexSectionsWithContent =
{
  0: "abcdefhiklmnoprstuvwy",
  1: "dops",
  2: "s",
  3: "dmoprs",
  4: "acdfhilmnoprstuwy",
  5: "abcdefiklmnprstuvy",
  6: "o",
  7: "st"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros",
  7: "Pages"
};

