# The Burridge Knopoff model

## Descripton
Repository for simulating and plotting with the The Burridge Knopoff Pad model by master student Thea Marstrander.
See Chapter 4 of master thesis to read more about the implemation.

## Motivation
The Burridge Knopoff Pad model is a newly developed model to represent friction induced vibrations. Friction induced vibrations is a serious problem in industry, and several reserchers have tried to represent it. 
However, there is to this date no accurate model to represent friction induced vibrations.


## Directiories
### yaml
Contains .yaml-files used as input in the simualtions.
A description of each parameter is found in the thesis.

### src
Contains three sub-directories, each with their own readme.md-file.

#### src/simulation: 
Contains three classes c++ (.cpp) scripts mainly used to simulate the model.
    1. Simulation: used to simulate the BKP model. Split into to six scripts.
    2. Parameters: used to input the parameters.
    3. Operation: used to compute mode shapes and modal contributions. Split into three scripts.

#### src/plots: 
Contains python-scripts used to plot simulations.

#### src/archive: 
contains python-scripts used by previous students.

### include
Includes the header (.hpp) files used in simulation.

### results
When running the simulations, the results will end up in this directory.

### html, latex and .vscode
    
Dicrectories created when running doxygen. 

Open documentation webpage version:

`cd html`
`open index.html `

Generate pdf version from latex.
If latex is installed:

`cd latex`

`pdflatex refman`

`open refman.pdf`

#### Generate new dxygen:

Navigate to BK-Pad, run:

`doxygen doxy`

Then do the previous steps to open the webversion and pdf versions of doxygen.



## Installation
### Simulations packages (c++)
- yaml
- eigen
- openmp
- larmadillo

### Compilation 
Navigate to the simulation folder

`cd src/simulation`

Compile 

`make`

To run

`./main`

To run with the creation of eigenvalues, eigenvectors and displacememnt per mode shape:

`./main -e`

Delete object-files:

`make clean`

#### Problems?
As stated in the thesis, the compilation is done using the makefile under src/simulation.
If the program dosent compile, first make sure to have download the neceserry dependencies. If there are errors, try to add more flags behind CXXFLAGS according to the error message. Another option is to install and use cmake in order to compile.
        

### python packages
Main packages
- python3
- matplotlib
- seaborn 
- numpy
- yaml

Other packages
- math
- tqdm
- time
- statistics
